!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=320, initial-scale=1">
<title>WB's Monitoring System</title>
<link rel="stylesheet" href="{{ asset('/assets/css/bootstrap.min.css') }}">
<![endif]-->
</head>
<body>
<nav class="navbar navbar-inverse navbar-fixed-top">
<div class="container-fluid">
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<div class="navbar-header">
<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
<span class="sr-only">Toggle Navigation</span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<a id="home" class="navbar-brand" href="#"> Welcome &copy;</a>
</div>
<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
<ul class="nav navbar-nav navbar-right">
<li><a href="{{ url('/auth/login') }}">Login</a></li>
</ul>
</div>
</div>
</div>
</div>
</nav>
<div style="text-align:center;width:auto;color:white">
</div>
<br><br><br><br>
<div class="container-fluid">
<div class="row">
<div class="col-md-6 col-md-offset-3">
<div class="panel panel-success">
<div class="panel-heading">Login</div>
<div class="panel-body">
@if (count($errors) > 0)
<div class="alert alert-danger">
<strong>Whoops!</strong> There were some problems with your input.<br><br>
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif
<form class="form-horizontal" role="form" method="POST" action="{{ url('/auth/login') }}">
<input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
<div class="form-group">
<label class="col-md-4 control-label">Email</label>
<div class="col-md-6">
<input type="text" class="form-control" name="email" value="" required>
</div>
</div>
<div class="form-group">
<label class="col-md-4 control-label">Password</label>
<div class="col-md-6">
<input type="password" class="form-control" name="password" required>
</div>
</div>
<div class="form-group">
<div class="col-md-6 col-md-offset-4">
<div class="checkbox">
<label>
<input type="checkbox" name="remember"> Remember Me
</label>
</div>
</div>
</div>
<div class="form-group">
<div class="col-md-6 col-md-offset-4">
<button type="submit" class="btn btn-primary btn-block btn-lg">Login</button>
  <a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your
                                    Password?</a>
</div>

</div>
</br>
</br>
</br>
</br>
</form>
</div>
</div>
</div>
</div>
@include('flash::message')<br>
</div>
</body>
</html>
