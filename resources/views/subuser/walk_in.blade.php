<div id="walk_inShow" hidden>
    <div class="form-group col-lg-3  col-md-3 col-sm-3">
        <label for="Date">Date</label>
        <input type='text' name="date3" class="form-control"
               id='datetimepicker6' id="date2"/>
    </div>
    <div class="form-group col-lg-3 col-md-3 col-sm-3">
        <label for="OR">OR</label>
        <input type="text" name="or"
               placeholder="or #" class="form-control">
    </div>
    <div class="form-group col-lg-3 col-md-3 col-sm-3">
        <label for="Sold">Customer</label>
        <input type="text" name="sold"
               class="form-control" list="customers1" placeholder="select/new..">
        <datalist id="customers1">
            @foreach($custs1 as $cust)
                <option value="{{$cust['name']}}"></option>
            @endforeach
        </datalist>

    </div>
</div>
