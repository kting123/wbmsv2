@extends('appv1')

@section('content')
    {{--<style>--}}
    {{--.modal-header {--}}
    {{--padding: 2px;--}}
    {{--}--}}
    {{--</style>--}}

    <style>
        #arrow1 {
            font-weight: bold;
        }

        td, th {
            padding: 2px;
        }

        #arrow {
            visibility: hidden;
        }
    </style>
    <?php $i = 0;?>
    <div class="modal fade" id="change" role="dialog">
        <div class="modal-dialog modal-sm">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Change</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                        <label for="">Total Due ₱ </label>
                        <input class="form-control" type="text" id="amount_due" disabled>
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                        <label for="">Cash ₱ </label>
                        <input class="form-control" type="text" id="cash">
                    </div>
                    <div class="form-group col-lg-12 col-md-12 col-sm-12">
                        <label for="">Change ₱ </label>
                        <input class="form-control" type="text" id="change11" disabled>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-danger"><span
                                class="glyphicon glyphicon-remove"></span>
                        Cancel
                    </button>
                    <button id="confirm" class="btn btn-{{Auth::user()->buttons}}" disabled><span
                                class="glyphicon glyphicon-ok"></span>
                        Confirm
                    </button>
                    <div hidden style="background-image: url('assets/img/wait.gif');width: 35px;height: 35px;"
                         id="wait1">
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="container">
        <div class="row">
            <div class="container-fuild">
                <div class="panel panel-{{Auth::user()->panels}} ">
                    <div class="panel-heading" style="height:50px; margin-top: 5px;">
                        <h4><span class="glyphicon glyphicon-shopping-cart"></span> Order Info</h4>
                    </div>
                    <form type="hidden" method="post" action="./addOrder" id="order">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <input type="hidden" id="nowdate" name="nowdate" value=""/>
                        <input type="hidden" id="nowhr" name="hr" value=""/>
                        <input type="hidden" id="nowmin" name="min" value=""/>
                        <input type="hidden" id="nowsecs" name="secs" value=""/>
                        <input type="hidden" id="timestamp" name="timestamp" value=""/>
                        <div class="panel-body">
                            <div class="col-lg-12 col-md-12 col-sm-12">
                                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                    <label for="">Choose Type</label><br>
                                    <label class="radio-inline">
                                        <input type="radio" name="radBtn" id="Delivery" value="1" checked> <b>
                                            Delivery</b>
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="radBtn" id="WalkeIn" value="0"> <b>Walk-In </b>
                                    </label>
                                    &nbsp;&nbsp;
                                </div>
                                @include('subuser.delivery')
                                @include('subuser.walk_in')
                            </div>
                            @include('reusable.order_add_edit')
                            </tbody>
                            </table>
                        </div>

                        <label for="Total"><h3>Total Due (Php):</h3></label>
                        <label for="TAmount"><h3><input id="totalD" value="0.00" name="Tamount" readonly
                                                        style='width: 100px;color: #000000; background-color: transparent;height: 100%;width: 100%; border: 0;padding: 0px 0px 0px;'/>
                            </h3></label>
                </div>
            </div>
            <div class="form-group pull-right">
                <a href="./transactions" class="btn btn-danger"><span
                            class="glyphicon glyphicon-refresh"></span> Refresh All
                </a>
                <a onclick="ResetTable()" class="btn btn-warning"><span
                            class="glyphicon glyphicon-trash"></span> Reset List</a>
                <a id="checkout" class="btn btn-{{Auth::user()->buttons}}"><span
                            class="glyphicon glyphicon-shopping-cart"></span> Checkout
                </a>
            </div>
        </div>
        <div hidden style="background-image: url('assets/img/wait.gif');width: 35px;height: 35px;"
             id="wait1">
        </div>
        </form>
    </div>
    </div>
    </div>
    </div>
    </div>
    @include('reusable.getClientTime')
    <script>
        $(function () {
            $('#datetimepicker6').datepicker("setDate", '1d');
            $('#datetimepicker4').datepicker("setDate", '1d');
            $('#datetimepicker5').datepicker("setDate", '1d');
            $('#checkout').click(function () {
                if ($('#Delivery').is(":checked")) {
                    var amount = $('#amount_due').val();
                    if (amount == '') {
                        alert('Please add some item.');
                    }
                    else {
                        $('#wait1').show();
                        $('#order').submit();
                    }
                }
                if ($('#WalkeIn').is(":checked")) {
                    $('#change').modal('show');
                }
            });
            $('#cash').keyup(function () {
                var amount = $('#amount_due').val();
                var cash = $('#cash').val();
                var change = cash - amount;
                if (change < 0) {
                    $('#confirm').attr('disabled', true);
                }
                else {
                    $('#confirm').removeAttr('disabled');
                }
                document.getElementById("change11").value = change;
                // alert(change);

            });
            $('input[type="radio"]').click(function () {
                if ($(this).attr('id') == 'Delivery') {
                    $('#deliveryShow').show();
                    $('#walk_inShow').hide();

                }
                else {
                    $('#deliveryShow').hide();
                    $('#walk_inShow').show();

                }
            });
            $('#confirm').click(function () {
                var amount = $('#amount_due').val();
                if (amount == '') {
                    alert('Please add some item.');
                }
                else {
                    $('#wait1').show();
                    $('#order').submit();
                }

            });
        });
    </script>
    @include('reusable.JsForAdd_Edit_Order')
@endsection
