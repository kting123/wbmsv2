@extends('appv1')

@section('content')
    <style>
        #arrow1 {
            font-weight: bold;
        }

        #arrow {
            visibility: hidden;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="panel panel-info col-lg-10 col-lg-offset-1">
                <div class="panel-heading" style="height:50px; margin-top: 5px;">
                    Order List <a style="font-size: larger;" data-toggle="modal" data-target="#info" class="pull-right"
                                  href="#"><b><span class="glyphicon glyphicon-info-sign"></span></b></a>
                </div>
                <div class="panel-body">
                    <form type="hidden" method="post" action="./add_outsourceOrder" id="form1">
                        <input type="hidden" name="id" value="{{$articles['0']['outsource_id']}}"/>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <input type="hidden" id="nowdate" name="nowdate" value=""/>
                        <input type="hidden" id="nowhr" name="hr" value=""/>
                        <input type="hidden" id="nowmin" name="min" value=""/>
                        <input type="hidden" id="nowsecs" name="secs" value=""/>
                        <input type="hidden" id="timestamp" name="timestamp" value=""/>
                        <div class="container col-lg-12 col-md-12 col-sm-12">
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="Date">Date</label>
                                <input type='text' name="date1" class="form-control"
                                       id='datetimepicker4' id="date"/>
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="DR">DR#</label>
                                <input type="text" name="dr" placeholder="" class="form-control" id="DR#" required>
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="Deliver">Delivered To</label>
                                <input type="text" name="deliver" class="form-control" list="Deliver" required>
                                <datalist id="Deliver">
                                    @foreach($custs as $cust)
                                        <option value="{{$cust['name']}}"></option>
                                    @endforeach
                                </datalist>
                            </div>

                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="dueDate">Due Date</label>
                                <input type='text' class="form-control" name="date2"
                                       id='datetimepicker5' id="date1"/>
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="gender">Articles</label>
                                <select id="articles" class="form-control" size="1">
                                    @foreach($articles as $article)
                                        @if($article['remain_qty'] <= 0)
                                        @else
                                            <option id = "{{$article['id']}}" value="{{ $article['article']['name'] }}">{{ $article['article']['name'] }}
                                                </option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="Qty">Qty</label>
                                <input type="text" value="" placeholder="kg" class="form-control"
                                       id="Qty"  data-toggle = "tool-tip" title = "Available quantity">
                            </div>

                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="unit">Unit Price</label>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="unit">
                            </div>
                            <div class="form-group col-lg-2 col-md-2 col-sm-2" style="margin-top: 22px;">
                                <button class="btn btn-{{Auth::user()->buttons}}" type="button"
                                        onclick="AddData()">
                                    <span class="glyphicon glyphicon-plus"></span>Add Item
                                </button>
                                <!-- <input id = "button" type = "button" value = " Add " onclick = "AddData()"> -->
                            </div>
                            <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="table table-responsive">
                                    <table class="table table-condensed" id="list">
                                        <thead id="tblHead">
                                        <tr>
                                            <th>Articles</th>
                                            <th>Qty. (Kg)</th>
                                            <th>Unit Price</th>
                                            <th>Total Price (Php)</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                                <label for="Total"><h3>Total Due (Php):</h3></label>
                                <label for="TAmount"><h3><input id="totalD" value="0.00" name="Tamount"
                                                                style='width: 100px;color: #000000; background-color: transparent;height: 100%;width: 100%; border: 0;padding: 0px 0px 0px;'/>
                                    </h3></label>
                                <div class="form-group pull-right">
                                    <a href="./vendor_articleOrder{{$article->outsource_id}}"
                                       class="btn btn-danger"><span
                                                class="glyphicon glyphicon-refresh"></span> Refresh All
                                    </a>
                                    <a onclick="ResetTable()" class="btn btn-warning"><span
                                                class="glyphicon glyphicon-trash"></span> Reset List</a>
                                    <button type="submit" class="btn btn-success"><span
                                                class="glyphicon glyphicon-shopping-cart"></span> Make a Delivery
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="info" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content" style="background-image: url('assets/img/123.jpg')">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-info-sign"></span> Order From Vendor Info
                    </h4>
                </div>
                <div class="table-responsive">
                    <table class="table table-hover col-lg-12">
                        <thead>
                        <th>Article</th>
                        <th>Quantity Left (kg)</th>
                        </thead>
                        <tbody>
                        @foreach($articles as $article)
                            <tr>
                                <td>{{$article['article']['name']}}</td>
                                @if($article->remain_qty == 0)
                                    <td><label class="label label-default">{{$article->remain_qty}}</label></td>
                                    <td><label class="label label-default">{{$article->unit_price}}</label></td>
                                @elseif($article->remain_qty > 0)
                                    <td><label class="label label-success">{{$article->remain_qty}}</label></td>
                                    <td><label class="label label-success">{{$article->unit_price}}</label></td>
                                @elseif($article->remain_qty < 0)
                                    <td><label class="label label-default">{{$article->remain_qty}}</label></td>
                                    <td><label class="label label-default">{{$article->unit_price}}</label></td>
                                @endif
                            </tr>
                        @endforeach
                        </tbody>

                    </table>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-success"><span
                                class="glyphicon glyphicon-ok"></span> OK
                    </button>
                </div>
            </div>
        </div>
    </div>

    @include('reusable.getClientTime')
    @include('reusable.AddData')
    <script type="text/javascript">
        document.getElementById("arrow1").innerHTML = "  Forward";
        $(document).ready(function () {
            $(function () {
                $('#datetimepicker4').datepicker("setDate", '1d');
                $('#datetimepicker5').datepicker("setDate", '1d');

            });
            $("#articles").change(function () {
                var resource_id = $(this).children(":selected").attr("id");
                // console.log(resource_id);
                /* ajax request in getting the  notifications*/
                $.ajax({
                    url: 'API/getQty',
                    type: "get",
                    data: {'id': resource_id},
                    success: function (data) {
                        console.log(data);
                        document.getElementById("Qty").value = data;
                    }
                });

            });
        });

    </script>
@endsection
