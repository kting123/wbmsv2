@extends('appv1')

@section('content')
    <style>
        #arrow1 {
            font-weight: bold;
        }
        #arrow {
            visibility: hidden;
        }
    </style>
    <div class="container">
        <div class="row">
            <div class="panel panel-{{Auth::user()->panels}}">
                <div class="panel-heading" style="height: 50px">
                    <h4 class="pull-left"><span class = "glyphicon glyphicon-shopping-cart"></span>  Purchased From Suppliers</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover col-lg-12">
                            <thead>
                            <th>Date Purchased</th>
                            <th>Supplier</th>
                            <th>DR #</th>
                            <th>Total Amount (Php)</th>
                            <th>Action</th>
                            </thead>
                            <tbody>
                            @foreach($vendors as $vendor)
                            <tr>

                                <td>{{ $vendor->date}}</td>
                                <td>{{ $vendor->ordered_from}}</td>
                                <td>{{ $vendor->dr}}</td>
                                <td>{{ $vendor->amount}}</td>
                                <?php $i=0 ; ?>
                                @foreach($vendor['article_resources'] as $query)
                                    <?php
                                        $i = $i + $query->remain_qty;
                                    ?>
                                @endforeach

                                @if($i > 0)
                                    <td><a href="./vendor_articleOrder{{$vendor->id}}"><span class="glyphicon glyphicon-shopping-cart"></span></a></td>

                                @else
                                    <td><span class="glyphicon glyphicon-minus-sign"></span></td>
                                     @endif
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="pull-right">
                        <a>
                            <button class="btn btn-primary" data-toggle="modal" data-target="#myModal">New
                                Purchase
                            </button>
                        </a>
                    </div>
                </div>
                <div class="panel-footer">
                  <em class ="pull-right">{!! $vendors->render() !!}</em>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content" style="background-image: url('assets/img/123.jpg')">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title"> <span class = "glyphicon glyphicon-shopping-cart"></span> New Purchase</h3>
                </div>
                <form type="hidden" method="post" action="./addPurchase" id="form1">
                    <div class="modal-body">
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                        <input type="hidden"  id = "nowdate" name="nowdate" value=""/>
                        <input type="hidden"  id = "nowhr" name="hr" value=""/>
                        <input type="hidden"  id = "nowmin" name="min" value=""/>
                        <input type="hidden"  id = "nowsecs" name="secs" value=""/>
                        <input type="hidden"  id = "timestamp" name="timestamp" value=""/>
                        <div class="container col-lg-12 col-md-12 col-sm-12 ">
                            <div class="form-group col-lg-3  col-md-3 col-sm-4">
                                <label for="Date">Date Purchased</label>
                                <input type='text' name="datePurchased" class="form-control"
                                       id='datetimepicker0'/>

                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-4">
                                <label for="OR">DR#</label>
                                <input type="text" name="dr#" value=""
                                       placeholder="" class="form-control" id="dr#" required>
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-4">
                                <label for="Sold">Supplier</label>
                                <input type="text" name="supplier" value="" placeholder=""
                                       class="form-control" id="supplier" required>
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-4">
                                <label for="Date">Due Date</label>

                                <input type='text' name="dueDate" class="form-control"
                                       id='datetimepicker3'/>

                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="articles1">Articles</label>
                                <select id="articles" class="form-control" size="1">
                                    @foreach($article as $art)
                                        <option>{{ $art->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="Qty">Qty</label>
                                <input type="text" value="" placeholder="kg" class="form-control"
                                       id="Qty">
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="unit1">Unit Price</label>
                                <input type="text" value="" placeholder="" class="form-control"
                                       id="unit">

                            </div>
                            <div class="form-group col-lg-2 col-md-2 col-sm-2" style="margin-top: 22px;">
                                <button class="btn btn-{{Auth::user()->buttons}}" type="button"
                                        onclick="AddData()">
                                    <span class="glyphicon glyphicon-plus"></span>Add Item
                                </button>
                                <!-- <input id = "button" type = "button" value = " Add " onclick = "AddData()"> -->
                            </div>
                            <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                    <div class="table table-responsive">
                                        <table class="table table-condensed" id="list">
                                            <thead id="tblHead">
                                            <tr>
                                                <th>Articles</th>
                                                <th>Qty. (Kg)</th>
                                                <th>Unit Price</th>
                                                <th>Total Price (Php)</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>
                                        </table>
                                    </div>
                                    <label for="Total"><h3>Total Amount (Php):</h3></label>
                                    <label for="TAmount"><h3><input readonly id="totalD"  value="0.00"
                                                                    name="Tamnt"
                                                                    style='width: 100px;color: #21305c; background-color: transparent;height: 100%;width: 100%; border: 0;padding: 0px 0px 0px;'/>
                                        </h3></label>
                                </div>
                            </div>
                        </div>

                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-danger"><span
                                        class="glyphicon glyphicon-remove"></span> Cancel
                            </button>
                            <button type="submit" class="btn btn-success"><span
                                        class="glyphicon glyphicon-save"></span> Confirm
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('reusable.getClientTime')
    @include('reusable.AddData')
    <script type="text/javascript">
        document.getElementById("arrow1").innerHTML = "  Vendors";
        $(document).ready(function () {

            $(function () {
                $('#datetimepicker0').datepicker("setDate", '1d');
                $('#datetimepicker3').datepicker("setDate", '1d');

            });
        });
    </script>
@endsection
