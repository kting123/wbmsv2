<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name = "_token" content = "{!! csrf_token() !!}">

    <title>Wb's Admin Monitoring App</title>

    <!-- Bootstrap Core CSS -->
    <link href="./assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="./assets/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="./assets/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="./assets/bower_components/morrisjs/morris.css" rel="stylesheet">
    <link rel="stylesheet" href="./assets/css/bootstrap-datepicker.min.css">
    <!-- Custom Fonts -->
    <link href="./assets/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script src="./assets/js/jquery.min.js"></script>

    <style>
        .panel {
            opacity: {{Auth::user()->opacity}};
        }

        /* Circle Avatar Styles */

        #image {
            border-radius: 50%;
            /* relative value for
                           adjustable image size */
        }
     .alert {
         z-index:1;
         position: absolute;
         left:40%;
     }
        .panel-body {
            padding: 2px;
        }
        .embed-container {
            position: relative;
            padding-bottom: 56.25%;
            height: 0;
            overflow: hidden;
            max-width: 100%;
            height: auto;
        }
        .embed-container iframe,
        .embed-container object,
        .embed-container embed {
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
        }

        .panel-heading {
            padding: 2px 2px;
            border-bottom: 1px solid transparent;
            border-top-right-radius: 1px;
            border-top-left-radius: 1px;
        }

        li > a {
            color: {{Auth::user()->lc}};
        }

        h1.page-header {
            color: {{Auth::user()->header}};
        }

        #wrapper {
            background-color: {{Auth::user()->wc}};
        }

        .modal-footer {
            margin-top: 5px;
            padding: 15px 15px 15px;
            text-align: right;
            border-top: 1px solid #e5e5e5;
        }

        .table td {
            font-size: 16px;
        }

        .table th {
            font-size: 16px;
        }

        .pagination {
            display: inline-block;
            padding-left: 0;
            margin: 0px 0;
            border-radius: 0px;
        }


        @if(Auth::user()->backimage == '1')
 #page-wrapper {
            background: url(assets/img/{{"b".Auth::user()->id}}.jpg) no-repeat center center fixed;
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            z-index: -1;
            margin-top:70px;

        }
        @media only screen and  (max-width:767px){
            #page-wrapper{
                margin-top:100px;
            }

        }

        @else
#page-wrapper {
            background-color: {{Auth::user()->bc}};
            -webkit-background-size: cover;
            -moz-background-size: cover;
            -o-background-size: cover;
            background-size: cover;
            z-index: -1;
            margin-top: 70px;

        }
        @media only screen and  (max-width:767px){
            #page-wrapper{
                margin-top:100px;
            }

        }

        @endif
    </style>
</head>
<body>

<div id="wrapper">
    <!-- Navigation -->
    <nav class="navbar navbar-{{Auth::user()->bi}} navbar-fixed-top" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a href="./" class="navbar-brand"> <span class="glyphicon glyphicon-tint"></span> WBMS</a>
            <a data-toogle="tooltip" title="Back" id="back" class="navbar-brand" href="#"> <span
                        class="glyphicon glyphicon-chevron-left"></span></a>
            <a data-toogle="tooltip" title="Forward" id="forward" class="navbar-brand" href="#"> <span
                        class="glyphicon glyphicon-chevron-right"></span></a>
            @include('flash::message')

        </div>
        <!-- /.navbar-header -->

        <ul class="nav navbar-top-links navbar-right">
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <sup id="count" style="font-size:medium"></sup><i class="fa fa-bell fa-fw"></i> Notifications <i
                            class="fa fa-caret-down"></i>
                </a>
                <ul id="alerts" class="dropdown-menu dropdown-messages">
                    <li class='center'><a class="text-center" href='./seeAllNoti'>
                            <strong>View all alerts</strong>
                            <i class="fa fa-angle-right"></i>
                        </a></li>
                </ul>
                <!-- /.dropdown-messages -->
            </li>
            <!-- /.dropdown -->
            <li class="dropdown">
                <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                    <img id="image" height="40" width="40" src="./assets/img/avatar{{Auth::user()->id}}.jpg">
                    {{ Auth::user()->name }} <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu dropdown-user">
                    <li>
                        <a href="./Adminprofile">
                            <i class="fa fa-user fa-fw"></i>
                            User Profile</a>
                    </li>
                    <li><a href="./change_user"> <i class="fa fa-random fa-fw"></i> Switch Account</a>
                    </li>
                    <li><a href="./Adminthemes{{Auth::user()->id}}"><i class="fa fa-gear fa-fw"></i> Themes</a>
                    </li>
                    <li class="divider"></li>
                    <li><a href="./auth/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                    </li>
                </ul>
                <!-- /.dropdown-user -->
            </li>
            <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        <div class="navbar-{{Auth::user()->bi}} sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse">
                <ul class="nav" id="side-menu">
                    <li style="margin-top:10px;">
                        <a href="./home"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-fw"></i> Meat Shop<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="./dr"><i class="fa fa-file-text-o fa-fw"></i> Delivery Receipts</a>
                            </li>
                            <li>
                                <a href="./or"><i class="fa fa-file-text-o fa-fw"></i> Official Receipts</a>
                            </li>
                            <li>
                                <a href="./own"><i class="fa fa-list fa-fw"></i> Farm</a>
                            </li>
                            <li>
                                <a href="./outsource"><i class="fa fa-list fa-fw"></i> Outsource</a>
                            </li>
                            <li>
                                <a href="./view_tabo_tabo"><i class="fa fa-list fa-fw"></i> Tabo Tabo</a>
                            </li>
                            <li>
                                <a href="#"><i class="fa fa-suitcase fa-fw"></i> Inventory<span class="fa arrow"></span></a>
                                <ul class="nav nav-third-level">
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#exportOverAll"><i
                                                    class="fa fa-print fa-fw"></i> Overall Inventory</a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#exportOwn"><i
                                                    class="fa fa-print fa-fw"></i> From Farm</a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#exportOutsource"><i
                                                    class="fa fa-print fa-fw"></i> From Outsources</a>
                                    </li>
                                    <li>
                                        <a href="#" data-toggle="modal" data-target="#exportTabo"><i
                                                    class="fa fa-print fa-fw"></i> From Tabo Tabo</a>
                                    </li>

                                </ul>
                                <!-- /.nav-third-level -->
                            </li>
                            <li>
                                <a href="./viewCust"><i class="fa fa-envelope-o fa-fw"></i> Customer Report</a>
                            </li>
                            <li>
                                <a href="./showStock"><i class="fa fa-flask fa-fw"></i> Current Stocks</a>
                            </li>
                            <li>
                                <a href="./articles"><i class="fa fa-list-alt fa-fw"></i> Manage Articles</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-fw"></i> Big A<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="./BigA_dr"><i
                                            class="fa fa-print fa-fw"></i> Delivery Receipts</a>
                            </li>
                            <li>
                                <a href="./BigA_or"><i
                                            class="fa fa-print fa-fw"></i> Official Receipts</a>
                            </li>
                            <li>
                                <a href="./BigA_stocks"><i class="fa fa-flask fa-fw"></i> Deliver Stocks</a>
                            </li>
                            <li>
                                <a href="./BigA_Supply"><i class="fa fa-flask fa-fw"></i> Order Supplies</a>
                            </li>
                        </ul>
                        <!-- /.nav-second-level -->
                    </li>

                    <li>
                        <a href="#"><i class="fa fa-sitemap fa-fw"></i> Diary<span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="#">Second Level Item</a>
                            </li>
                            <li>
                                <a href="#">Second Level Item</a>
                            </li>

                        </ul>
                        <!-- /.nav-second-level -->
                    </li>
                    <li>
                        <a href="./viewImports"><i class="fa fa-paperclip fa-fw"></i> Import Data</a>
                    </li>

                    <li>
                        <a href="./users"><i class="fa fa-users fa-fw"></i> Manage Users</a>
                    </li>
                    {{--<div class='embed-container'>--}}
                        {{--<iframe src='https://embed.spotify.com/?uri=https://play.spotify.com/album/1bU3wDVlDxju39cZxC2LY3' frameborder='0' allowtransparency='true'></iframe>--}}
                    {{--</div>--}}
                </ul>

            </div>
            <!-- /.sidebar-collapse -->
        </div>
        <!-- /.navbar-static-side -->

    </nav>


    <!-- /#page-wrapper -->
    <script type="text/javascript">
        $.ajaxSetup({
            headers:
            {
                'X-CSRF-Token': $('meta[name=_token]').attr('content')
            }
        });
    </script>
    <?php $ii = Auth::user()->name;?>
    @yield('content')
</div>


@include('modal_exports')
        <!-- /#wrapper -->
<!-- jQuery -->
<script src="./assets/js/bootstrap-datepicker.min.js"></script>

<!-- Bootstrap Core JavaScript -->
<script src="./assets/js/bootstrap.min.js"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src="./assets/bower_components/metisMenu/dist/metisMenu.min.js"></script>

<!-- Custom Theme JavaScript -->
<script src="./assets/dist/js/sb-admin-2.js"></script>
<!-- Notifications JavaScript -->
<script src="./assets/js/appNotifications.js"></script>
</body>

</html>
