@extends('appv20')
@section('content')
<div id="page-wrapper">
   <br>
        <div class="row">
                    <div class="panel panel-{{Auth::user()->panels}}">
                        <div class="panel-heading"><b><span class = "glyphicon glyphicon-bell"></span> User Logs</b> </div>
                        <div class="panel-body">
                          <ul style="font-size:larger; list-style-type: none;">
                            @foreach($nots as $notify)
                            <div class = "panel panel-{{Auth::user()->panels}}">
                                    @if($notify['user_id']==Auth::user()->id)
                                      <li>
                                      @if($notify->seen != Auth::user()->id)
                                      <i style = "color:yellow" class="fa fa-rss-square"></i>
                                      @else
                                      <i class="fa fa-rss-square"></i>
                                      @endif
                                        <b>You</b>,
                                          <a href="./viewlog{{$notify->id}}"  data-toogle ="tool-tip" title="see details">{{$notify->description}}.</a>
                                      <li class = "divider">
                                        &nbsp;    &nbsp;&nbsp;<i class="fa fa-spinner"></i>
                                        <!-- include blade which contain JavaScript -->
                                         @include('reusable.notimainfJS')
                                      </abbr><a  class = "pull-right" href = "./removeLog_{{$notify['id']}}" data-toogle ="tool-tip" title="remove log"><i class="fa  fa-times-circle"></i></a></li>
                                    @else
                                      <li>
                                        @if($notify->seen != Auth::user()->id)
                                        <i style = "color:yellow" class="fa fa-rss-square"></i>
                                        @else
                                        <i class="fa fa-rss-square"></i>
                                        @endif
                                         <b>{{$notify['user']['name']}}</b>,
                                         <a href="./viewlog{{$notify->id}}"  data-toogle ="tool-tip" title="see details">{{$notify->description}}.</a>
                                       </li>
                                      <li class = "divider">
                                        &nbsp;    &nbsp;&nbsp;<i class="fa fa-spinner"></i>
                                        <!-- include blade which contain JavaScript -->
                                         @include('reusable.notimainfJS')
                                      </abbr></abbr><a class = "pull-right"  href = "./removeLog_{{$notify['id']}}" data-toogle ="tool-tip" title="remove log"><i class="fa  fa-times-circle"></i></a></li>
                                      @endif
                                    </div>

                               @endforeach
                              </ul>
                              <a class ="btn btn-outline btn-{{Auth::user()->buttons}}"  href = "./clearAllNot"><span class = "glyphicon glyphicon-trash"></span> Clear All</a>
                              <div class="pull-right">
                              {!! $nots->render() !!}
                              </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
@endsection
