<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>WB's Monitoring System</title>


        <!-- Bootstrap Core CSS -->
        <link href="./assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- MetisMenu CSS -->
        <link href="./assets/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

        <!-- Timeline CSS -->
        <link href="./assets/dist/css/timeline.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="./assets/dist/css/sb-admin-2.css" rel="stylesheet">

        <!-- Morris Charts CSS -->
        <link href="./assets/bower_components/morrisjs/morris.css" rel="stylesheet">
        <link rel="stylesheet" href="./assets/css/bootstrap-datepicker.min.css">
        <!-- Custom Fonts -->
        <link href="./assets/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script src="./assets/js/jquery.min.js"></script>

    <![endif]-->
</head>
<style>
    .panel {
        opacity: {{Auth::user()->opacity}};
    }

    .modal-footer {
        margin-top: 5px;
        padding: 15px 15px 15px;
        text-align: right;
        border-top: 1px solid #e5e5e5;
    }

    .table td {
        font-size: 18px;
    }

    .table th {
        font-size: 18px;
    }
    .alert {
        z-index:1;
        position: absolute;
        left:40%;
    }

    @if(Auth::user()->backimage == '1')
     body {
        background: url(assets/img/{{"b".Auth::user()->id}}.jpg) no-repeat center center fixed;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
        width: 100%;
        height: 100%;
        z-index:-1;

    }

    @else
    body {
        background-color: {{Auth::user()->bc}};
        width: 100%;
         height: 100%;
        -webkit-background-size: cover;
        -moz-background-size: cover;
        -o-background-size: cover;
        background-size: cover;
         z-index:-1;

    }

    @endif
 a {
        color: black;
    }

    #wait {
        width: 35px;
        height: 35px;
        position: fixed;
        top: 80%;
        left: 50%;
        background-color: #dbf4f7;
        background-image: url('assets/img/wait.gif');
        background-repeat: no-repeat;
        z-index: 100;

        /* alpha settings for browsers */
        opacity: 0.9;
        filter: alpha(opacity=90);
        -moz-opacity: 0.9;
    }

.dropdown-menu > li.kopie > a {
   padding-left:5px;
}

.dropdown-submenu {
   position:relative;
}
.dropdown-submenu>.dropdown-menu {
  top:0;left:100%;
  margin-top:-6px;margin-left:-1px;
  -webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;
}

.dropdown-submenu > a:after {
 border-color: transparent transparent transparent #333;
 border-style: solid;
 border-width: 5px 0 5px 5px;
 content: " ";
 display: block;
 float: right;
 height: 0;
 margin-right: -10px;
 margin-top: 5px;
 width: 0;
}

.dropdown-submenu:hover>a:after {
   border-left-color:#555;
}

.dropdown-menu > li > a:hover, .dropdown-menu > .active > a:hover {
 text-decoration: underline;
}

@media (max-width: 767px) {

 .navbar-nav  {
    display: inline;
 }
 .navbar-default .navbar-brand {
   display: inline;
 }
 .navbar-default .navbar-toggle .icon-bar {
   background-color: #fff;
 }
 .navbar-default .navbar-nav .dropdown-menu > li > a {
   color: red;
   background-color: #ccc;
   border-radius: 4px;
   margin-top: 2px;
 }
  .navbar-default .navbar-nav .open .dropdown-menu > li > a {
    color: #333;
  }
  .navbar-default .navbar-nav .open .dropdown-menu > li > a:hover,
  .navbar-default .navbar-nav .open .dropdown-menu > li > a:focus {
    background-color: #ccc;
  }

  .navbar-nav .open .dropdown-menu {
    border-bottom: 1px solid white;
    border-radius: 0;
  }
 .dropdown-menu {
     padding-left: 10px;
 }
 .dropdown-menu .dropdown-menu {
     padding-left: 20px;
  }
  .dropdown-menu .dropdown-menu .dropdown-menu {
     padding-left: 30px;
  }
  li.dropdown.open {
   border: 0px solid red;
  }

}
 ul.nav li:hover > ul.dropdown-menu {
   display: block;
 }
 #navbar {
   text-align: center;
 }
 @media only screen and (max-width: 500px) {
    body{
        margin-bottom:230px;
    }

}

</style>
<body>
<nav class="navbar navbar-{{Auth::user()->bi}} navbar-static-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="navbar-header ">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                            data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>

                    </button>
                    <a  class="navbar-brand" href="./">
                    <img src="./assets/img/logo.png" alt="Home" width="25px" height="25px" /></a>
                  <a id="back" class="navbar-brand" href="javascript::void(0);"> <span class = "glyphicon glyphicon-arrow-left"></span></a>
                  <a id="forward" class="navbar-brand" href="javascript::void(0);"> <span class = "glyphicon glyphicon-arrow-right"></span></a>
                  <a id="refresh" class="navbar-brand" href="javascript::void(0);"> <span class = "glyphicon glyphicon-refresh"></span></a>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav navbar-left">
                        <li><a id="arrow" class="navbar-brand" href="./meatshop"></a></li>
                        <li><a id="arrow1" class="navbar-brand" href=""></a></li>
                    </ul>
                    @include('flash::message')

                    <ul class="nav navbar-nav navbar-right">
                        @if (Auth::guest())
                            <li><a href="{{ url('/auth/login') }}">Login</a></li>
                        @else
                            @if(Auth::user()->userType == 'Admin')
                            <li class="dropdown" id="meatshop">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><span
                                            class="glyphicon glyphicon-plane"></span> Quick Links
                                <span
                                        class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="./meatshop"> <span
                                                    class="glyphicon glyphicon-arrow-up"></span>
                                            Meatshop</a>
                                    </li>
                                    <li><a href="./view_tabo_tabo"> <span
                                                    class="glyphicon glyphicon-arrow-up"></span> Tabo Tabo</a>
                                    </li>
                                    <li><a href="./outsource"> <span
                                                    class="glyphicon glyphicon-arrow-up"></span> Outsources</a>
                                    </li>
                                    <li><a href="./own"> <span
                                                    class="glyphicon glyphicon-arrow-up"></span> Farm</a>
                                    </li>
                                    <li><a href="./dr"> <span
                                                    class="glyphicon glyphicon-arrow-up"></span> Delivery Receipts</a>
                                    </li>
                                    <li><a href="./or"> <span
                                                    class="glyphicon glyphicon-arrow-up"></span> Official Receipts</a>
                                    </li>
                                    <li><a href="./seeAllNoti"> <span
                                                    class="glyphicon glyphicon-info-sign"></span> User Logs</a>
                                    </li>



                                </ul>
                            </li>
                                <li class="dropdown" id="meatshop">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false"><span
                                                class="glyphicon glyphicon-briefcase"></span> Inventory
                                    <span
                                            class="caret"></span></a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="#" data-toggle="modal" data-target="#exportOverAll"> <span
                                                        class="glyphicon glyphicon-download-alt"></span> Overall
                                                Meatshop</a>
                                        </li>
                                        <li><a href="#" data-toggle="modal" data-target="#exportTabo"> <span
                                                        class="glyphicon glyphicon-download-alt"></span> Tabo Tabo</a>
                                        </li>
                                        <li><a href="#" data-toggle="modal" data-target="#exportOutsource"> <span
                                                        class="glyphicon glyphicon-download-alt"></span> Vendors</a>
                                        </li>
                                        <li><a href="#" data-toggle="modal" data-target="#exportOwn"> <span
                                                        class="glyphicon glyphicon-download-alt"></span> Farm</a>
                                        </li>

                                        <li><a href="./showStock" role="button"
                                               aria-expanded="false"><span class="glyphicon glyphicon-star"></span>
                                                Stocks </a></li>
                                                <li><a href="./viewCust"> <span
                                                                class="glyphicon glyphicon-info-sign"></span> Generate Customer Report</a>
                                                </li>

                                    </ul>
                                </li>
                                @elseif(Auth::user()->userType == 'user')
                                <li><a href="./baka"> <span
                                                class="glyphicon glyphicon-share-alt"></span> Resources</a>
                                </li>
                                <li><a href="./transactions"> <span
                                                class="glyphicon glyphicon-share-alt"></span> Orders</a>
                                </li>
                                <li><a href="./vendor" > <span
                                                class="glyphicon glyphicon-share-alt"></span> Vendors</a>
                                </li>
                            @endif

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                   aria-expanded="false"><span
                                            class="glyphicon glyphicon-user"></span> {{ Auth::user()->name }}
                                    <span
                                            class="caret"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    @if(Auth::user()->userType == 'Admin' || Auth::user()->admin == '1')
                                            <li><a href="./Adminprofile"><i class="fa fa-user fa-fw"></i> User Profile</a>
                                            </li>
                                            <li><a href="./change_user"> <i class="fa fa-random fa-fw"></i> Switch Account</a>
                                            </li>
                                            <li><a href="./Adminthemes{{Auth::user()->id}}"><i class="fa fa-gear fa-fw"></i> Themes</a>
                                            </li>
                                            <li class="divider"></li>
                                            <li><a href="./auth/logout"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                                            </li>

                                     @else
                                     <li><a href="./preferences"> <span
                                             class="glyphicon glyphicon-cog"></span> Preferences</a>
                                     </li>
                                     <li><a href="{{ url('/auth/logout') }}"> <span
                                                     class="glyphicon glyphicon-log-out"></span> Logout</a></li>
                                    @endif


                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <!--
                    <ul class="nav navbar-nav">
                            <li><a href="{{ url('/') }}">Home</a></li>
                    </ul>
                            --->

                </div>


            </div>
        </div>
    </div>
</nav>

@yield('content')

@include('modal_exports')

<!-- Scripts -->
<script>
    $('.alert').delay(3000).slideUp(300);
</script>
<script src="./assets/js/bootstrap.min.js"></script>
<script src="./assets/js/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">
$(function(){;
  $('#back').click(function(e){
      history.back();
  });
  $('#forward').click(function(e){
      history.forward();
  });
  $('#refresh').click(function(e){
      window.location.reload();
  });
})
</script>
</body>
</html>
