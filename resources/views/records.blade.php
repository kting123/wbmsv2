<style>
    label {

    }</style>
<div class="col-lg-10">
    <div class="panel panel-{{Auth::user()->panels}}">
        <div class="panel-heading"><h4>Manage Database Records</h4></div>
        <div class="panel-body">
            <br>
            <br>
            <div class=" form-group col-lg-4 col-lg-offset-1">

                <form type="hidden" method="post" action="./backup" id="backup"/>
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>

                <button class="btn btn-{{Auth::user()->buttons}} btn-lg" id="back"><span
                            class="glyphicon glyphicon-hdd"></span> Backup Database Records
                </button>
                </form>
            </div>

            <div class="form-group col-lg-4 col-lg-offset-1">
                <button class="btn btn-{{Auth::user()->buttons}} btn-lg" data-toggle="modal" data-target="#delete"><span
                            class="glyphicon glyphicon-trash"></span> Clean Up Database Records
                </button>
            </div>
        </div>
        <center></center>
        <br>
        <br>
    </div>
</div>

<div class="modal fade" id="delete" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><span
                            class="glyphicon glyphicon-trash"></span> Clean Up Database Records</h4>
            </div>
            <form type="hidden" method="post" action="./deleteDB" id="deltethis"/>
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="modal-body">
                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                    <h3>Are you sure you want to do this? </h3>
                    <strong>Note : </strong><label style="color:red;"> All Records will deleted except for users data,
                        ,Articles Name and customers info.</label>
                </div>
                <div hidden style="background-image: url('assets/img/wait.gif');width: 35px;height: 35px;" id="wait">
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-danger"><span
                            class="glyphicon glyphicon-remove"></span>
                    Cancel
                </button>
                <a id="trash" class="btn btn-{{Auth::user()->buttons}}"><span
                            class="glyphicon glyphicon-trash"></span>
                    Clean Up
                </a>
            </div>
        </div>
        </form>
    </div>
</div>

<script>
    $(function () {
        $('#trash').click(function () {
            $('#wait').show();
            $('#deltethis').submit();
        });
        $('#back').click(function () {
            $('#wait1').show();
            $('#backup').submit();
        });
    })
</script>