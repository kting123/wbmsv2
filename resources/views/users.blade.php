@extends('appv20')

@section('content')
    <style>
        #table {
            border: 0;
        }

        #table tr {
            display: flex;
        }

        #table td {
            flex: 1 auto;
            width: 2px;
        }

        #table thead tr:after {
        }

        #table thead th {
            flex: 1;
        }

        #table tbody {
            display: block;
            width: 100%;
            overflow-y: auto;
            height: 300px;
        }

    </style>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" style="color:{{Auth::user()->h}}">Manage Users <span data-toggle="modal"
                                                                                             data-target="#editHeader"
                                                                                             class="glyphicon glyphicon-edit"></span>
                </h1></div>
        </div>
        @include('editHeaderModal')
        <div class="row">
            <div class="panel panel-{{Auth::user()->panels}}">
                <div class="panel-heading">
                    <h4> Add, Edit and remove users</h4>
                </div>

                <div class="table table-responsive">
                    <table class="table table-hover" id="table">
                        <thead>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>User Type</th>
                        <th>Action</th>
                        </thead>
                        <tboby>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{$user['id']}}</td>
                                    <td>{{$user['name']}}</td>
                                    <td>{{$user['email']}}</td>
                                    <td>{{$user['userType']}}</td>
                                    <td><a data-toggle="modal" href="#" data-target="#edit{{$user['id']}}">
                                   <span
                                           class="glyphicon glyphicon-edit"></span></a> &nbsp; | &nbsp;<a href="#"
                                                                                                          data-toggle="modal"
                                                                                                          data-target="#delete{{$user['id']}}"> <span
                                                    class="glyphicon glyphicon-remove-circle"></span></a> | <a
                                                data-toggle="modal"
                                                href="#"
                                                data-target="#refresh{{$user['id']}}">
                                            &nbsp;&nbsp;<span
                                                    class="glyphicon glyphicon-refresh"></span></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tboby>

                    </table>

                </div>
                <div class="panel-footer">
                    <button class="pull-right btn btn-{{Auth::user()->buttons}} btn-lg" data-toggle="modal"
                            data-target="#addarticle"><i class="fa fa-plus-circle fa-1x">
                        </i> Add New User
                    </button>
                </div>
            </div>
        </div>
    </div>
    @foreach($users as $user)

        <div class="modal fade" id="delete{{$user['id']}}" role="dialog">
            <div class="modal-dialog modal-md">
                <!-- Modal content-->
                <div class="modal-content"
                >
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Remove User</h4>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post"
                              action="./delete_user{{$user['id']}}" id="form1"/>
                        <input type="hidden" name="_token" value="{{csrf_token() }}"/>
                        <div class="container col-lg-12  col-md-12">
                            <h5> Are you sure you want to delete this User?</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-success"><span
                                    class="glyphicon glyphicon-ok"></span>
                            Ok
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>
        <div class="modal fade" id="refresh{{$user['id']}}" role="dialog">
            <div class="modal-dialog modal-md">
                <!-- Modal content-->
                <div class="modal-content"
                >
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Reset Password</h4>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post"
                              action="./reset_user{{$user['id']}}" id="form1"/>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="container col-lg-12  col-md-12">
                            <h5> Are you sure you want to reset the password of this user to
                                (<b>123</b>)?</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-success"><span
                                    class="glyphicon glyphicon-ok"></span>
                            Ok
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>

        <div class="modal fade" id="edit{{$user['id']}}" role="dialog">
            <div class="modal-dialog modal-md">
                <!-- Modal content-->
                <div class="modal-content"
                >
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Edit User</h2>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post"
                              action="./edit_user{{$user['id']}}" id="form1"/>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="container col-lg-12  col-md-12">
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="DR">Name</label>
                                <input type="text" name="name"
                                       value="{{$user['name']}}"
                                       placeholder="name here"
                                       class="form-control">
                            </div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="DR">Email</label>
                                <input type="text" name="email"
                                       value="{{$user['email']}}"
                                       placeholder="name here"
                                       class="form-control">
                            </div>
                            <div class="form-group col-lg-4 col-md-4">
                                <label for="DR">UserType</label>
                                <select type="text" name="userType"

                                        placeholder="name here"
                                        class="form-control">
                                    <option value="{{$user['userType']}}">{{$user['userType']}}</option>
                                    <option value="Admin">Admin</option>
                                    <option value="user">User</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-success"><span
                                    class="glyphicon glyphicon-ok"></span>
                            Save
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>

    @endforeach
    <div class="modal fade" id="addarticle" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"
                            data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add New User</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" enctype="multipart/form-data" role="form" method="POST"
                          action="./add_user" id="form1"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="form-group">
                        <label class="col-md-4 control-label">Name</label>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">E-Mail Address</label>
                        <div class="col-md-6">
                            <input type="email" class="form-control" name="email" value="{{ old('email') }}">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">UserType</label>
                        <div class="col-md-6">
                            <select class="form-control" name="userType">
                                <option value="user">User</option>
                                <option value="Admin">Admin</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-4 control-label">Password</label>
                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-4 control-label">Confirm Password</label>
                        <div class="col-md-6">
                            <input type="password" class="form-control" name="password_confirmation">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-success"><span
                                    class="glyphicon glyphicon-ok"></span>
                            Register
                        </button>
                    </div>
                    </form>
                </div>

            </div>

        </div>
    </div>

@endsection
