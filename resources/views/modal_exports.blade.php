<div class="modal fade" id="exportOutsource" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Vendors Inventory</h4>
            </div>
            <form type="hidden" method="post" action="./exportOutsource" id="form1"/>
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="modal-body">
                <div class="form-group col-lg-6 col-md-6 col-sm-6">
                    <label for="">Type</label>
                    <select type='text' name="type" class="form-control">
                        <option value="0">Detailed Report</option>
                        <option value="2">General Report by Range</option>
                        <option value="1">General Report by Month</option>
                    </select>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                    <label for="">Specify Range</label>
                    <input type='text' name="date_from" class="form-control" placeholder="from"
                           id='datetimepicker1111' required/>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                    <label for="">&copy;</label>;
                    <input type='text' name="date_to" class="form-control" placeholder="to"
                           id='datetimepicker1112' required/>
                </div>
                <div class="form-group col-lg-12 col-md-12">
                    <p><b>Note : </b> This will generate an excel file of from Out Source <a href="#"
                                                                                             data-toggle="tooltip"
                                                                                             title="Detailed Report - Include breakdowns, General Report By Range -  Generate the records within that range , General Report By Month -  Generate within the range and separate by month tru sheets"><span
                                    class="glyphicon glyphicon-info-sign"></span></a></p>
                </div>

            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-danger"><span
                            class="glyphicon glyphicon-remove"></span>
                    Cancel
                </button>
                <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                            class="glyphicon glyphicon-export"></span>
                    Export
                </button>
            </div>
        </div>
        </form>
    </div>

</div>

<div class="modal fade" id="exportOwn" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Farm Inventory</h4>
            </div>
            <form type="hidden" method="post" action="./exportOwn" id="form1"/>
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="modal-body">
                <div class="form-group col-lg-6 col-md-6 col-sm-6">
                    <label for="">Type</label>
                    <select type='text' name="type" class="form-control">
                        <option value="0">Detailed Report</option>
                        <option value="2">General Report by Range</option>
                        <option value="1">General Report by Month</option>
                    </select>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                    <label for="">Specify Range</label>
                    <input type='text' name="date_from" class="form-control" placeholder="from"
                           id='datetimepicker1110' required/>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                    <label for="">&copy;</label>;
                    <input type='text' name="date_to" class="form-control" placeholder="to"
                           id='datetimepicker1107' required/>
                </div>
                <div class="form-group col-lg-12 col-md-12">
                    <p><b>Note : </b> This will generate an excel file of from farm <a href="#"
                                                                                       data-toggle="tooltip"
                                                                                       title="Detailed Report - Include breakdowns, General Report By Range -  Generate the records within that range , General Report By Month -  Generate within the range and separate by month tru sheets"><span
                                    class="glyphicon glyphicon-info-sign"></span></a></p>
                </div>

            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-danger"><span
                            class="glyphicon glyphicon-remove"></span>
                    Cancel
                </button>
                <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                            class="glyphicon glyphicon-export"></span>
                    Export
                </button>
            </div>
        </div>
        </form>
    </div>

</div>

<div class="modal fade" id="exportTabo" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Tabo Tabo Inventory</h4>
            </div>
            <form type="hidden" method="post" action="./exportTabo" id="form1"/>
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="modal-body">
                <div class="form-group col-lg-6 col-md-6 col-sm-6">
                    <label for="">Type</label>
                    <select type='text' name="type" class="form-control">
                        <option value="0">Detailed Report</option>
                        <option value="2">General Report by Range</option>
                        <option value="1">General Report by Month</option>
                    </select>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                    <label for="">Specify Range</label>
                    <input type='text' name="date_from" class="form-control" placeholder="from"
                           id='datetimepicker1109' required/>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                    <label for="">&copy;</label>;
                    <input type='text' name="date_to" class="form-control" placeholder="to"
                           id='datetimepicker1108' required/>
                </div>
                <div class="form-group col-lg-12 col-md-12">
                    <p><b>Note : </b> This will generate an excel file of Tabo Tabo <a href="#"
                                                                                       data-toggle="tooltip"
                                                                                       title="Detailed Report - Include breakdowns, General Report By Range -  Generate the records within that range , General Report By Month -  Generate within the range and separate by month tru sheets"><span
                                    class="glyphicon glyphicon-info-sign"></span></a></p>
                </div>

            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-danger"><span
                            class="glyphicon glyphicon-remove"></span>
                    Cancel
                </button>
                <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                            class="glyphicon glyphicon-export"></span>
                    Export
                </button>
            </div>
        </div>
        </form>
    </div>
</div>

<div class="modal fade" id="exportOverAll" role="dialog">
    <div class="modal-dialog modal-sm">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">OverAll Inventory For Meatshop</h4>
            </div>
            <form type="hidden" method="post" action="./genAll" id="form1"/>
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="modal-body">
                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                    <label for="">Specify Range</label>
                    <input type='text' name="date_from" class="form-control" placeholder="from"
                           id='datetimepicker1114' required/>
                </div>
                <div class="form-group col-lg-12 col-md-12 col-sm-12">
                    <label for="">&copy;</label>;
                    <input type='text' name="date_to" class="form-control" placeholder="to"
                           id='datetimepicker1113' required/>
                </div>

            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-danger"><span
                            class="glyphicon glyphicon-remove"></span>
                    Cancel
                </button>
                <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                            class="glyphicon glyphicon-export"></span>
                    Generate
                </button>
            </div>
        </div>
        </form>
    </div>

</div>
<script>
    $(function () {
        $('#datetimepicker1110').datepicker();
        $('#datetimepicker1109').datepicker();
        $('#datetimepicker1108').datepicker();
        $('#datetimepicker1107').datepicker();
        $('#datetimepicker1111').datepicker();
        $('#datetimepicker1112').datepicker();
        $('#datetimepicker1113').datepicker();
        $('#datetimepicker1114').datepicker();
        //   $('[data-toggle = "tool-tip"]').tooltip();
    });
</script>