@extends('appv1')

@section('content')
<div class = "container">
  <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header" style = "color:{{Auth::user()->h}}">User Dashboard <span data-toggle="modal" data-target="#editHeader"class = "glyphicon glyphicon-edit"></span></h1></div>
  </div>
  @include('editHeaderModal')

<div class="row">
  <div class="col-lg-4 col-md-6">
      <div class="panel panel-yellow">
          <div class="panel-heading">
              <div class="row">
                  <div class="col-xs-3">
                      <i class="fa fa-rocket fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                      <div class="huge">Resources</div>
                      <div><h4>add parts, tabo, from farm</h4></div>
                  </div>
              </div>
          </div>
          <a href="./baka">
              <div class="panel-footer" >
                  <span class="pull-left">Dive Me In!</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
              </div>
          </a>
      </div>
  </div>

  <div class="col-lg-4 col-md-6">
      <div class="panel panel-primary">
          <div class="panel-heading">
              <div class="row">
                  <div class="col-xs-3">
                      <i class="fa fa-shopping-cart fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                      <div class="huge">Orders</div>
                      <div><h4>delivery,walked in</h4></div>
                  </div>
              </div>
          </div>
            <a href="./transactions">
              <div class="panel-footer">
                  <span class="pull-left">Dive Me In!</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
              </div>
          </a>
      </div>
  </div>
  <div class="col-lg-4 col-md-6">
      <div class="panel panel-green">
          <div class="panel-heading">
              <div class="row">
                  <div class="col-xs-3">
                        <i class="fa fa-retweet fa-5x"></i>
                  </div>
                  <div class="col-xs-9 text-right">
                      <div class="huge">Vendors</div>
                      <div><h4>forward order, add items</h4></div>
                  </div>
              </div>
          </div>
              <a href="./vendor">
              <div class="panel-footer">
                  <span class="pull-left">Dive Me In!</span>
                  <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                  <div class="clearfix"></div>
              </div>
          </a>
      </div>
  </div>
</div>
</div>
@endsection
