<style>
    @media screen  and (min-width: 778px) {
        #pan {
            height: auto;
        }

    }
</style>

<div class="col-lg-10 col-md-10 col-sm-10">
    <div id="pan" class="panel panel-{{Auth::user()->panels}}">
        <div class="panel-heading"><h4>Manage Themes</h4></div>
        <div class="panel-body">
            <form type="hidden" method="post" action="./editThemes" id="form1" enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <div class="container col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="sold1">Panels</label>
                        <select name="panel" value="" placeholder="" class="form-control">
                            <option value="{{Auth::user()->panels}}">{{Auth::user()->panels}}</option>
                            <option value="green" style="color: green;">Green</option>
                            <option value="red" style="color: red;">Red</option>
                            <option value="yellow" style="color: yellow;">Yellow</option>
                            <option value="primary" style="color: #337ab7;">Blue</option>
                            <option value="success" style="color: #5cb85c;">Light Green</option>
                            <option value="info" style="color: #5bc0de;">SkyBlue</option>
                            <option value="default" style="color: #ccc;">White Grey</option>
                            <option value="warning" style="color: #f0ad4e;">Light Orange</option>
                            <option value="danger" style="color: #c9302c;">Orange</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="sold1">Buttons</label>
                        <select name="button" value="" placeholder="" class="form-control">
                            <option value="{{Auth::user()->buttons}}">{{Auth::user()->buttons}}</option>
                            <option value="primary" style="color: #337ab7;">Blue</option>
                            <option value="success" style="color: #5cb85c;">Light Green</option>
                            <option value="info" style="color: #5bc0de;">Sky Blue</option>
                            <option value="default" style="color: #ccc;">White Grey</option>
                            <option value="warning" style="color: #f0ad4e;">Light Orange</option>
                            <option value="danger" style="color: #c9302c;">Orange</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="sold1">Labels</label>
                        <select name="label" value="" placeholder="" class="form-control">
                            <option value="{{Auth::user()->labels}}">{{Auth::user()->labels}}</option>
                            <option value="primary" style="color: #337ab7;">Blue</option>
                            <option value="success" style="color: #5cb85c;">Light Green</option>
                            <option value="info" style="color: #5bc0de;">Sky Blue</option>
                            <option value="default" style="color: #ccc;">White Grey</option>
                            <option value="warning" style="color: #f0ad4e;">Light Orange</option>
                            <option value="danger" style="color: #c9302c;">Orange</option>
                        </select>
                    </div>
                    <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label for="sold1">Nav Bar</label>
                        <select name="bi" value="" placeholder="" class="form-control">
                            <option value="{{Auth::user()->bi}}">{{Auth::user()->bi}}</option>
                            <option value="inverse" style="color: black;">Inverse(Dark)</option>
                            <option value="default" style="color: #ccc;">Default(White)</option>

                        </select>
                    </div>
                </div>
                <br> <br>
                <div class="container col-lg-12 col-md-12 col-sm-12">
                    <div class="form-group col-lg-3 col-md-3 col-sm-3" style="margin-left:10px; ">
                        @if(Auth::user()->backimage =='0')
                            <label> <input type="radio" name="pref" value="0" checked/>Background Color</label>
                            <input type='color' class="form-control" value="{{Auth::user()->bc}}" name="bc"/>
                    </div>

                    <div class="form-group col-lg-3 col-md-2 col-sm-2">
                        <label>Transparency</label>
                        <input id = "opacitySlide" type='range' data-toggle="tool-tip" min='0.3' max = '1' step='0.025'
                               title="higher value means more opaque or clear. Lower value is more transparent."
                               class="form-control" value="{{Auth::user()->opacity}}" name="opa"/><em id = "opacityCount"style = "font-size:17px;"></em>
                    </div>
                    <div class="form-group col-lg-3 col-md-3 col-sm-3">
                        <label><input type="radio" name="pref" value="1"/>Background Image</label>
                        <input type='file' class="form-control" id="file" name="file"/>
                    </div>
                    @elseif(Auth::user()->backimage =='1')
                        <label> <input type="radio" name="pref" value="0"/>Background Color</label>
                        <input type='color' class="form-control" value="{{Auth::user()->bc}}" name="bc"/>
                </div>

                <div class="form-group col-lg-3 col-md-2 col-sm-2">
                    <label>Transparency</label>
                    <input id = "opacitySlide" type='range' data-toggle="tool-tip" min='0.3' max = '1' step='0.025'
                           title="higher value means more opaque or clear. Lower value is more transparent."
                           class="form-control" value="{{Auth::user()->opacity}}" name="opa"/><em id = "opacityCount"style = "font-size:17px;"></em>
                </div>
                <div class="form-group col-lg-3 col-md-3 col-sm-3">
                    <label><input type="radio" name="pref" value="1" checked/>Background Image</label>
                    <input type='file' class="form-control" id="file" name="file"/>
                </div>
                @endif
                <center>
                    <br>
                    <button style="margin-bottom: 50px;" class="btn btn-{{Auth::user()->buttons}} btn-lg"
                            type="submit">
                        Save
                        Changes
                    </button>
                </center>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#opacityCount').html($('#opacitySlide').val());
        $('#opacitySlide').mousemove(function () {
            $('#opacityCount').html($(this).val());
        });
    });
</script>
