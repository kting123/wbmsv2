@extends('appv20')

@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" style="color:{{Auth::user()->h}}">Manage Customers <span data-toggle="modal"
                                                                                                 data-target="#editHeader"
                                                                                                 class="glyphicon glyphicon-edit"></span>
                </h1></div>
        </div>
        @include('editHeaderModal')
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading">
                        <h4>Major Customers</h4>
                    </div>

                    <div class="panel-body ">
                        <div class="table-responsive" style="y-overflow:auto;">
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12 results">
                                <thead>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($customers as $customer)
                                    @if($customer['customer_type']=='1')
                                        <tr>
                                            <td>{{$customer['id']}}</td>
                                            <td>{{$customer['name']}}</td>
                                            <td><a data-toggle="modal" data-target="#generate{{$customer['id']}}"
                                                   href="#"><span class="glyphicon glyphicon-export"></span></a> |
                                                <a data-toggle="modal" data-target="#edit{{$customer['id']}}"
                                                   href="#"><span class="glyphicon glyphicon-edit"></span></a> |
                                                <a data-toggle="modal" data-target="#remove{{$customer['id']}}"
                                                   href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>

                            <script>
                                $(document).ready(function () {
                                    $(function () {
                                        $('#datetimepicker4').datepicker();
                                        $('#datetimepicker5').datepicker();
                                    });
                                });
                            </script>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading">
                        <h4>Other Vendor Customers</h4>
                    </div>

                    <div class="panel-body ">
                        <div class="table-responsive"  style="y-overflow:auto;">
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12 results">
                                <thead>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($customers as $customer)
                                    @if($customer['customer_type']=='2')
                                    <tr>
                                        <td>{{$customer['id']}}</td>
                                        <td>{{$customer['name']}}</td>
                                        <td>
                                            <a data-toggle="modal" data-target="#edit{{$customer['id']}}" href="#"><span
                                                        class="glyphicon glyphicon-edit"></span></a> |
                                            <a data-toggle="modal" data-target="#remove{{$customer['id']}}"
                                               href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                    </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>

                            <script>
                                $(document).ready(function () {
                                    $(function () {
                                        $('#datetimepicker4').datepicker();
                                        $('#datetimepicker5').datepicker();
                                    });
                                });
                            </script>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12 col-lg-12">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading">
                        <h4>Walked In Customer</h4>
                    </div>

                    <div class="panel-body ">
                        <div class="table-responsive"  style="y-overflow:auto;">
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12 results">
                                <thead>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($customers as $customer)
                                    @if($customer['customer_type']=='0')
                                    <tr>
                                        <td>{{$customer['id']}}</td>
                                        <td>{{$customer['name']}}</td>
                                        <td>
                                            <a data-toggle="modal" data-target="#edit{{$customer['id']}}" href="#"><span
                                                        class="glyphicon glyphicon-edit"></span></a> |
                                            <a data-toggle="modal" data-target="#remove{{$customer['id']}}"
                                               href="#"><span class="glyphicon glyphicon-trash"></span></a></td>
                                    </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>

                            <script>
                                $(document).ready(function () {
                                    $(function () {
                                        $('#datetimepicker4').datepicker();
                                        $('#datetimepicker5').datepicker();
                                    });
                                });
                            </script>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

    @foreach($customers as $Customer)
        <div class="modal fade" id="generate{{$Customer['id']}}" role="dialog">
            <div class="modal-dialog modal-sm">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Generate Report for {{$Customer['name']}}</h4>
                    </div>
                    <form type="hidden" method="post" action="./genCust{{$Customer['id']}}" id="form1"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <div class="modal-body">

                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label for="">Specify Range</label>
                            <input type='text' name="date_from" class="form-control" placeholder="from"
                                   id='datetimepickers{{$Customer['id']}}' required/>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label for=""></label>
                            <input type='text' name="date_to" class="form-control" placeholder="to"
                                   id='datetimepickerss{{$Customer['id']}}' required/>
                        </div>

                    </div>

                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span>
                            Cancel
                        </button>
                        <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-export"></span>
                            Generate
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="modal fade" id="remove{{$Customer['id']}}" role="dialog">
            <div class="modal-dialog modal-sm">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Remove Article</h4>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post"
                              action="./delete_customer{{$Customer['id']}}" id="form1"/>
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                        <div class="container col-lg-12  col-md-12">
                            <h5> Are you sure you want to delete this Customer?</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-success"><span
                                    class="glyphicon glyphicon-ok"></span>
                            Ok
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>


        <div class="modal fade" id="edit{{$Customer['id']}}" role="dialog">
            <div class="modal-dialog modal-sm">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Edit Customer</h2>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post"
                              action="./edit_customer{{$Customer['id']}}" id="form1"/>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="container col-lg-12  col-md-12">
                            <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                <label for="DR">Customer Name</label>
                                <input type="text" name="customer"
                                       value="{{$Customer['name']}}"
                                       placeholder="name here"
                                       class="form-control">
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-success"><span
                                    class="glyphicon glyphicon-ok"></span>
                            Save
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>
    @endforeach
    <script>
        $(document).ready(function () {
            @foreach($customers as $Customer)
            $('#datetimepickers{{$Customer['id']}}').datepicker();
            $('#datetimepickerss{{$Customer['id']}}').datepicker();
            @endforeach
        });
    </script>
@endsection
