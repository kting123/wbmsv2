@extends('appv20')
@section('content')
        <!-- Morris Chart JavaScript -->
<script src="./assets/bower_components/raphael/raphael-min.js"></script>
<script src="./assets/bower_components/morrisjs/morris.min.js"></script>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header" style="color:{{Auth::user()->h}}">BIG A <span data-toggle="modal"
                                                                                      data-target="#editHeader"
                                                                                      class="glyphicon glyphicon-edit"></span>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    @include('editHeaderModal')
            <!-- /.row -->
    <div class="row">
        <div class="col-lg-4 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-book fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div id="or" class="huge">---</div>
                            <div><h4>Official Reciepts</h4></div>
                        </div>
                    </div>
                </div>
                <a href="./BigA_or">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-4 col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div id="dr" class="huge">---</div>
                            <div><h4>Delivery Receipts</h4></div>
                        </div>
                    </div>
                </div>
                <a href="./BigA_dr">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa  fa-list-alt fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div id="farm" class="huge">---</div>
                            <div><h4>Order Supplies</h4></div>
                        </div>
                    </div>
                </div>
                <a href="./BigA_Supply">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-ambulance fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div id="stocks" class="huge">---</div>
                            <div><h4>Deliver Stocks</h4></div>
                        </div>
                    </div>
                </div>
                <a href="./BigA_stocks">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-medkit fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div id="stocks" class="huge">---</div>
                            <div><h4>Inventory of Medicines </h4></div>
                        </div>
                    </div>
                </div>
                <a href="./BigA_inventory">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-md-4">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-medkit fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div id="stocks" class="huge">---</div>
                            <div><h4>Manage Medicines</h4></div>
                        </div>
                    </div>
                </div>
                <a href="./manage_medicines">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="panel panel-{{Auth::user()->panels}}">
        <div class="panel-heading">
            Revenue from Farm, Tabo tabo & Outsources Per month
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div id="meatshop"></div>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
    <input type="hidden" id="Walking" value="">
    <input type="hidden" id="delivery" value="">
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-{{Auth::user()->panels}}">
                <div class="panel-heading">
                    Statistics : Delivery Vs. Walked In
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="sales"></div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-6">
            <div class="panel panel-{{Auth::user()->panels}}">
                <div class="panel-heading">
                    Statistics : Farm Vs. Tabo Vs. Outsource
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="expenseVSprofit"></div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->
        <!-- /.col-lg-6 -->
    </div>
</div>
<script type="text/javascript">

</script>
@endsection
