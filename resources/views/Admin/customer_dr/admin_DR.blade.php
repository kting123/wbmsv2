@extends('appv20')
@section('content')
<div id="page-wrapper">
   <br>

   <div class="row">
         <div class="panel panel-{{Auth::user()->panels}}">
            <div class="panel-heading">
               <h4>Delivery Receipts <i data-toggle="modal" data-target="#export" class="pull-right fa fa-download fa-2x"></i></h4></div>
                  <div class="panel-body">
               <div class="table-responsive">
                  <table class="table table-hover col-lg-12 col-md-12 col-xs-12">
                     <thead>
                        <th>Date</th>
                        <th>DR#</th>
                        <th>Delivered to</th>
                        <th>Amount</th>
                        <th>Status</th>
                        <th>Due Date</th>
                        <th>Action</th>
                     </thead>
                     <tbody>
                       <?php $dued = 0; $nearDue = 0; $safeDue = 0; ?>
                        @foreach($order as $data)
                        <tr>
                           @if($data['status']=="cancelled")
                           <td><strike>{{$data['date']}}<strike></td>
                           <td><a href="./dr_order{{$data['id']}}" data-toggle="tool-tip" title="see details"><strike>{{$data['dr']}}</strike></a></td>
                           <td><strike>{{$data['customer']['name']}}<strike></td>
                           <td><strike>₱{{number_format($data['total_due'], 2)}}</strike></td>
                           <td><strike><label class="label label-danger">{{$data['status']}}</label></strike></td>
                           <td><strike>{{$data['due_date']}}</strike></td>
                           @else
                           <td>{{$data['date']}}</td>
                           <td><a class="btn btn-outline btn-{{Auth::user()->buttons}}"href="./dr_order{{$data['id']}}" data-toggle="tool-tip" title="see details">{{$data['dr']}}</a></td>
                           <td>{{$data['customer']['name']}}</td>
                           <td>₱{{number_format($data['total_due'], 2)}}</td>
                           <?php
                              $due = (int) strtotime($data['due_date']);
                              $due_warning = $due - 432000;
                              ?>
                           @if($data['status']=="paid")
                           <td><button class="btn btn-outline btn-success" style="font-size:medium">{{$data['status']}}</button></td>
                           <td><button class="btn btn-outline btn-success" style="font-size:medium">{{$data['due_date']}}</button></td>
                           @else
                           @if(($data['status']=="pending" || $data['status']=="partial") && $due <=$now)
                           <td><button class="btn btn-outline btn-danger" style="font-size:medium">{{$data['status']}}</button></td>
                           <td><button class="btn btn-outline btn-danger" style="font-size:medium">{{$data['due_date']}}</button></td>
                           <?php $dued++;?>

                           @elseif(($data['status']=="pending" || $data['status']=="partial") && $due_warning <=$now )
                           <td><button class="btn btn-outline btn-warning" style="font-size:medium">{{$data['status']}}</button></td>
                           <td><button class="btn btn-outline btn-warning" style="font-size:medium">{{$data['due_date']}}</button></td>
                            <?php $nearDue++;?>
                           @else
                           <td><button class="btn btn-outline btn-warning" style="font-size:medium">{{$data['status']}}</button></td>
                           <td><button class="btn btn-outline btn-primary" style="font-size:medium">{{$data['due_date']}}</button></td>
                           <?php $safeDue++;?>
                           @endif
                           @endif
                           @endif
                           <td><label href="#" data-toggle="modal" data-target="#edit{{$data['id']}}"><span class="glyphicon glyphicon-edit"></span></label> |
                              <label href="#" data-toggle="modal" data-target="#remove{{$data['id']}}"><span class="glyphicon glyphicon-trash"></span></label>
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
                  <script>$(document).ready(function(){$(function(){$("#datetimepicker4").datepicker();$("#datetimepicker5").datepicker()})});</script>
               </div>

            </div>
            <div class="panel-footer">
              <label  style="font-size:medium"> Due : {{$dued}} </label> &nbsp;&nbsp;&nbsp;
                <label  style="font-size:medium"> Near to due : {{$nearDue}}</label>&nbsp;&nbsp;&nbsp;
                 <label style="font-size:medium"> Safe from due : {{$safeDue}}</label>
                 <label style = "margin-bottom: 0px;" class="pull-right">{!! $order->render() !!}</label>
         </div>
            </div>
         </div>

      </div>
   </div>
@foreach($order as $data)
<div class="modal fade" id="edit{{$data['id']}}" role="dialog">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h2 class="modal-title">Edit Transaction</h2>
         </div>
         <div class="modal-body">
            <form type="hidden" method="post" action="./save_dr/{{$data['id']}}" id="form1"/>
               <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
               <div class="container col-lg-12 col-md-12">
                  <div class="form-group col-lg-3 col-md-6">
                     <label for="Date">Date</label>
                     <input type='text' class="form-control" id='datetimepicker135{{$data['id']}}' name="date" value="{{$data['date']}}"/>
                  </div>
                  <div class="form-group col-lg-3 col-md-6">
                     <label for="DR">DR No.</label>
                     <input type="text" value="{{$data['dr']}}" placeholder="" class="form-control" name="dr">
                  </div>
                  <div class="form-group col-lg-3 col-md-6">
                     <label for="Deliver">Delivered to</label>
                     <input type="text" value="{{$data['customer']['name']}}" placeholder="" class="form-control" disabled name="customer">
                  </div>
                  <div class="form-group col-lg-3 col-md-6">
                     <label for="gender">Amount</label>
                     <input type="text" value="{{$data['total_due']}}" placeholder="" class="form-control" name="amount" disabled>
                  </div>
                  <div class="form-group col-lg-3 col-md-6">
                     <label for="gender">Status</label>
                     <select class="form-control" name="status">
                        <option>{{$data['status']}}</option>
                        <option>pending</option>
                        <option>paid</option>
                        <option>partial</option>
                        <option>cancelled</option>
                     </select>
                  </div>
                  <div class="form-group col-lg-3 col-md-6">
                     <label for="Qty">Due Date</label>
                     <input type="text" id='datetimepicker134{{$data['id']}}' value="{{$data['due_date']}}" class="form-control" name="due_date">
                  </div>
               </div>
         </div>
         <div class="modal-footer">
         <button data-dismiss="modal" class="btn btn-outline btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel
         </button>
         <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span class="glyphicon glyphicon-save"></span>
         Save
         </button>
         </div>
      </div>
      </form>
   </div>
</div>
<div class="modal fade" id="remove{{$data['id']}}" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h2 class="modal-title">Remove Transaction</h2>
         </div>
         <div class="modal-body">
            <form type="hidden" method="post" action="./delete_order/{{$data['id']}}" id="form1"/>
               <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
               <div class="container col-lg-12 col-md-12">
                  <h5> Are you sure you want to delete this transaction?</h5>
               </div>
         </div>
         <div class="modal-footer">
         <button data-dismiss="modal" class="btn btn-outline btn-danger"><span class="glyphicon glyphicon-remove"></span> Cancel
         </button>
         <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span class="glyphicon glyphicon-ok"></span>
         Confirm
         </button>
         </div>
      </div>
      </form>
   </div>
</div>
<script>
$(document).ready(function()
{
  $("#datetimepicker134{{$data['id']}}").datepicker();
  $("#datetimepicker135{{$data['id']}}").datepicker();
});
  </script>
@endforeach
<div class="modal fade" id="export" role="dialog">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">Export DR's</h4>
         </div>
         <div class="modal-body">
            <form type="hidden" method="post" action="./exportDR" id="form1"/>
               <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
               <div class="modal-body">
                  <div class="form-group col-lg-6 col-md-6 col-sm-6">
                     <label for="">Type</label>
                     <select type='text' name="type" class="form-control">
                        <option value="2">Paid</option>
                        <option value="0">Partially Paid</option>
                        <option value="1">Pending</option>
                        <option value="3">Cancel Transactions</option>
                     </select>
                  </div>
                  <div class="form-group col-lg-3 col-md-3 col-sm-3">
                     <label for="">Specify Range</label>
                     <input type='text' name="date_from" class="form-control" placeholder="from" id='datetimepicker1' required/>
                  </div>
                  <div class="form-group col-lg-3 col-md-3 col-sm-3">
                     <label for="">&copy;</label>
                     <input type='text' name="date_to" class="form-control" placeholder="to" id='datetimepicker2' required/>
                  </div>
                  <input type="hidden" name="all" value="0"/>
                  <label><input style="margin-left:18px" name="all" type="checkbox" value="1">Generate
                  All</label>
                  <div class="form-group col-lg-10 col-md-8">
                     <p><b>Note : </b> This will generate an excel file of Delivery Receipts.</p>
                  </div>
               </div>
         </div>
         <div class="modal-footer">
         <button data-dismiss="modal" class="btn btn-outline btn-danger"><span class="glyphicon glyphicon-remove"></span>
         Cancel
         </button>
         <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span class="glyphicon glyphicon-export"></span>
         Export
         </button>
         </div>
      </div>
      </form>
   </div>
</div>
</div>
<script>
$(document).ready(function()
{
  $("#datetimepicker1").datepicker();
  $("#datetimepicker2").datepicker();
});
  </script>
@endsection
