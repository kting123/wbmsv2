@extends('appv20')

@section('content')
    <div id="page-wrapper">
       <br>

        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading"><h4><span class = "glyphicon glyphicon-info-sign"></span> Transaction Breakdown</h4></div>
                    <div class="panel-body">
                        <label data-toggle="modal" data-target="#editList" class="label label-{{Auth::user()->labels}}"
                               style="font-size:medium;"> Order List &nbsp;<i
                                    class="fa fa-edit fa-fw"></i></label>

                        </br>

                        <div class="container col-lg-12 col-md-12 col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-hover col-lg-12 col-md-12 col-xs-12">
                                    <thead>
                                    <th>Medicines</th>
                                    <th># of Bottles</th>
                                    <th>Unit Price</th>
                                    <th>Amount</th>
                                    </thead>
                                    <tbody>
                                   
                                        <tr>
                                            <td>Paracetamol (<a href = "#" data-toggle="modal" data-target = "#"> 65 </a>)</td>
                                            <td>5</td>
                                            <td>200.00</td>
                                            <td>₱1000.00</td>
                                        </tr>

                                    
                                    <tr>

                                        <td></td>
                                        <td></td>
                                        <td><strong>Total Dues</strong></td>
                                        <td><u><strong>₱1000.00</strong></u>
                                        </td>
                                    </tr>
                                    <tbody>
                                </table>

                            </div>

                            
                                <div style="padding-top: 20px;"><a>
                                        <button class="btn btn-outline btn-{{Auth::user()->buttons}} btn-lg" data-toggle="modal"
                                                data-target="#pay"><span
                                                    class="glyphicon glyphicon-credit-card"></span> Post
                                            Payment

                                        </button>
                                    </a></div>
                        </div>

                    </div>

                    <pre style="text-align: center">Customer : <b>Ann</b>  |  Date : <b>01/01/2016</b>  |  DR : <b>0005</b>  |  OR : <b>0009</b>  |  Status : <b>Partial</b> </pre>
                </div>

            </div>
        </div>

    </div>
    </div>
    </div>
    </div>
    </div>
    </div>
    <div class="modal fade" id="pay" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Post Payment</h4>
                </div>
                <div class="modal-body">
                    <form type="hidden" method="post" action="./postpayment/" id="form1"/>
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                    <input type="hidden"  id = "nowdate" name="nowdate" value=""/>
                    <input type="hidden"  id = "nowhr" name="hr" value=""/>
                    <input type="hidden"  id = "nowmin" name="min" value=""/>
                    <input type="hidden"  id = "nowsecs" name="secs" value=""/>
                    <input type="hidden"  id = "timestamp" name="timestamp" value=""/>
                    <div class="table-responsive">
                        <td><h5><b>History of Payments &nbsp;<i class="fa fa-edit fa-fw"></i></b></h5></td>
                        <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                            <thead style="border-bottom:solid 2px;">
                            <th></th>
                            <th>Date of Payment</th>
                            <th>Amount (Php)</th>
                            <th>OR #</th>
                            <th>Bank Name</th>
                            <th>Check No.</th>
                            <th>Balance (Php)</th>
                            </thead>
                            <tbody>
                           
                                <tr>
                                    <td></td>
                                    <td>01/01/2016</td>
                                    <td>500.00</td>
                                    <td>0009</td>
                                    <td>BPI</td>
                                    <td>8908472</td>
                                    <td></td>
                                    
                                </tr>
                            
                            <tr style="border-top:solid 2px;">
                                <td style="background-color: #c6cad5;"><h5><b>Current Balance</b></h5></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"><b><br>500.00</b></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-lg-12">
                        <label class="checkbox-inline" style="margin-left: 10px;"> <input type="checkbox" id="cheque"> Pay in Cheque</input>
                        </label>
                    </div>
                    <div class="form-group col-lg-12 col-md-12">
                        <div class="col-md-4 col-lg-4">
                            <label>Amount</label>
                            <input placeholder="" value="500.00" name="amount"
                                   class="form-control" required>
                            </input>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            <label>OR No.</label>
                            <input placeholder="or" name="or" class="form-control" required>
                            </input>
                        </div>
                        <div class="col-md-4 col-lg-4">
                            <label>Date Of Payment</label>
                            <input placeholder="" name="dateofcheck" id="date2" class="form-control">
                            </input>
                        </div>
                        <div class="col-md-4 col-lg-4" id="bank" hidden>
                            <label>Bank</label>
                            <input placeholder="Bank Name" name="bank" id="banks" class="form-control" disabled>
                            </input>
                        </div>
                        <div class="col-md-4 col-lg-4" id="checkNum" hidden>
                            <label>Check No.</label>
                            <input placeholder="check no" name="checkno" id="check" class="form-control" disabled>
                            </input>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-outline btn-danger"><span class="glyphicon glyphicon-remove"></span>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                class="glyphicon glyphicon-ok"></span> Post
                        Payment
                    </button>
                </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Item Details</h4>
                </div>
                <div class="modal-body">
                  <p>
                    This item is coming from <b></b> which listed as part of: <b><br> Farm Body Number : 
                          <br>Tabo Tabo Body Number : <br>
                          Vendor Id  :</b>
                  </p>
            </div>
        </div>
    </div>
    </div>


    <div class="modal fade" id="editList" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit List</h4>
                </div>
                <form type="hidden" method="post" action="./edit_order" id="form1"/>
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <input type="hidden" id="nowdate" name="nowdate" value=""/>
                <input type="hidden" id="nowhr" name="hr" value=""/>
                <input type="hidden" id="nowmin" name="min" value=""/>
                <input type="hidden" id="nowsecs" name="secs" value=""/>
                <input type="hidden" id="timestamp" name="timestamp" value=""/>
               
                <?php $i = 0; ?>
                
                    <tr>
                        <td>
                            <select style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'  name="articleD{{$i}}" class="form-control">
                                <option value=""></option>
                               
                                    <option value=""></option>
                                
                            </select></td>
                        <td>
                            <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' onkeyup="ComputeTotal{{$i}}()" class="form-control" value=""
                                   id="qtyD{{$i}}"
                                   name="qtyD{{$i}}" type="text"/>
                            <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'  class="form-control"
                                   />
                            <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'  class="form-control"
                                   />
                        </td>
                        <td>
                            <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' onkeyup="ComputeTotal{{$i}}()" class="form-control"
                                   value="" id="unitPD{{$i}}"
                                   name="unitPD{{$i}}" type="text"/>
                        </td>
                        <td>
                            <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' class="form-control" value="" name="TotalPD{{$i}}"
                                   id="TotalPD{{$i}}"
                                   type="text" readonly/>
                        </td>
                        <td>
                            <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' class="form-control" value="" name="articlesFrD{{$i}}"
                                   id="articlesFrD{{$i}}"
                                   type="text" readonly/></td>
                        <td>
                            <label id="" href = "#"data-toggle="tooltip" class = "label label-danger"title="Remove Item">X</label>
                        </td>
                    </tr>

                    <script>
                        //                                        var total = 0;
                        function ComputeTotal{{$i}}() {
                            var qty = document.getElementById('qtyD{{$i}}').value;
                            var unit_price = document.getElementById('unitPD{{$i}}').value;
                            var amount = qty * unit_price;
//                                            console.log(amount);
//                                            total = Number(total) - Number(amount);
//                                            document.getElementById('total').value = total;
                            document.getElementById('TotalPD{{$i}}').value = amount;

                        }
                    </script>
                    <?php $i++; ?>
               
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                </tbody>
                </table>
                <div class="col-lg-6">
                    <input type="hidden" disabled id = "amount_due">
                    <b></b> <input type="hidden" disabled style  = "font-size:20px;"class="form-control" name = "total" id = "totalD" value = ""type="text">
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button data-dismiss="modal" class="btn btn-danger">Discard</button>
        <button class="btn btn-outline btn-{{Auth::user()->buttons}}">Save</button>
    </div>
    </form>
    </div>
    </div>
    </div>
    
    <script>
        $(document).ready(function () {
            $(function () {
                $('#date2').datepicker("setDate", '1d');
            });
        });

        $(document).ready(function () {
            $('#cheque').click(function () {
                if ($(this).is(":checked")) {
                    // check = 1;
                    $('#bank').show();
                    $('#checkNum').show();
                    document.getElementById("banks").disabled = false;
                    document.getElementById("check").disabled = false;

                }
                else {
                    $('#bank').hide();
                    $('#checkNum').hide();
                    document.getElementById("banks").disabled = true;
                    document.getElementById("check").disabled = true;
                    //  check = 0;
                }
            });
        });

    </script>
@endsection
