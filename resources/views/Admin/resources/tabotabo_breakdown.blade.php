@extends('appv20')

@section('content')
<div id="page-wrapper">
   <br>

        <div class="row">
            <div class="col-md-12 col-sm-12 col-lg-12">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading"><b>Income Statement Per Item <i data-toggle="modal" data-target="#detail" class="pull-right fa fa-info-circle fa-2x"></i></b></div>
                    <div class="panel-body" style="background-image: url('assets/img/.jpg')">
                        <div class="table table-responsive">
                            <table class="table table-hover">
                                <thead style="padding: 0px 0px 0px 0px">
                                <th></th>
                                <th>Article <br>Expense</th>
                                <th>Total Qty<br>(Kls)</th>
                                <th>Remain Qty<br>(Kls)</th>
                                <th>Sales Qty<br>(Kls)</th>
                                <th>Unit Price<br>(₱)</th>
                                <th>Sales<br>(₱)</th>
                                <th>Total<br>(₱)</th>
                                </thead>
                                <tbody>

                                <tr>
                                    <td><b>Sales : </b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <?php $gross = 0; ?>
                                @foreach($tabos['article_resources'] as $article)
                                    <tr>
                                        <td></td>
                                        @if($article['unit_price']==0)
                                            <td><a href="#" data-toggle="modal"
                                                   data-target="#{{$article['id']}}">{{$article['article']['name']}}</a>  </td>
                                        @else
                                            <td>{{$article['article']['name']}}</td>
                                        @endif
                                        <td>{{$article['quantity']}}</td>
                                        <td>{{$article['remain_qty']}}</td>
                                        <td>{{$article['sales_qty']}}</td>
                                        @if($article['unit_price']==0)
                                            <td>-</td>
                                        @else
                                            <td>{{number_format($article['unit_price'],2)}}</td>
                                        @endif


                                        <td>{{number_format($article['total_sales'],2)}}</td>
                                        <?php $gross = $gross + $article['total_sales']; ?>
                                        <td></td>
                                    </tr>
                                @endforeach
                                <tr style="border-top:solid 2px;">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td>
                                        <b>{{number_format($gross,2)}}</b>
                                    </td>

                                </tr>


                                <tr>
                                    <td><b>Expenses : </b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <?php $expenses = 0; ?>
                                @foreach($tabos['expenses'] as $expense)
                                    <tr>
                                        <td></td>
                                        <td>{{$expense['name']}}</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>{{number_format($expense['amount'],2)}}</td>
                                        <td></td>
                                        <?php $expenses = $expenses + $expense['amount']; ?>

                                    </tr>
                                @endforeach

                                <tr style="border-top:solid 2px;">
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Total Expenses</td>

                                    <td>
                                        <b>- {{number_format($expenses,2)}}</b>
                                    </td>
                                </tr>

                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Price:</td>
                                    <td>
                                        <b>- {{number_format($tabos['price'],2)}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>Gross Profit :</td>
                                    <td>
                                        <b>+ {{number_format($gross-$expenses,2)}}</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a style = "margin:10px 10px 10px 10px;"href="./printTaboReport{{$tabos['id']}}"
                                       class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                                class="glyphicon glyphicon-export"></span> Export File</a></td>
                                    <td><button style = "margin:10px 10px;" data-toggle="modal" class="btn btn btn-outline btn-{{Auth::user()->buttons}}"
                                            data-target="#addArticle"
                                            area-label="add tabo tabo"><span
                                                class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                                        Articles
                                    </button></td>
                                    <td><button style = "margin:10px 10px;" data-toggle="modal" class="btn btn btn-outline btn-{{Auth::user()->buttons}} "
                                            data-target="#addExpense"
                                            area-label="add tabo tabo"><span
                                                class="glyphicon glyphicon-minus" aria-hidden="true"></span> Expenses
                                    </button></td>
                                    <td>  @if($tabos['status']==0)
                                          <button style = "margin:10px 10px;" class="btn btn btn-outline btn-{{Auth::user()->buttons}}  " data-toggle="modal"
                                                  data-target="#addOrder"
                                                  aria-label="add order"><span
                                                      class="glyphicon glyphicon-shopping-cart"
                                                      aria-hidden="true"></span>
                                              Delivery
                                          </button>
                                      @endif</td>
                                    <td></td>
                                    <td></td>
                                    <td><b>Net : </b></td>
                                    <td style="border-top:solid 2px;">
                                        <b>₱ {{number_format($gross-$expenses-$tabos['price'],2)}}</b>
                                    </td>
                                </tr>

                                </tbody>

                            </table>
                        </div>

                    </div>

                </div>
                        </div>

            </div>


    <div class="modal fade" id="addOrder" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-shopping-cart"></span>New Delivery</h4>
                </div>
                <div class="modal-body">
                    <form type="hidden" method="post" action="./addVendorOrder" id="form1"/>
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                    <input type="hidden" name="id" value="{{$tabos['id']}}"/>
                    <input type="hidden"  id = "nowdate" name="nowdate" value=""/>
                    <input type="hidden"  id = "nowhr" name="hr" value=""/>
                    <input type="hidden"  id = "nowmin" name="min" value=""/>
                    <input type="hidden"  id = "nowsecs" name="secs" value=""/>
                    <input type="hidden"  id = "timestamp" name="timestamp" value=""/>

                    <div class="container col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="Date">Date</label>
                            <input type='text' name="date1" class="form-control" id='datetimepicker14' required />
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="DR">PO#:</label>
                            <input type="text" name="dr1" value="" placeholder=""
                                   class="form-control" required />
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="sold1">Delivered to</label>
                            <input class ="form-control" list = "customer" name = "customer2" required/>
                            <datalist  id="customer">
                                @foreach($cust as $cust)
                                <option value="{{$cust['name']}}">
                                @endforeach
                            </datalist>
                        </div>

                        <div class="container col-lg-4 col-md-4 col-sm-4">
                            <label for="dueDate">Due Date</label>
                            <input type='text' class="form-control" id='datetimepicker15' name="duedate1" required />
                        </div>

                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="Qty2">Qty:</label>
                            <input type="text" placeholder="" value="{{$tabos['mw']}}" class="form-control"
                                   name="qty1" required />
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="unit2">Price Per Kilo:</label>
                            <input class ="form-control" list = "price" name = "amount1" required>
                            <datalist  id="price">
                                    <option value="180">
                            </datalist>
                        </div>
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                            <label for="unit2">Voucher No.</label>
                            <input type ="text" class ="form-control" name = "voucher">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-danger"><span
                                class="glyphicon glyphicon-remove"></span>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                class="glyphicon glyphicon-save"></span> Confirm
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
    <div class="modal fade" id="addExpense" role="dialog">
        <div class="modal-dialog modal-md">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-minus-sign"></span> Add Expense (s)</h4>
                </div>
                <div class="modal-body">
                    <form type="hidden" method="post" action="./addExpense" id="form1">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <input type="hidden" name="id" value="{{$tabos['id']}}"/>
                        <div id="parentDIV" class="container col-lg-12 col-md-12 col-sm-12">

                            <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                <label for="Date">Expense Name</label>
                                <input type='text' name="name0" placeholder="name here.."
                                       class="form-control"/>
                            </div>
                            <div class="form-group col-lg-6 col-md-6 col-sm-6">
                                <label for="DR">Amount</label>
                                <input type="text" name="amount0" placeholder="amount here"
                                       class="form-control"/>
                            </div>

                        </div>
                        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<label onclick="AddData()"
                                                                    class="label label-info"
                                                                    style="font-size: medium"><span
                                    class="glyphicon glyphicon-plus"></span> Add Field
                        </label>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-danger"><span
                                        class="glyphicon glyphicon-remove"></span>
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                        class="glyphicon glyphicon-save"></span>
                                Confirm
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addArticle" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title"><span class="glyphicon glyphicon-plus-sign"></span> Add Articles</h4>
                </div>
                <div class="modal-body">
                    <form type="hidden" method="post" action="./addArticles" id="form1"/>
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                    <input type="hidden" name="id" value="{{$tabos['id']}}"/>
                    <div id="parentDIV1" class="container col-lg-12 col-md-12 col-sm-12">
                        <div class="form-group col-lg-12 col-md-12 col-sm-12">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="Date">Article</label>
                            <select type='text' name="" id="article" class="form-control"
                                    placeholder="articles">
                                @foreach($articles as $article)
                                    @if($article['name']=='Carcass')
                                    @else
                                        <option value="{{$article['name']}}">{{$article['name']}}</option>
                                    @endif
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-2">
                            <label for="DR">Qty</label>
                            <input type="text" name="" id="qty" value="" placeholder="quantity"
                                   class="form-control">
                        </div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-2">
                            <label for="DR">Total Prize</label>
                            <input type="text" name="" id="unit" value="" placeholder="price"
                                   class="form-control">
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="DR">Availability</label>
                            <select type="text" name="" id="available"
                                    class="form-control">
                                <option>Sold</otpion>
                                <option>Available</otpion>
                            </select>
                        </div>
                        <br>
                        <br>

                        <div class="form-group col-lg-1 col-md-1 col-sm-1" style="margin-top: 3px;">
                            <a onclick="AddData1()" class="btn btn-info"><span
                                        class="glyphicon glyphicon-plus"></span>
                            </a>
                        </div>

                    </div>


                    <div class="container col-lg-12">
                        <table class="table table-condensed" id="list1">
                            <thead id="tblHead">
                            <tr>
                                <th>Articles</th>
                                <th>Qty</th>
                                <th>Unit Price</th>
                                <th>Total (₱)</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>

                            </tbody>
                        </table>
                    </div>

                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-danger"><span
                                class="glyphicon glyphicon-remove"></span>
                        Cancel
                    </button>

                    <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span
                                class="glyphicon glyphicon-save"></span>
                        Confirm
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
    <br><br>

    @foreach($tabos['article_resources'] as $article)
        @if($article['unit_price']==0)
            <div class="modal fade" id="{{$article['id']}}" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><span class="glyphicon glyphicon-info-sign"></span> Item Orders
                                History - {{$article['article']['name']}}</h4>
                        </div>
                        <div class="modal-body">
                            <div class="table table-responsive">
                                <table class="table table-bordered">
                                    <thead>
                                    <tr>
                                        <th>DR</th>
                                        <th>OR</th>
                                        <th>Date</th>
                                        <th>Article</th>
                                        <th>Quantity</th>
                                        <th>Unit Price</th>
                                        <th>Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($article['orderlist'] as $order)
                                        @if($order['order']['status'] =='cancelled')
                                            <tr>
                                                <td><strike><a data-toggle="tool-tip" title="see details"
                                                               href=".\dr_order{{$order['order']['id']}}">{{$order['order']['dr']}}</a></strike>
                                                </td>
                                                <td><strike><a data-toggle="tool-tip" title="see details"
                                                               href=".\dr_order{{$order['order']['id']}}">{{$order['order']['or']}}</a></strike>
                                                </td>
                                                <td><strike>{{$order['order']['date']}}</strike></td>
                                                <td><strike>{{$order['article']['name']}}</strike></td>
                                                <td><strike>{{$order['qty']}}</strike></td>
                                                <td><strike>{{number_format($order['unit_price'],2)}}</strike></td>
                                                <td><strike>{{number_format($order['amount'],2)}}</strike></td>
                                            </tr>
                                        @else
                                            <tr>
                                                <td><a data-toggle="tool-tip" title="see details"
                                                       href=".\order_dr{{$order['order']['id']}}">{{$order['order']['dr']}}</a>
                                                </td>
                                                <td><a data-toggle="tool-tip" title="see details"
                                                       href=".\order_or{{$order['order']['id']}}">{{$order['order']['or']}}</a>
                                                </td>
                                                <td>{{$order['order']['date']}}</td>
                                                <td>{{$order['article']['name']}}</td>
                                                <td>{{$order['qty']}}</td>
                                                <td>{{number_format($order['unit_price'],2)}}</td>
                                                <td>{{number_format($order['amount'],2)}}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td><b>TOTAL</b></td>
                                        <td style="border-top:2px solid;">
                                            <b>₱{{number_format($article['total_sales'],2)}}</b></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-{{Auth::user()->buttons}}"><span
                                        class="glyphicon glyphicon-ok"></span> Ok
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        @else
        @endif
    @endforeach
<div class="modal fade" id = "detail">
  <div class="col-md-4 col-lg-4 col-sm-4 col-lg-offset-4">
      <div class="panel panel-{{Auth::user()->panels}}">
          <div class="panel-heading"><b>Basic Info</b></div>
          <div class="panel-body" style="background-image: url('assets/img/.jpg')">
          <pre> Date  : <b>{{$tabos['date']}}</b>
Tattoo:
Eartag no.:
Body# : <b>{{$tabos['id']}}</b>
LW    : <b>{{$tabos['lw']}}</b> Kgs.
SW    : <b>{{$tabos['sw']}}</b> Kgs.
MW    : <b>{{$tabos['mw']}}</b> Kgs.
Via   : <b>{{$tabos['customer']['name']}}</b>
          </pre>
          </div>
      </div>
  </div>
 <button data-dismiss = "modal" name="button">X</button>
</div>
  @include('reusable.getClientTime')
    <script>
        $(function () {
            $('#datetimepicker15').datepicker("setDate","1d");
            $('#datetimepicker14').datepicker("setDate","1d");
        });

        var count = 0;
        function AddData() {
            count++;
            $('<div class="form-group col-lg-6"><input type="text"  placeholder="name here.." class="form-control" name="name' + count + '"/> </div> <div class="form-group col-lg-6"><input type="text"  placeholder="amount here" class="form-control" name="amount' + count + '"/> </div>').appendTo("#parentDIV");
        }
        var unit = 0;
        var i = 0;
        function AddData1() {
            var rows = "";
            var totalP = "";
            var articles = document.getElementById("article").value;
            var qty = document.getElementById("qty").value;
            var tot_prize = document.getElementById("unit").value;
            var status = document.getElementById("available").value;
            //var totalD = document.getElementById("totalD").innerHTML

            unit = tot_prize/qty;
            var unit = unit.toFixed(2);
            rows += "<tr><td><input name=article" + i + " value='" + articles + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=qty" + i + " value='" + qty + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=unit_price" + i + " value='" + unit + "'style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=total_due" + i + " value='" + tot_prize + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=status" + i + " value='" + status + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td></tr>";
            $(rows).appendTo("#list1 tbody");
            i++;
        }

    </script>
@endsection
