@extends('appv20')

@section('content')
<div id="page-wrapper">
   <br>

        <div class="row">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading">
                        <h4>From Other Vendors</h4>
                    </div>
                    <div class="panel-body" >
                        <div class="table table-responsive">
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12"
                                   xmlns="http://www.w3.org/1999/html">
                                <thead>
                                <th>Date</th>
                                <th>DR</th>
                                <th>Ordered From</th>
                                <th>Status</th>
                                <th>Due Date</th>
                                <th>Amount</th>
                                <th>Current Sales</th>
                                <th>Gross Profit</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                  <?php $dued = 0; $nearDue = 0; $safeDue = 0; ?>
                                @foreach($outsource as $data)
                                    <tr>
                                        <td>{{$data['date']}}</td>
                                        <td><a data-toggle="tool-tip" title="See Details"
                                               class="btn btn-outline btn-{{Auth::user()->buttons}}"
                                               href="./outsource_reports{{$data->id}}">{{$data['dr']}}</a></td>
                                        <td>{{$data['ordered_from']}}</td>
                                        <?php
                                        $due = (int)strtotime($data['due_date']);
                                        $now = (int)strtotime(date("m/d/y", time()));
                                        $due_warning = $due - 432000;
                                        ?>
                                        @if($data['status']=="paid")
                                            <td><button class="btn btn-outline btn-success"
                                                       style="font-size:medium;">{{$data['status']}}</button></td>
                                            <td><button class="btn btn-outline btn-success"
                                                       style="font-size:medium;">{{$data['due_date']}}</button>
                                            </td>
                                        @else
                                            @if(($data['status']=="pending" || $data['status']=="partial") && $due <= $now)
                                                <td><button class="btn btn-outline btn-warning"
                                                           style="font-size:medium;">{{$data['status']}}</button>
                                                </td>
                                                <td><button class="btn btn-outline btn-danger"
                                                           style="font-size:medium;">{{$data['due_date']}}</button>
                                                </td>
                                                <?php $dued++;?>
                                            @elseif(($data['status']=="pending" || $data['status']=="partial") && $due_warning <=$now )
                                                <td><button class="btn btn-outline btn-warning"
                                                           style="font-size:medium;">{{$data['status']}}</button>
                                                </td>
                                                <td><button class="btn btn-outline btn-warning"
                                                           style="font-size:medium;">{{$data['due_date']}}</button>
                                                </td>
                                                <?php $nearDue++;?>
                                            @else
                                                <td><button class="btn btn-outline btn-warning"
                                                           style="font-size:medium;">{{$data['status']}}</button>
                                                </td>
                                                <td><button class="btn btn-outline btn-primary"
                                                           style="font-size:medium;">{{$data['due_date']}}</button>
                                                </td>
                                                <?php $safeDue++;?>
                                            @endif
                                        @endif
                                        <td>₱ {{number_format($data['amount'],2)}}</td>
                                        <?php $sales = 0; ?>
                                        @foreach($data['article_resources'] as $resource)
                                            <?php $sales = $sales + $resource['total_sales']; ?>
                                        @endforeach
                                        <td>₱ {{number_format($sales,2)}}</td>
                                        <td>₱ {{number_format($sales-$data['amount'],2)}}</td>
                                        <td><a href="#editTabo{{$data['id']}}" data-toggle="modal"
                                               data-target="#editTabo{{$data['id']}}"><span
                                                        class="glyphicon glyphicon-edit"></span></a> | <a
                                                    href="#remove{{$data['id']}}" data-toggle="modal"
                                                    data-target="#remove{{$data['id']}}"><span
                                                        class="glyphicon glyphicon-trash"></span></a></td>

                                @endforeach
                                </tbody>
                            </table>

                        </div>

                    </div>
                    <!-- <label class = "label label-danger">Total Quantity :
                     Ordered Quantity :
                     Remaining Quantity : </label>
                     --->
                     <div class="panel-footer">
                       <label  style="font-size:medium"> Due : {{$dued}} </label> &nbsp;&nbsp;
                        <label  style="font-size:medium"> Near to due : {{$nearDue}}</label>&nbsp;&nbsp;
                       <label style="font-size:medium"> Safe from due : {{$safeDue}}</label>
                    <label class="pull-right">{!! $outsource->render() !!}</label>

                     </div>
                </div>
            </div>
    </div>
    @foreach($outsource as $data)
        <div class="modal fade" id="editTabo{{$data['id']}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content"
                >
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit </h4>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post" action="./edit_outsource/{{$data['id']}}"
                              id="form1"/>
                        <input type="hidden" name="_token"
                               value="{{csrf_token() }}"/>
                        <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="Date">Date :</label>
                                <input type='text' value="{{$data['date']}}"
                                       class="form-control"
                                       name="date" id='datetimepicker4'/>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="bodyNum">DR :</label>
                                <input type="text" value="{{$data['dr']}}"
                                       placeholder="dr"
                                       class="form-control" name="dr">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="bodyNum">OR :</label>
                                <input type="text" value="{{$data['or']}}"
                                       placeholder="or"
                                       class="form-control" name="or">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="color">Ordered From:</label>
                                <input type="text" value="{{$data['ordered_from']}}"
                                       placeholder="color"
                                       class="form-control" name="ordered_from">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="gender">Status</label>
                                <select name="status" class="form-control">
                                    <option>{{$data['status']}}</option>
                                    <option>pending</option>
                                    <option>paid</option>
                                    <option>cancelled</option>
                                </select>

                            </div>


                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="weight">Amount:</label>
                                <input type="text" value="{{$data['amount']}}"
                                       placeholder="kg" class="form-control"
                                       name="amount">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="sw">Due Date:</label>
                                <input id = "datetimepicker133{{$data['id']}}" type="text" value="{{$data['due_date']}}"
                                       placeholder="kg" class="form-control"
                                       name="due_date">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span>
                            Cancel
                        </button>
                        <button type="submit" class="btn btn-success"><span
                                    class="glyphicon glyphicon-save"></span>
                            Save
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <script>
            $(function () {
                $('#datetimepicker133{{$data['id']}}').datepicker();
                //   $('[data-toggle = "tool-tip"]').tooltip();
            });
        </script>
        <div class="modal fade" id="remove{{$data['id']}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Remove Transaction</h2>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post" action="./delete_outsource/{{$data['id']}}" id="form1"/>
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                        <div class="container col-lg-12  col-md-12">
                            <h5> Are you sure you want to delete this transaction?</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-success"><span class="glyphicon glyphicon-ok"></span>
                            Confirm
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>
    @endforeach

@endsection
