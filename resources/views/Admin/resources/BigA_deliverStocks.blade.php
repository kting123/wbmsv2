@extends('appv20')

@section('content')
    <div id="page-wrapper">
        <br>
        <div class="">
            <div class="row">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading" style="height: 50px">
                        <h4 class="pull-left"><span class="glyphicon glyphicon-shopping-cart"></span> Orders From
                            Clients</h4>
                    </div>

                    <div class="panel-body">
                      <form type="hidden" method="post" action="./addOrder" id="form1">
                          <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                          <input type="hidden" id="nowdate" name="nowdate" value=""/>
                          <input type="hidden" id="nowhr" name="hr" value=""/>
                          <input type="hidden" id="nowmin" name="min" value=""/>
                          <input type="hidden" id="nowsecs" name="secs" value=""/>
                          <input type="hidden" id="timestamp" name="timestamp" value=""/>
                          <div class="container col-lg-12 col-md-12 col-sm-12 ">
                            <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                <label class="radio-inline">
                                    <input type="radio" name="radBtn" id="credit" value="1" checked> <b>
                                        Credit</b>
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="radBtn" id="cash" value="0"> <b>Cash</b>
                                </label>
                                &nbsp;&nbsp;
                            </div>
                              <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                  <label for="Sold">From</label>
                                  <select name="myClient" class="form-control" id="supplier" size="1">
                                      <option>Select</option>
                                          @foreach($outsources as $resources)
                                              <?php $total = 0; ?>
                                              @foreach($resources['med_resources'] as $source)
                                              <?php $total = $total+ $source['remain_qty']; $data[] = array(); ?>
                                                @if($total>0)
                                                      @if(in_array($resources['id'],$data))
                                                      @else
                                                      <option value="{{$resources['id']}}">{{$resources['date']}} => {{$resources['clients']['name']}}</option>
                                                      @endif
                                                      <?php $data = array($resources['id']); ?>
                                                @else
                                                @endif
                                              @endforeach
                                      @endforeach
                                  </select>
                              </div>
                              <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                  <label for="Sold">To</label>
                                  <input type="text" name="to" value=""
                                         placeholder="" class="form-control" id="to"  required>
                              </div>
                              <div id = "dr" class="form-group col-lg-2 col-md-3 col-sm-4">
                                  <label for="OR">DR#</label>
                                  <input type="text" name="dr" value=""
                                         placeholder="" class="form-control" id="dr#">
                              </div>
                              <div id = "or" class="form-group col-lg-2 col-md-3 col-sm-4" hidden>
                                  <label for="OR">OR#</label>
                                  <input type="text" name="or" value=""
                                         placeholder="" class="form-control" id="or#" >
                              </div>
                              <div class="form-group col-lg-2  col-md-3 col-sm-4">
                                  <label for="Date">Date Ordered</label>
                                  <input type='text' name="datePurchased" class="form-control"
                                         id='datetimepicker110'/>

                              </div>
                              <div id = "due_date" class="form-group col-lg-2 col-md-3 col-sm-4">
                                  <label for="Date">Due Date</label>
                                  <input type='text' name="dueDate" class="form-control"
                                         id='datetimepicker101'/>
                              </div>
                              <div class="form-group col-lg-3 col-md-3 col-sm-6">
                                  <label for="Med">Medicines</label>
                                  <select id="med" class="form-control" size="1">
                                      <option></option>
                                  </select>
                              </div>
                              <div class="form-group col-lg-2 col-md-6 col-sm-6">
                                  <label for="Med">Unit Price</label>
                                  <input type="text" id="unit" class="form-control" placeholder="unit price" size="1">
                              </div>
                              <div class="form-group col-lg-1 col-md-6 col-sm-6">
                                  <label for="Med">Add(%)</label>
                                  <input type="text" value="0" id="percent" class="form-control" placeholder="%" size="1">
                              </div>
                              <div class="form-group col-lg-2 col-md-3 col-sm-3">
                                  <label for="Qty">Quantity</label>
                                  <input type="text"  placeholder="" class="form-control"
                                         id="qty">
                              </div>
                              <div class="form-group col-lg-2 col-md-2 col-sm-2" style="margin-top: 22px;">
                                  <button class="btn btn-{{Auth::user()->buttons}}" type="button"
                                          onclick="AddData1()">
                                      <span class="glyphicon glyphicon-plus"></span>Add Item
                                  </button>
                                  <!-- <input id = "button" type = "button" value = " Add " onclick = "AddData()"> -->
                              </div>
                              <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                  <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                      <div class="table table-responsive">
                                          <table class="table table-condensed" id="list">
                                              <thead id="tblHead">
                                              <tr>
                                                  <th>Medicine</th>
                                                  <th># of Bottles</th>
                                                  <th>% of Original Price</th>
                                                  <th>Unit Price</th>
                                                  <th>Total Price (Php)</th>
                                              </tr>
                                              </thead>
                                              <tbody>

                                              </tbody>
                                          </table>
                                      </div>
                                      <label for="Total"><h3>Total Amount (Php):</h3></label>
                                      <label for="TAmount"><h3><input id="totalD" value="0.00"
                                                                      name="Tamnt" readonly
                                                                      style='width: 100px;color: #21305c; background-color: transparent;height: 100%;width: 100%; border: 0;padding: 0px 0px 0px;'/>
                                          </h3></label>
                                  </div>
                              </div>
                          </div>

                          <div class="modal-footer">
                            <label href="./BigA_stocks" class="btn btn-danger"><span
                                        class="glyphicon glyphicon-refresh"></span> Refresh All
                            </label>
                            <label onclick="ResetTable()" class="btn btn-warning"><span
                                        class="glyphicon glyphicon-trash"></span> Reset List</label>
                            <button id="checkout" class="btn btn-{{Auth::user()->buttons}}"><span
                                        class="glyphicon glyphicon-shopping-cart"></span> Checkout
                            </button>
                          </div>
                      </form>
                    </div>

                </div>
            </div>

        </div>
    </div>


    @include('reusable.getClientTime')
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker101').datepicker("setDate", '1d');
            $('#datetimepicker110').datepicker("setDate", '1d');
            $('#datetimepicker4').datepicker("setDate", '1d');
            $('#datetimepicker5').datepicker("setDate", '1d');
            $('#supplier').change(function () {
                var id = $(this).val();
                $.ajax({
                    url: 'API/getMedInfo1',
                    type: "get",
                    data: {'id': id},
                    success: function (data) {
                        var i = 0;
                        $("#med").find('option').remove().end().append('<option value="Select">Select</option>')
                        for (info in data) {
                          if(data[i].qty>0){
                            $('#med').append('<option id = "' + data[i].medicine_id + '"value ="' + data[i].medicines + '">'+data[i].qty +' - ' + data[i].medicines + '</option>');
                          }
                          else{

                          }
                          i++;
                        }
                    }
                });
            });
            $('#med').change(function () {
                var id = $(this).children(":selected").attr("id");
                $.ajax({
                    url: 'API/getUnitPrice',
                    type: "get",
                    data: {'id': id},
                    success: function (data) {
                        $('#unit').val(data);
                    }
                });
            });
            $('#list').on('click', 'tr a', function (e) {
                e.preventDefault();
                var minus = $(this).attr("id");
                //  console.log(minus);
                totalD = totalD - minus;
                document.getElementById("totalD").value = totalD;
                $(this).parents('tr').remove();

            });
        });
        var totalD = 0;
        var i = 0;
        function AddData1() {
            var rows = "";
            var totalP = "";
            var med = document.getElementById("med").value;
            var qty = document.getElementById("qty").value;
            var unit = document.getElementById("unit").value;
            var percent = document.getElementById("percent").value;
            var resource_id = document.getElementById('supplier').value;
            //var totalD = document.getElementById("totalD").innerHTML;
            var con = parseInt(totalD);
            var newUnit = Number(unit) + (Number(unit)*(Number(percent)/100));
            newUnit = newUnit.toFixed(2);
            totalP = qty * newUnit;
            var TotalP = totalP.toFixed(2);
            totalD = document.getElementById("totalD").value;
            totalD = Number(totalD) + totalP;
            var TotalD = totalD.toFixed(2);
            if (qty == "" || unit == "") {
                alert('Pleas add Some quantity and unit price');
            }
            else {
                rows += "<tr><td><input hidden name = id"+i+" value = '"+resource_id+"'><input readonly name=medicine" + i + " value= '" + med + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' ></td><td><input readonly name=MedQty" + i + " value= '" + qty + "' style='width: 30px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' ></td><td><input readonly name=percent" + i + " value= '" + percent + "%' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' ></td><td><input readonly name=MedUnit" + i + " value= '" + newUnit + "' style='width: 30px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' ></td><td><input readonly name=MedTotal" + i + " value= '" + TotalP + "' style='width: 80px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' ></td><td> <a id=" + TotalP + " data-toggle='tooltip' class = 'btn btn-outline btn-danger'title='Remove Item'>X</a></td></tr>";
                $(rows).appendTo("#list tbody");
                //alert(totalP);
                document.getElementById("totalD").value = TotalD;
                i++;
            }
        }
        function ResetTable() {
            i = 0;
            $('#list tbody tr').remove();
            document.getElementById("totalD").value = '';
            totalD = 0;
        }
        $('input[type="radio"]').click(function () {
            if ($(this).attr('id') == 'credit') {
                $('#dr').show();
                $('#due_date').show();
                $('#or').hide();

            }
            else {
                $('#or').show();
                $('#dr').hide();
                $('#due_date').hide();

            }
        });
    </script>
@endsection
