@extends('appv20')

@section('content')
<div id="page-wrapper">
   <br>

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading"><h4> Angus Inventory <span data-toggle="modal" data-target="#detail"class="pull-right glyphicon glyphicon-info-sign"></span></h4></div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12">
                                <thead style="border-bottom:solid 2px;">
                                <th></th>
                                <th>Article <br>Name</th>
                                <th>Total <br>Qty</th>
                                <th>Remain <br>Qty</th>
                                <th>Sales <br>Qty</th>
                                <th>Unit<br>Price(₱)</th>
                                <th>Sales <br>(₱)</th>
                                <th>TOTAL <br>(₱)</th>
                                </thead>
                                <tbody>
                                <tr>
                                    <td><b>Choice Cuts</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <?php $total1 = 0; ?>
                                @foreach($cow['article_resources'] as $list)
                                    @if($list['type']=='Choice Cuts')
                                        <tr>
                                            <td></td>
                                            @if($list['unit_price']==0)
                                                <td><a href="#" data-toggle="modal"
                                                       data-target="#{{$list['id']}}">{{$list['article']['name']}}</a>  </td>
                                            @else
                                                <td>{{$list['article']['name']}}</td>
                                            @endif
                                            <td>{{$list['quantity']}}</td>
                                            <td>{{$list['remain_qty']}}</td>
                                            <td>{{$list['sales_qty']}}</td>
                                            @if($list['unit_price']==0)
                                                <td>-</td>
                                            @else
                                                <td>{{number_format($list['unit_price'],2)}}</td>
                                            @endif
                                            <td>{{number_format($list['total_sales'],2)}}</td>

                                            <?php $total1 = $total1 + $list['total_sales']; ?>
                                            <td></td>


                                        </tr>
                                    @endif
                                @endforeach
                                <tr style="border-top:solid 2px;">
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td><b>₱{{number_format($total1,2)}}</b></td>

                                </tr>

                                <tr>
                                    <td><b>Special Cuts</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                </tr>
                                <?php $total2 = 0; ?>
                                @foreach($cow['article_resources'] as $list)
                                    @if($list['type']=='Special Cuts')
                                        <tr>
                                            <td></td>
                                            @if($list['unit_price']==0)
                                                <td><a href="#" data-toggle="modal"
                                                       data-target="#{{$list['id']}}"> {{$list['article']['name']}}</a> </td>
                                            @else
                                                <td>{{$list['article']['name']}}</td>
                                            @endif
                                            <td>{{$list['quantity']}}</td>
                                            <td>{{$list['remain_qty']}}</td>
                                            <td>{{$list['sales_qty']}}</td>
                                            @if($list['unit_price']==0)
                                                <td>-</td>
                                            @else
                                                <td>{{number_format($list['unit_price'],2)}}</td>
                                            @endif


                                            <td>{{number_format($list['total_sales'],2)}}</td>

                                            <?php $total2 = $total2 + $list['total_sales']; ?>
                                            <td></td>


                                        </tr>
                                    @endif
                                @endforeach
                                <tr style="border-top:solid 2px;">
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td><b>₱{{number_format($total2,2)}}</b></td>
                                </tr>


                                <tr>
                                    <td><b>Other Cuts</b></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                </tr>
                                <?php $total3 = 0; ?>
                                @foreach($cow['article_resources'] as $list)
                                    @if($list['type']=='Other Cuts')
                                        <tr>
                                            <td></td>
                                            @if($list['unit_price']==0)
                                                <td><a href="#" data-toggle="modal"
                                                       data-target="#{{$list['id']}}"> {{$list['article']['name']}} </a></td>
                                            @else
                                                <td>{{$list['article']['name']}}</td>
                                            @endif
                                            <td>{{$list['quantity']}}</td>
                                            <td>{{$list['remain_qty']}}</td>
                                            <td>{{$list['sales_qty']}}</td>
                                            @if($list['unit_price']==0)
                                                <td>-</td>
                                            @else
                                                <td>{{number_format($list['unit_price'],2)}}</td>
                                            @endif
                                            <td>{{number_format($list['total_sales'],2)}}</td>
                                            <?php $total3 = $total3 + $list['total_sales']; ?>
                                            <td></td>
                                        </tr>
                                    @endif
                                @endforeach
                                <tr style="border-top:solid 2px;">
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td><b>₱{{number_format($total3,2)}}</b></td>

                                </tr>


                                <tr>
                                    <?php $total4 = $total1 + $total2 + $total3 ?>
                                    <td><b>TOTAL SALES</b></td>
                                    <td></td>
                                    <td></td>

                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>

                                    <td STYLE="border-bottom:solid 5px; "><b>₱{{number_format($total4,2)}}</b></td>

                                </tr>
                                <tbody>
                            </table>
                        </div>

                    </div>
                    <div class="panel-footer">
                      <a href="./printCowReport{{$cow['id']}}" class="btn btn-outline btn-{{Auth::user()->buttons}} btn-lg"><span
                                  class="glyphicon glyphicon-export"></span> Export to Excel File</a>
                    </div>
                </div>

            </div>
        </div>
        @foreach($cow['article_resources'] as $article)
            @if($article['unit_price']==0)
                <div class="modal fade" id="{{$article['id']}}" role="dialog">
                    <div class="modal-dialog">
                        <!-- Modal content-->
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"><span class="glyphicon glyphicon-info-sign"></span> Item Orders
                                    History - {{$article['article']['name']}}</h4>
                            </div>
                            <div class="modal-body">
                                <div class="table table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                        <tr>
                                            <th>DR</th>
                                            <th>OR</th>
                                            <th>Date</th>
                                            <th>Article</th>
                                            <th>Quantity</th>
                                            <th>Unit Price</th>
                                            <th>Amount</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($article['orderlist'] as $order)
                                            @if($order['order']['status'] =='cancelled')
                                                <tr>
                                                    <td><strike><a data-toggle="tool-tip" title="see details"
                                                                   href=".\dr_order{{$order['order']['id']}}">{{$order['order']['dr']}}</a></strike>
                                                    </td>
                                                    <td><strike><a data-toggle="tool-tip" title="see details"
                                                                   href=".\dr_order{{$order['order']['id']}}">{{$order['order']['or']}}</a></strike>
                                                    </td>
                                                    <td><strike>{{$order['order']['date']}}</strike></td>
                                                    <td><strike>{{$order['article']['name']}}</strike></td>
                                                    <td><strike>{{$order['qty']}}</strike></td>
                                                    <td><strike>{{number_format($order['unit_price'],2)}}</strike></td>
                                                    <td><strike>{{number_format($order['amount'],2)}}</strike></td>
                                                </tr>
                                            @else
                                                <tr>
                                                    <td><a data-toggle="tool-tip" title="see details"
                                                           href=".\order_dr{{$order['order']['id']}}">{{$order['order']['dr']}}</a>
                                                    </td>
                                                    <td><a data-toggle="tool-tip" title="see details"
                                                           href=".\order_or{{$order['order']['id']}}">{{$order['order']['or']}}</a>
                                                    </td>
                                                    <td>{{$order['order']['date']}}</td>
                                                    <td>{{$order['article']['name']}}</td>
                                                    <td>{{$order['qty']}}</td>
                                                    <td>{{number_format($order['unit_price'],2)}}</td>
                                                    <td>{{number_format($order['amount'],2)}}</td>
                                                </tr>
                                                @endif
                                        @endforeach
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td><b>TOTAL</b></td>
                                            <td style="border-top:2px solid;">
                                                <b>₱{{number_format($article['total_sales'],2)}}</b></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button data-dismiss="modal" class="btn btn-{{Auth::user()->buttons}}"><span
                                            class="glyphicon glyphicon-ok"></span> Ok
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            @else
            @endif
        @endforeach
    </div>
    <div class="modal fade" id = "detail">
      <div class="col-md-8 col-lg-8 col-sm-8 col-lg-offset-2">
          <div class="panel panel-{{Auth::user()->panels}}">
              <div class="panel-body">
                  <div style="text-align: center;font-size:16px;"> Date :
                      <b>{{$cow['date']}}</b> &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;  Body# : <b>{{$cow['id']}}</b> &nbsp;&nbsp;
                      &nbsp; &nbsp;&nbsp; &nbsp;LW :
                      <b>{{$cow['lw']}}</b> Kls. &nbsp;&nbsp; &nbsp; &nbsp;&nbsp; &nbsp;  SW : <b>{{$cow['sw']}}</b> Kls.
                      &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;
                      MW : <b>{{$cow['mw']}}</b> Kls.
                      <!--                        &nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp; <span
                                                  class="glyphicon glyphicon-minus-sign"></span> <b> QUARTER ANGUS</b> &nbsp;&nbsp;&nbsp;
                                              &nbsp;Front:1. &nbsp;&nbsp; 2. &nbsp;&nbsp;&nbsp; &nbsp;Rear:1.&nbsp;&nbsp; 2.-->
                  </div>
                <h4>  BASE PARTS</h4>
                  <div class="table table-responsive">
                      <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                          <thead>
                          <th>Article Name</th>
                          <th>Quantity (Kls.)</th>
                          </thead>
                          <tbody>
                          <?php $qaunt =0; ?>
                          @foreach($cow['main_parts'] as $part)
                              <tr>
                                  <td>{{$part['article']}}</td>
                                  <td>{{$part['quantity']}}</td>
                                  <?php $qaunt =$qaunt+$part['quantity']; ?>
                              </tr>
                          @endforeach
                          <tr style="border-top:solid 2px;">
                              <td><b>Total</b></td>
                              <td><b>{{$qaunt}} Kls</b></td>
                          </tr>
                          <tbody>
                      </table>
                  </div>
              </div>
          </div>
      </div>
 <button data-dismiss = "modal" name="button">X</button>
    </div>
@endsection
