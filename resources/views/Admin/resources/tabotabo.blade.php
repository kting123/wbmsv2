@extends('appv20')
@section('content')
    <div id="page-wrapper">
        <br>

        <div class="row">
            <div class="panel panel-{{Auth::user()->panels}}">
                <div class="panel-heading">
                    <h4> Recent List - Tabo Tabo <span data-toggle="modal" class="pull-right glyphicon glyphicon-plus"
                                                       data-target="#addTabo"></span></h4>
                </div>
                <div class="panel-body">
                    <div class="table table-responsive">
                        <table class="table table-hover col-lg-12 col-md-12 col-xs-12 results"
                               xmlns="http://www.w3.org/1999/html">
                            <thead>
                            <th>Date of Purchase</th>
                            <th>Body#</th>
                            <th>Gender</th>
                            <th>Owner</th>
                            <th>Area</th>
                            <th>Color</th>
                            <th>Price</th>
                            <th>Gross</th>
                            <th>Expense</th>
                            <th>Income/Diff</th>
                            <th>Action</th>

                            </thead>
                            <tbody>
                            @foreach($tabotabos as $tabotabo)
                                <tr>
                                    <td>{{$tabotabo['date']}}</td>
                                    <td>
                                        <a href="./view_tabo_tabo_br{{$tabotabo['id']}}"
                                           class="btn btn-outline btn-{{Auth::user()->buttons}}"
                                           data-toggle="tool-tip"
                                           data-placement="top"
                                           title="LW : {{$tabotabo['lw']}}   SW : {{$tabotabo['sw']}}   MW: {{$tabotabo['mw']}}
                                                   ">{{$tabotabo['id']}}</a>
                                    </td>
                                    <td>{{$tabotabo['sex']}}</td>
                                    <td>{{$tabotabo['owner']}}</td>
                                    <td>{{$tabotabo['area']}}</td>
                                    <td style="color:{{$tabotabo['color']}}">{{$tabotabo['color']}}</td>
                                    <td>₱ {{number_format($tabotabo['price'],2)}}</td>
                                    <?php $exp = 0;  $resources = 0;?>
                                    @foreach($tabotabo['expenses'] as $expense)
                                        <?php $exp = $exp + $expense['amount']; ?>
                                    @endforeach
                                    @foreach($tabotabo['article_resources'] as $resource)
                                        <?php $resources = $resources + $resource['total_sales']; ?>
                                    @endforeach
                                    <?php $income_deff = $resources - $tabotabo['price'] - $exp;?>
                                    <td>{{number_format($resources,2)}}</td>
                                    <td>{{number_format($exp,2)}}</td>
                                    <td>{{number_format($income_deff,2)}}</td>

                                    <td><a href="#editTabo{{$tabotabo['id']}}" data-toggle="modal"
                                           data-target="#editTabo{{$tabotabo['id']}}"><span
                                                    class="glyphicon glyphicon-edit"></span></a> | <a
                                                href="#remove{{$tabotabo['id']}}" data-toggle="modal"
                                                data-target="#remove{{$tabotabo['id']}}"><span
                                                    class="glyphicon glyphicon-trash"></span></a></td>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer">
                        <label class="pull-right">{!! $tabotabos->render() !!}</label>
                    </div>
                </div>

            </div>
        </div>
    </div>

    @foreach($tabotabos as $tabotabo)
        <div class="modal fade" id="editTabo{{$tabotabo['id']}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content"
                >
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit Tabo</h4>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post" action="./edit_tabo/{{$tabotabo['id']}}" id="form1"/>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="Date">Date Purchase:</label>
                                <input type='text'
                                       value="{{$tabotabo['date']}}"
                                       class="form-control"
                                       name="dateofpurchased" id='datetimepicker19' required/>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="bodyNum">Body #:</label>
                                <input type="text" value="{{$tabotabo['id']}}"
                                       placeholder="Body #"
                                       class="form-control" name="bodyNum" required>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="owner">Owner:</label>
                                <input type="text" value="{{$tabotabo['owner']}}"
                                       placeholder="owner"
                                       class="form-control" name="owner" required>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="area">Area:</label>
                                <input type="text" value="{{$tabotabo['area']}}"
                                       placeholder="area" class="form-control"
                                       name="area" >
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="color">Color:</label>
                                <input type="text" value="{{$tabotabo['color']}}"
                                       placeholder="color"
                                       class="form-control" name="color">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="gender">Gender</label>
                                <select name="gender" class="form-control">
                                    <option>{{$tabotabo['sex']}}</option>
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>

                            </div>
                        </div>
                        <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="weight">LW:</label>
                                <input type="text" value="{{$tabotabo['lw']}}"
                                       placeholder="kg" class="form-control"
                                       name="lw" required>
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="sw">SW:</label>
                                <input type="text" value="{{$tabotabo['sw']}}"
                                       placeholder="kg" class="form-control"
                                       name="sw" required>
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="weight">MW:</label>
                                <input type="text" value="{{$tabotabo['mw']}}"
                                       placeholder="kg" class="form-control"
                                       name="mw">
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="perKilo">per Kilo:</label>
                                <input type="text"
                                       value="{{$tabotabo['price_per_kilo']}}"
                                       placeholder="per Kilo"
                                       class="form-control" name="perKilo" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span>
                            Cancel
                        </button>
                        <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-save"></span>
                            Save
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>

        <div class="modal fade" id="remove{{$tabotabo['id']}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content"
                >
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h2 class="modal-title">Remove Transaction</h2>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post"
                              action="./delete_tabo/{{$tabotabo['id']}}" id="form1"/>
                        <input type="hidden" name="_token"
                               value="{{csrf_token() }}"/>
                        <div class="container col-lg-12  col-md-12">
                            <h5> Are you sure you want to delete this transaction?</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-ok"></span>
                            Confirm
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>
    @endforeach





    <div class="modal fade" id="addTabo" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add Tabo-Tabo</h4>
                </div>
                <div class="modal-body">
                    <form type="hidden" method="post" action="./addTabo" id="form1"/>
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                    <input type="hidden" id="nowdate" name="nowdate" value=""/>
                    <input type="hidden" id="nowhr" name="hr" value=""/>
                    <input type="hidden" id="nowmin" name="min" value=""/>
                    <input type="hidden" id="nowsecs" name="secs" value=""/>
                    <input type="hidden" id="timestamp" name="timestamp" value=""/>
                    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="Date">Date Purchase:</label>
                            <input type='text' class="form-control" name="dateofpurchased" id='datetimepicker4' required/>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="bodyNum">Body #:</label>
                            <input type="text" value="" placeholder="Body #"
                                   class="form-control" name="bodyNumber" required>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="owner">Owner:</label>
                            <input type="text" value="" placeholder="owner"
                                   class="form-control" name="owner" required>
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="area">Area:</label>
                            <input type="text" value="" placeholder="area" class="form-control"
                                   name="area">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="color">Color:</label>
                            <input type="text" value="" placeholder="color"
                                   class="form-control" name="color">
                        </div>
                        <div class="form-group col-lg-4 col-md-4 col-sm-4">
                            <label for="gender">Gender</label>
                            <select name="gender" class="form-control">
                                <option>Male</option>
                                <option>Female</option>
                            </select>

                        </div>
                    </div>
                    <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group col-lg-3 col-md-4 col-sm-3">
                            <label for="weight">LW:</label>
                            <input type="text" value="" placeholder="kg" class="form-control"
                                   name="lw" required>
                        </div>
                        <div class="form-group col-lg-3 col-md-4 col-sm-3">
                            <label for="sw">SW:</label>
                            <input type="text" value="" placeholder="kg" class="form-control"
                                   name="sw" required>
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="weight">MW:</label>
                            <input type="text" value="" placeholder="kg" class="form-control"
                                   name="mw">
                        </div>
                        <div class="form-group col-lg-3 col-md-4 col-sm-3">
                            <label for="perKilo">per Kilo:</label>
                            <input type="text" value="" placeholder="per Kilo"
                                   class="form-control" name="perKilo" required>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-danger"><span
                                class="glyphicon glyphicon-remove"></span>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                class="glyphicon glyphicon-plus"></span>
                        Save
                    </button>
                </div>
            </div>
            </form>
        </div>

    </div>
    @include('reusable.getClientTime')
    <script>
        $(function () {
            $('#datetimepicker19').datepicker();
            $('#datetimepicker5').datepicker();
            $('#datetimepicker4').datepicker();
            $('#datetimepicker1').datepicker();
            $('#datetimepicker2').datepicker();
        });
    </script>
@endsection
