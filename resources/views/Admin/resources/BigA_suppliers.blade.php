@extends('appv20')

@section('content')
    <div id="page-wrapper">
        <br>
        <div class="">
            <div class="row">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading" style="height: 50px">
                        <h4 class="pull-left"><span class="glyphicon glyphicon-shopping-cart"></span> Orders From
                            Clients</h4>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12">
                                <thead>
                                <th>Date Ordered</th>
                                <th>DR #</th>
                                <th>Clients</th>
                                <th>OR #</th>
                                <th>Status</th>
                                <th>Due Date</th>
                                <th>Total Amount (Php)</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                <?php $dued = 0; $nearDue = 0; $safeDue = 0; ?>
                                @foreach($resources as $data)
                                    <tr>
                                        @if($data['status']=="cancelled")
                                            <td><strike>{{$data['date']}}<strike></td>
                                            <td><a href="./BigA_Supply_br{{$data['id']}}" data-toggle="tool-tip" title="see details"><strike>{{$data['dr']}}</strike></a></td>
                                            <td><strike>{{$data['customer']['name']}}<strike></td>
                                            <td>{{$data['or']}}</td>
                                            <td><strike>₱{{number_format($data['total_due'], 2)}}</strike></td>
                                            <td><strike><label class="label label-danger">{{$data['status']}}</label></strike></td>
                                            <td><strike>{{$data['due_date']}}</strike></td>
                                        @else
                                            <td>{{$data['date']}}</td>
                                            <td><a class="btn btn-outline btn-{{Auth::user()->buttons}}"  href="./BigA_Supply_br{{$data['id']}}"  data-toggle="tool-tip" title="see details">{{$data['dr']}}</a></td>
                                            <td>{{$data['clients']['name']}}</td>
                                            <td>{{$data['or']}}</td>
                                            <td>₱{{number_format($data['total_due'], 2)}}</td>
                                            <?php
                                            $due = (int) strtotime($data['due_date']);
                                            $due_warning = $due - 432000;
                                            ?>
                                            @if($data['status']=="paid")
                                                <td><button class="btn btn-outline btn-success" style="font-size:medium">{{$data['status']}}</button></td>
                                                <td><button class="btn btn-outline btn-success" style="font-size:medium">{{$data['due_date']}}</button></td>
                                            @else
                                                @if(($data['status']=="pending" || $data['status']=="partial") && $due <=$now)
                                                    <td><button class="btn btn-outline btn-danger" style="font-size:medium">{{$data['status']}}</button></td>
                                                    <td><button class="btn btn-outline btn-danger" style="font-size:medium">{{$data['due_date']}}</button></td>
                                                    <?php $dued++;?>

                                                @elseif(($data['status']=="pending" || $data['status']=="partial") && $due_warning <=$now )
                                                    <td><button class="btn btn-outline btn-warning" style="font-size:medium">{{$data['status']}}</button></td>
                                                    <td><button class="btn btn-outline btn-warning" style="font-size:medium">{{$data['due_date']}}</button></td>
                                                    <?php $nearDue++;?>
                                                @else
                                                    <td><button class="btn btn-outline btn-warning" style="font-size:medium">{{$data['status']}}</button></td>
                                                    <td><button class="btn btn-outline btn-primary" style="font-size:medium">{{$data['due_date']}}</button></td>
                                                    <?php $safeDue++;?>
                                                @endif
                                            @endif
                                        @endif

                                        <td><label href="#" data-toggle="modal" data-target="#edit{{$data['id']}}"><span
                                                        class="glyphicon glyphicon-edit"></span></label> |
                                            <label href="#" data-toggle="modal" data-target="#remove{{$data['id']}}"><span
                                                        class="glyphicon glyphicon-trash"></span></label></td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="panel-footer">
                            <div class="pull-right">
                                {!! $resources->render() !!}
                            </div>
                        </div>
                        <button class="btn btn-outline btn-{{Auth::user()->buttons}}" data-toggle="modal"
                                data-target="#myModal">
                            Add Supply
                        </button>
                    </div>

                </div>
            </div>

        </div>
    </div>
    @foreach($resources as $data)

        <div class="modal fade" id="edit{{$data['id']}}" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><span class="glyphicon glyphicon-edit"></span> Edit Transaction</h4>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post" action="./save_supply{{$data['id']}}" id="form1"/>
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                        <div class="container col-lg-12  col-md-12">
                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Date">Date</label>
                                <input  type='text' class="form-control" id='datetimepicker4' name="date"
                                       value="{{$data['date']}}"/>
                            </div>
                            <div class="form-group col-lg-3 col-md-6">
                                <label for="DR">OR No.</label>
                                <input type="text" value="{{$data['or']}}" placeholder="" class="form-control"
                                       name="or">
                            </div>
                            <div class="form-group col-lg-3 col-md-6">
                                <label for="DR">DR No.</label>
                                <input type="text" value="{{$data['dr']}}" placeholder="" class="form-control"
                                       name="dr">
                            </div>
                            <div class="form-group col-lg-3 col-md-6">
                                <label for="DR">Status</label>
                                <select class="form-control" name="status">
                                    <option>{{$data['status']}}</option>
                                    <option>pending</option>
                                    <option>paid</option>
                                    <option>partial</option>
                                    <option>cancelled</option>
                                </select>
                            </div>
                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Deliver">Due Date</label>
                                <input id = "datetimepicker5" type="text" value="{{$data['due_date']}}" placeholder="" class="form-control"
                                       name="dueDate">
                            </div>
                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Deliver">Supplier</label>
                                <select name="client" class="form-control"  size="1">
                                    <option value = "{{$data['client_id']}}">{{$data['clients']['name']}}</option>
                                    @foreach($clients as $c)
                                        <option value="{{$c->id}}">{{$c->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="form-group col-lg-3  col-md-6">
                                <label for="gender">Amount</label>
                                <input type="text" value="{{$data['total_due']}}" placeholder="" class="form-control"
                                       name="amount" readonly>
                            </div>

                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-outline btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-save"></span>
                            Save
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>
        <div class="modal fade" id="remove{{$data['id']}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><span class="glyphicon glyphicon-remove"></span> Remove Transaction</h4>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post" action="./delete_supply/{{$data['id']}}" id="form1"/>
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                        <div class="container col-lg-12  col-md-12">
                            <h5> Are you sure you want to delete this transaction?</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-outline btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-ok"></span>
                            Confirm
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>
    @endforeach

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content" style="background-image: url('assets/img/123.jpg')">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h3 class="modal-title"><span class="glyphicon glyphicon-shopping-cart"></span> New Purchase</h3>
                </div>
                <form type="hidden" method="post" action="./addSupply" id="form1">
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                    <input type="hidden" id="nowdate" name="nowdate" value=""/>
                    <input type="hidden" id="nowhr" name="hr" value=""/>
                    <input type="hidden" id="nowmin" name="min" value=""/>
                    <input type="hidden" id="nowsecs" name="secs" value=""/>
                    <input type="hidden" id="timestamp" name="timestamp" value=""/>
                    <div class="container col-lg-12 col-md-12 col-sm-12 ">
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="Sold">Client</label>
                            <select name="myClient" class="form-control" id="supplier" size="1">
                                <option>Select</option>
                                @foreach($clients as $c)
                                    <option value="{{$c->id}}">{{$c->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-lg-2 col-md-3 col-sm-4">
                            <label for="OR">DR#</label>
                            <input type="text" name="dr#" value=""
                                   placeholder="" class="form-control" id="dr#" required>
                        </div>
                        <div class="form-group col-lg-2 col-md-3 col-sm-4">
                            <label for="Date">Date Ordered</label>
                            <input type='text' name="datePurchased" class="form-control"
                                   id='datetimepicker110'/>

                        </div>
                        <div class="form-group col-lg-2 col-md-3 col-sm-4">
                            <label for="Date">Due Date</label>
                            <input type='text' name="dueDate" class="form-control"
                                   id='datetimepicker101'/>

                        </div>
                        <div class="form-group col-lg-3 col-md-6 col-sm-6">
                            <label for="Med">Medicines</label>
                            <select id="med" class="form-control" >
                                <option></option>
                            </select>
                        </div>
                        <div class="form-group col-lg-2 col-md-6 col-sm-6">
                            <label for="Med">Unit Price</label>
                            <input type="text" id="unit" class="form-control" placeholder="unit price" >
                        </div>
                        <div class="form-group col-lg-2 col-md-3 col-sm-3">
                            <label for="Qty">Quantity</label>
                            <input type="text" value="" placeholder="quantity" class="form-control"
                                   id="qty">
                        </div>
                        <div class="form-group col-lg-2 col-md-3 col-sm-3">
                            <label for="Qty">ML</label>
                            <input type="text" value="" placeholder="MI" class="form-control"
                                   id="ml">
                        </div>
                        <div class="form-group col-lg-2 col-md-3 col-sm-3">
                            <label for="Qty">Manufacturer</label>
                            <input type="text" value="" placeholder="Manufacturer" class="form-control"
                                   id="manufacturer">
                        </div>
                        <div class="form-group col-lg-2 col-md-3 col-sm-3">
                            <label for="Qty">Expiry Date</label>
                            <input type="text" value="" placeholder="expiry date" class="form-control"
                                   id="expiry">
                        </div>
                        <div class="form-group col-lg-2 col-md-2 col-sm-2" style="margin-top: 22px;">
                            <button class="btn btn-{{Auth::user()->buttons}}" type="button"
                                    onclick="AddData1()">
                                <span class="glyphicon glyphicon-plus"></span>Add Item
                            </button>
                            <!-- <input id = "button" type = "button" value = " Add " onclick = "AddData()"> -->
                        </div>
                        <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                <div class="table table-responsive">
                                    <table class="table table-condensed" id="list">
                                        <thead id="tblHead">
                                        <tr>
                                            <th>Medicine</th>
                                            <th># of Bottles</th>
                                            <th>MI</th>
                                            <th>Expiry Date</th>
                                            <th>Unit Price</th>
                                            <th>Manufacturer</th>
                                            <th>Total Price (Php)</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                                <label for="Total"><h3>Total Amount (Php):</h3></label>
                                <label for="TAmount"><h3><input id="totalD" value="0.00"
                                                                name="Tamnt" readonly
                                                                style='width: 100px;color: #21305c; background-color: transparent;height: 100%;width: 100%; border: 0;padding: 0px 0px 0px;'/>
                                    </h3></label>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-success"><span
                                    class="glyphicon glyphicon-save"></span> Confirm
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('reusable.getClientTime')
    <script type="text/javascript">
        $(function () {
            $('#datetimepicker101').datepicker("setDate", '1d');
            $('#datetimepicker110').datepicker("setDate", '1d');
            $('#datetimepicker4').datepicker("setDate", '1d');
            $('#datetimepicker5').datepicker("setDate", '1d');
            $('#expiry').datepicker("setDate", '1d');
            $('#supplier').change(function () {
                var id = $(this).val();
                $.ajax({
                    url: 'API/getMedInfo',
                    type: "get",
                    data: {'id': id},
                    success: function (data) {
                        var i = 0;
                        $("#med").find('option').remove().end().append('<option value="Select">Select</option>')
                        for (info in data) {
                            $('#med').append('<option id = "' + data[i].id + '"value ="' + data[i].medicine + '">' + data[i].medicine + '</option>');
                            i++;
                        }
                    }
                });
            });
            $('#med').change(function () {
                var id = $(this).children(":selected").attr("id");
                $.ajax({
                    url: 'API/getUnitPrice',
                    type: "get",
                    data: {'id': id},
                    success: function (data) {
                        $('#unit').val(data);
                    }
                });
            });
            $('#list').on('click', 'tr a', function (e) {
                e.preventDefault();
                var minus = $(this).attr("id");
                //  console.log(minus);
                totalD = totalD - minus;
                document.getElementById("totalD").value = totalD;
                $(this).parents('tr').remove();

            });
        });
        var totalD = 0;
        var i = 0;
        function AddData1() {
            var rows = "";
            var totalP = "";
            var med = document.getElementById("med").value;
            var qty = document.getElementById("qty").value;
            var unit = document.getElementById("unit").value;
            var ml = document.getElementById("ml").value;
            var expiry = document.getElementById("expiry").value;
            var manufacturer = document.getElementById("manufacturer").value;
            //var totalD = document.getElementById("totalD").innerHTML;
            var con = parseInt(totalD);

            totalP = qty * unit;
            var TotalP = totalP.toFixed(2);
            totalD = document.getElementById("totalD").value;
            totalD = Number(totalD) + totalP;
            var TotalD = totalD.toFixed(2);
            if (qty == "" || unit == "") {
                alert('Pleas add Some quantity and unit price');
            }
            else {
                rows += "<tr><td><input readonly name=medicine" + i + " value= '" + med + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' ></td><td><input readonly name=MedQty" + i + " value= '" + qty + "' style='width: 30px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' ></td><td><input readonly name=ml" + i + " value= '" + ml + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' ></td><td><input readonly name=expiry" + i + " value= '" + expiry + "' style='width: 30px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' ></td><td><input readonly name=MedUnit" + i + " value= '" + unit + "' style='width: 30px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' ></td><td><input readonly name=manufacturer" + i + " value= '" + manufacturer + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' ></td><td><input readonly name=MedTotal" + i + " value= '" + TotalP + "' style='width: 80px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' ></td><td> <a id=" + TotalP + " data-toggle='tooltip' class = 'btn btn-outline btn-danger'title='Remove Item'>X</a></td></tr>";
                $(rows).appendTo("#list tbody");
                //alert(totalP);
                document.getElementById("totalD").value = TotalD;
                i++;
            }
        }


    </script>
@endsection
