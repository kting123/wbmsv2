@extends('appv20')

@section('content')
    <div id="page-wrapper">
      <br>

        <div class="row">
                <div class="panel panel-{{Auth::user()->panels}}">
                  <div class="panel-heading">
                    <h4>Inventory of Medicines <i data-toggle="modal" data-target="#export" href = "#" class="pull-right fa fa-download fa-2x"></i> </h4>
                  </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12" id="full1">
                                <thead>
                                <th>Quantity Remain</th>
                                <th>ML</th>
                                <th>Generic Name</th>
                                <th>Brand Name</th>
                                <th>Manufacturer</th>
                                <th>Action</th>

                                </thead>
                                <tbody>
                               
                                    <tr>
                                        <td>10</td>
                                        <td>100</td>
                                        <td><a class="btn btn-outline btn-{{Auth::user()->buttons}}"
                                               href="./BigA_inventory_reports" data-toggle="tool-tip"
                                               title="see details">IVERMECTIN</a></td>
                                        <td>IVOMEC</td>
                                        <td>Merial Saude</td>
                                        
                                        
                                        <td><a href="#" data-toggle="modal"
                                                   data-target="#edit"><span
                                                            class="glyphicon glyphicon-edit"></span></a> |
                                                <a href="#" data-toggle="modal"
                                                   data-target="#remove"><span
                                                            class="glyphicon glyphicon-trash"></span></a></td>
                                      
                                    </tr>
                             
                                </tbody>
                            </table>
                            
                            <script>
                                $(document).ready(function () {
                                    $(function () {
                                        $('#datetimepicker4').datepicker();
                                        $('#datetimepicker5').datepicker();
                                        $('#datetimepicker125').datepicker();
                                        $('#datetimepicker111').datepicker();
                                        $('#datetimepicker112').datepicker();

                                    });
                                });
                            </script>
                        </div>


                    </div>
                    
                    </div>

        </div>

        <!-- /.row -->
    </div>



        <div class="modal fade" id="edit" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content -->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><span class="glyphicon glyphicon-edit"></span> Edit Record</h4>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post" action="#" id="form1"/>
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                        <input type="hidden" name="month" value="#"/>
                        <input type="hidden" name="year" value="#"/>
                        <div class="container col-lg-12  col-md-12">

                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Date">Quantity Remain</label>

                                <input  type='text' class="form-control" name="date"
                                       value="10"/>

                            </div>
                            <div class="form-group col-lg-3 col-md-6">
                                <label for="DR">ML</label>
                                <input type="text" value="100" placeholder="" class="form-control"
                                       name="or">
                            </div>
                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Deliver">Generic Name</label>
                                <input type="text" value="IVERMECTIN" placeholder="" class="form-control"
                                       name="customer" >
                            </div>

                            <div class="form-group col-lg-3  col-md-6">
                                <label for="gender">Brand Name</label>
                                <input type="text" value="IVOMEC" placeholder="" class="form-control"
                                       name="amount" >
                            </div>
                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Qty">Manufacturer</label>
                                <input type="text" value="Merial Saude" placeholder="" class="form-control"
                                       name="bank">
                            </div>
                            
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-outline btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-save"></span>
                            Save
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>
        <div class="modal fade" id="remove" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content -->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><span class="glyphicon glyphicon-remove"></span> Remove Transaction</h4>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post" action="#" id="form1"/>
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                        <div class="container col-lg-12  col-md-12">
                            <h5> Are you sure you want to delete this transaction?</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-outline btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-ok"></span>
                            Confirm
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>
       

    <div class="modal fade" id="export" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Export OR's</h4>
                </div>
                <div class="modal-body">
                    <form type="hidden" method="post" action="./exportOR" id="form1"/>
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                    <div class="modal-body">
                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="">Type</label>
                            <select type='text' name="type" class="form-control">
                                <option value = "1" >Fully Paid - Delivery</option>
                                <option value = "0" >Partially Paid - Delivery</option>
                                <option value = "2" >Walked-In</option>
                                <option value = "3" >Both Delivery & WalkedIn</option>

                            </select>
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="">Specify Range</label>
                            <input type='text' name="date_from" class="form-control" placeholder="from"
                                   id='datetimepicker111' required/>
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="">&copy;</label>
                            <input type='text' name="date_to" class="form-control" placeholder="to"
                                   id='datetimepicker112' required/>
                        </div>

                        <div class="form-group col-lg-12 col-md-12">
                            <p><b>Note : </b> This will generate an excel file of Official Receipts.</p>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-outline btn-danger"><span
                                class="glyphicon glyphicon-remove"></span>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                class="glyphicon glyphicon-export"></span>
                        Export
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
    </div>
    <script>
        $(document).ready(function () {
            // $(".search").keyup(function () {
            //     var searchTerm = $(".search").val();
            //     var listItem = $('.results tbody').children('tr').children('td');
            //     var searchSplit = searchTerm.replace(/ /g, "'):containsi('");
            //     $.extend($.expr[':'], {
            //         'containsi': function (elem, i, match, array) {
            //             return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            //         }
            //     });
            //     $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function (e) {
            //         $(this).attr('visible', 'false');
            //     });
            //     $(".results tbody tr:containsi('" + searchSplit + "')").each(function (e) {
            //         $(this).attr('visible', 'true');
            //     });
            //     var jobCount = $('.results tbody tr[visible="true"]').length;
            //     $('.counter').text(jobCount + ' item');
            //     if (jobCount == '0') {
            //         $('.no-result').show();
            //     }
            //     else {
            //         $('.no-result').hide();
            //     }
            // });
        });
        
    </script>
@endsection
