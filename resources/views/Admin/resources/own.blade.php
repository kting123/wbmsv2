@extends('appv20')
@section('content')
<div id="page-wrapper">
   <br>

        <div class="row">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading">
                        <h4>From Farm</h4>
                    </div>
                    <div class="panel-body">
                        <div class="table table-responsive">
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12 results"
                                   xmlns="http://www.w3.org/1999/html">
                                <thead>
                                <th>Date Added</th>
                                <th>Body#</th>
                                <th>Gender</th>
                                <th>Color</th>
                                <th>Live Wt.</th>
                                <th>Slaugther Wt.</th>
                                <th>Meatshop Wt.</th>
                                <th>Current Sales</th>
                                <th>Action</th>
                                </thead>
                                <tbody>
                                @foreach($own as $data)
                                    <tr>
                                        <td>{{$data['date']}}</td>
                                        <td><a class="btn btn-outline btn-{{Auth::user()->buttons}}"
                                               href='./own_info{{$data['id']}}' data-toggle="tool-tip"
                                               title="see details">{{$data['id']}}</a></td>
                                        <td>{{$data['sex']}}</td>
                                        <td style="color:{{$data['color']}}">{{$data['color']}}</td>
                                        <td>{{$data['lw']}}</td>
                                        <td>{{$data['sw']}}</td>
                                          <?php $mw = 0; ?>
                                        @foreach($data['main_parts'] as $main)
                                        <?php $mw = $mw + $main['quantity']; ?>

                                        @endforeach
                                        <td>{{$mw}}</td>
                                        <?php $gross3 = 0; ?>
                                        @foreach($data['article_resources'] as $article)
                                            <?php $gross3 = $gross3 + $article['total_sales'];?>
                                        @endforeach
                                        <td><b>₱ {{number_format($gross3,2)}}</b></td>
                                        <td><a href="#editTabo{{$data['id']}}" data-toggle="modal"
                                               data-target="#editTabo{{$data['id']}}"><span
                                                        class="glyphicon glyphicon-edit"></span></a> | <a
                                                    href="#remove{{$data['id']}}" data-toggle="modal"
                                                    data-target="#remove{{$data['id']}}"><span
                                                        class="glyphicon glyphicon-trash"></span></a></td>

                                @endforeach
                                </tbody>
                            </table>

                        </div>
                        <div class="panel-footer">
                            <label class="pull-right">{!! $own->render() !!}</label>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @foreach($own as $data)
        <div class="modal fade" id="editTabo{{$data['id']}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit </h4>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post"
                              action="./edit_own/{{$data['id']}}"
                              id="form1"/>
                        <input type="hidden" name="_token"
                               value="{{{ csrf_token() }}}"/>
                        <div class="container col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="Date">Date Added:</label>
                                <input type='text' value="{{$data['date']}}"
                                       class="form-control"
                                       name="dateofpurchased" id='datetimepicker4'/>
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="bodyNum">Body #:</label>
                                <input type="text" value="{{$data['id']}}"
                                       placeholder="Body #"
                                       class="form-control" name="bodyNum">
                            </div>

                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="color">Color:</label>
                                <input type="text" value="{{$data['color']}}"
                                       placeholder="color"
                                       class="form-control" name="color">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="gender">Gender</label>
                                <select name="gender" class="form-control">
                                    <option>{{$data['sex']}}</option>
                                    <option>Male</option>
                                    <option>Female</option>
                                </select>

                            </div>


                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="weight">LW:</label>
                                <input type="text" value="{{$data['lw']}}"
                                       placeholder="kg" class="form-control"
                                       name="lw">
                            </div>
                            <div class="form-group col-lg-4 col-md-4 col-sm-4">
                                <label for="sw">SW:</label>
                                <input type="text" value="{{$data['sw']}}"
                                       placeholder="kg" class="form-control"
                                       name="sw">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span>
                            Cancel
                        </button>
                        <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-save"></span>
                            Save
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="modal fade" id="remove{{$data['id']}}" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close"
                                data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Remove Transaction</h4>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post"
                              action="./delete_own/{{$data['id']}}" id="form1"/>
                        <input type="hidden" name="_token"
                               value="{{ csrf_token() }}"/>
                        <div class="container col-lg-12  col-md-12">
                            <h5> Are you sure you want to delete this transaction?</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-ok"></span>
                            Confirm
                        </button>
                    </div>
                </div>
                </form>
            </div>
        </div>
    @endforeach
    <script>
        $(function () {
            $('#datetimepicker4').datepicker();
            $('#datetimepicker5').datepicker();
            $('#datetimepicker14').datepicker();
            $('#datetimepicker15').datepicker();
            //   $('[data-toggle = "tool-tip"]').tooltip();
        });
    </script>
@endsection
