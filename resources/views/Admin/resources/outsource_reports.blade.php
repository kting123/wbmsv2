@extends('appv20')
@section('content')
<div id="page-wrapper">
   <br>

        <div class="row">
            <div class="panel panel-{{Auth::user()->panels}}">
                <div class="panel-heading">
                    <h4>Outsourcing Reports</h4>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                            <thead style="border-bottom:solid 2px;">
                            <th></th>
                            <th>Article Name</th>
                            <th>Quantity (kg)</th>
                            <th>Unit Price (Php)</th>
                            <th>Total Price (Php)</th>
                            <th>Quantity Left (kg)</th>
                            <th>Total Sales (Php)</th>
                            <th>Total (Php)</th>
                            <!-- <th>Balance (Php)</th> -->
                            </thead>
                            <tbody>
                            <tr>
                                <td><b>Orderlists </b></td>
                            </tr>
                            @foreach($articles as $article)
                                <tr>
                                    <td></td>
                                    <td>{{$article['article']['name']}}</td>
                                    <td>{{$article->quantity}}</td>
                                    <td>{{number_format($article->unit_price,2)}}</td>
                                    <td>{{number_format($article->total_price,2)}}</td>
                                    <td>{{$article->remain_qty}}</td>
                                    <td></td>
                                    <td></td>

                                </tr>
                            @endforeach
                            <tr style="border-top:solid 2px; border-bottom:solid 2px;">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>TOTAL</td>
                                @if($article['outsource']['balance']=='0')
                                    <td><label class="label label-success"
                                               style="font-size:large;">₱ {{number_format($article['outsource']['amount'],2)}}</label>
                                    </td>
                                @elseif($article['outsource']['balance']<$article['outsource']['amount'])
                                    <td>
                                        <label style="font-size:large;"
                                               class="label label-warning">₱ {{number_format($article['outsource']['amount'],2)}}</label>
                                    </td>
                                @else
                                    <td>
                                        <label style="font-size:large;"
                                               class="label label-danger">₱ {{number_format($article['outsource']['amount'],2)}}</label>
                                    </td>
                                @endif
                            </tr>
                            <tr></tr>
                            <tr></tr>
                            <tr>
                                <td><h5><b>Sales</b></h5></td>
                            </tr>
                            <?php $total = 0; ?>
                            <?php $tot = 0; ?>
                            @foreach($articles as $article)
                                @foreach($article['orderlist'] as $order)
                                    <tr>
                                        <td></td>
                                        <td><a
                                                    @if($order['order']['or']=='')
                                                    href="./dr_order{{$order['order']['id']}}"
                                                    @else
                                                    href="./dr_order{{$order['order']['id']}}"
                                                    @endif
                                                    data-toggle="tool-tip" title="see info">{{$order['article']['name']}}</a>
                                        </td>
                                        <td>{{$order['qty']}}</td>
                                        <td>{{number_format($order['unit_price'],2)}}</td>
                                        <td>from : {{$order['from_article']}}</td>
                                        <td></td>
                                        <td>{{number_format($order['amount'],2)}}</td>
                                        <td></td>
                                    </tr>
                                    <?php $tot = $tot + $order['amount']; ?>
                                @endforeach
                            @endforeach
                            <tr style="border-top:solid 1px; border-bottom:solid 2px;">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>TOTAL SALES</td>
                                <td><b>₱ {{number_format($tot,2)}}</b></td>
                            </tr>
                            <tr></tr>
                            <tr></tr>
                            <tr>
                                <?php $total = $tot - $article['outsource']['amount']; ?>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><h4><b>Gross Profit</b></h4></td>
                                @if($total>0)
                                    <td style="background-color: #c0f080;"><b><br>₱ {{number_format($total,2)}}</b></td>
                                @elseif($total<0)
                                    <td style="background-color: #FFA07A;"><b><br>₱ {{number_format($total,2)}}</b></td>
                                @else
                                    <td style="background-color: #f3d17a;"><b><br>₱ {{number_format($total,2)}}</b></td>
                                @endif

                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                @if($article['outsource']['status'] == 'pending')
                    <button class="btn btn-outline btn-{{Auth::user()->buttons}}  btn-lg" data-toggle="modal" data-target="#myModal">
                        Pay Order
                    </button>
                @elseif($article['outsource']['status'] == 'partial')
                    <button class="btn btn-outline btn-{{Auth::user()->buttons}}  btn-lg" data-toggle="modal" data-target="#myModal">
                        Pay Order
                    </button>
                @else
                    <label href = "#"data-toggle="modal"
                           data-target="#myModal" class="label label-{{Auth::user()->labels}}" style="font-size:large;"><span
                                class="glyphicon glyphicon-ok"></span> Payment Posted
                        on {{$article['outsource']['date_of_check']}} </label>
                @endif

                <a href="./printOutsourceReport{{$article['outsource']['id']}}"
                   class="btn btn-outline btn-{{Auth::user()->buttons}}  btn-lg pull-right"><span
                            class="glyphicon glyphicon-export"></span>Export
                    Current File</a>
            </div>

        </div>
<br>
    </div>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-md">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Pay Order</h4>
                </div>
                <div class="modal-body">
                    <form type="hidden" method="post" action="./post_vendorPayment{{$article['outsource']['id']}}"
                          id="form1"/>
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                    <div class="table-responsive">
                        <td><h5><b>History of Payments</b></h5></td>
                        <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                            <thead style="border-bottom:solid 2px;">
                            <th></th>
                            <th>Date of Payment</th>
                            <th>Amount (Php)</th>
                            <th>OR #</th>
                            <th>Bank Name</th>
                            <th>Check No.</th>
                            <th>Balance (Php)</th>
                            </thead>
                            <tbody>
                            @foreach($history as $histories)
                                <tr>
                                    <td></td>
                                    <td>{{$histories['date_of_check']}}</td>
                                    <td>{{number_format($histories->amount_paid,2)}}</td>
                                    <td>{{$histories->or}}</td>
                                    <td>{{$histories->bank}}</td>
                                    <td>{{$histories->check}}</td>
                                    <td>{{number_format($histories->balance,2)}}</td>
                                </tr>
                            @endforeach
                            <tr style="border-top:solid 2px;">
                                <td style="background-color: #c6cad5;"><h5><b>Current Balance</b></h5></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;"></td>
                                <td style="background-color: #c6cad5;">
                                    <b><br>{{number_format($article['outsource']['balance'],2)}}</b></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    @if($article['outsource']['status'] == 'paid')
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-{{Auth::user()->buttons}}"><span class="glyphicon glyphicon-ok"></span>
                                OK
                            </button>
                        </div>
                    @else
                        <div class="col-lg-12">
                            <label class="checkbox-inline" style="margin-left: 10px;"> <input type="checkbox"
                                                                                              id="cheque">
                                Pay in Cheque</input>
                            </label>
                        </div>
                        <div class="form-group col-lg-12 col-md-12">

                            <div class="col-md-4 col-lg-4">
                                <label>Amount</label>
                                <input placeholder="" value="{{$article['outsource']['balance']}}" name="amount"
                                       class="form-control">
                                </input>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <label>OR No.</label>
                                <input placeholder="or" name="or" class="form-control">
                                </input>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <label>Date Of Payment</label>
                                <input placeholder="" name="dateofpayment" id="date2" class="form-control">
                                </input>
                            </div>
                            <div class=" col-md-4 col-lg-4" id="bank" hidden>
                                <label>Bank</label>
                                <input placeholder="Bank Name" name="bank" class="form-control" id="banks" disabled>
                                </input>
                            </div>
                            <div class=" col-md-4 col-lg-4" id="checkNum" hidden>
                                <label>Cheque No.</label>
                                <input placeholder="check no" name="checkno" id="check" class="form-control" disabled>
                                </input>
                            </div>
                        </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-{{Auth::user()->buttons}}"><span class="glyphicon glyphicon-ok"></span> Post
                        Payment
                    </button>
                </div>
                @endif
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {
                $('#date2').datepicker("setDate", "1d");
            });
        });

        $(document).ready(function () {
            $('#cheque').click(function () {
                if ($(this).is(":checked")) {
                    // check = 1;
                    $('#bank').show();
                    $('#checkNum').show();
                    document.getElementById("banks").disabled = false;
                    document.getElementById("check").disabled = false;

                }
                else {
                    $('#bank').hide();
                    $('#checkNum').hide();
                    document.getElementById("banks").disabled = true;
                    document.getElementById("check").disabled = true;
                    //  check = 0;
                }
            });
        });

    </script>

@endsection
