@extends('appv20')

@section('content')
<style>
#table {
    border: 0;
}

#table tr {
    display: flex;
}

#table td {
    flex: 1 auto;
    width: 2px;
}

#table thead tr:after {
}

#table thead th {
    flex: 1;
}

#table tbody {
    display: block;
    width: 100%;
    overflow-y: auto;
    height: 300px;
}
</style>
<div id="page-wrapper">
  <div class="row">
      <div class="col-lg-12">
        <?php $mydate = getdate();
        $date = "$mydate[weekday], $mydate[month] $mydate[mday], $mydate[year] ";?>
          <h3 class="page-header" style = "color:{{Auth::user()->h}}">Current Stock as of  {{$date}} <span data-toggle="modal" data-target="#editHeader"class = "glyphicon glyphicon-edit"></span></h3></div>
  </div>
  @include('editHeaderModal')
        <div class="row">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading"><h4>Viewing Stocks status</h4></div>
                    <div class="table table-responsive"  style = "height:350px;">
                        <table class="table table-hover" id = "table">
                            <thead>
                            <th>Articles</th>
                            <th>Quantity On Stock (Kls)</th>
                            </thead>
                            <tbody>
                            <?php $total1 = 0;?>
                            @foreach($articles as $data)
                                <?php $total = 0;?>
                                <tr>
                                    <td>{{$data['name']}}</td>
                                    @foreach($data['article_resources'] as $data1)
                                        <?php $total = $total + $data1['remain_qty']; ?>
                                    @endforeach
                                    <td>{{$total}}</td>
                                    <?php $total1 = $total1 + $total; ?>
                                </tr>
                            @endforeach
                            <tr>

                            </tr>
                            </tbody>
                        </table>
                    </div>
                      <div class="panel-footer">
                     <h3><b>TOTAL</b> :  <b>{{$total1}} Kls</b></h3>
                      </div>
                </div>
            </div>
        </div>
    </div>
@endsection
