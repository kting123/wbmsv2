<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-lg-2 col-sm-2 col-lg-offset-1">
            <div class="col-md-8 col-sm-8 col-lg-8">
                <div class="table table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td><b>Date</b></td>
                            <td><b>Body#</b></td>
                            <td><b>Gender</b></td>
                            <td><b>Area</b></td>
                            <td><b>Color</b></td>
                            <td><b>Owner</b></td>
                            <td><b>LW</b> Kgs.</td>
                            <td><b>SW</b> Kgs.</td>
                            <td><b>Price</b></td>
                        </tr>
                        <tr style="border-top:solid 2px;">
                            <td>{{$tabos['date']}}</td>
                            <td>{{$tabos['id']}}</td>
                            <td>{{$tabos['sex']}}</td>
                            <td>{{$tabos['area']}}</td>
                            <td>{{$tabos['color']}}</td>
                            <td>{{$tabos['owner']}}</td>
                            <td>{{$tabos['lw']}}</td>
                            <td>{{$tabos['sw']}}</td>
                            <td>{{number_format($tabos['price'],2)}}</td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="col-md-8 col-sm-8 col-lg-8">
                <div class="table table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td></td>
                            <td><b>Articles</b></td>
                            <td><b>Quantity</b></td>
                            <td><b>Remaining</b></td>
                            <td><b>Ordered</b></td>
                            <td><b>Price</b></td>
                            <td><b>Sales</b></td>
                            <td><b>Total</b></td>
                        </tr>
                        <tr>
                            <td><b>Sales : </b></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?php $gross = 0; ?>
                        @foreach($tabos['article_resources'] as $article)
                            @if($article['unit_price']!='0')
                                <tr>
                                    <td></td>
                                    <td>{{$article['article']['name']}}
                                    </td>
                                    <td>{{$article['quantity']}}</td>
                                    <td>{{$article['remain_qty']}}</td>
                                    <td>{{$article['sales_qty']}}</td>
                                    <td>{{number_format($article['unit_price'],2)}}</td>
                                    <td>{{number_format($article['total_sales'],2)}}</td>
                                    <td></td>
                                </tr>
                            @else
                                <?php $date[] = array(); ?>
                                @foreach($article['orderlist'] as $list)
                                    @if($list['order']['status']=='cancelled')
                                    @else
                                        <tr>
                                            <td></td>
                                            @if(in_array($article['article']['name'],$date))
                                                <td></td>
                                            @else
                                                <td>{{$article['article']['name']}}</td>
                                            @endif

                                            @if(in_array($article['quantity'],$date))
                                                <td></td>
                                            @else
                                                <td>{{$article['quantity']}}</td>
                                            @endif

                                            @if(in_array($article['remain_qty'],$date))
                                                <td></td>
                                            @else
                                                <td>{{$article['remain_qty']}}</td>
                                            @endif
                                            <td>{{$list['qty']}}</td>
                                            <td>{{$list['unit_price']}}</td>
                                            <td>{{$list['amount']}}</td>
                                        </tr>
                                        <?php $date = array($article['article']['name'], $article['quantity'], $article['remain_qty']); ?>
                                    @endif
                                @endforeach
                            @endif
                            <?php $gross = $gross + $article['total_sales'];?>
                        @endforeach
                        <tr style="border-top:solid 2px;">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>

                            <td>
                                <center><b>{{number_format($gross,2)}}</b></center>
                            </td>

                        </tr>


                        <tr>
                            <td><b>Expenses : </b></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?php $expenses = 0; ?>
                        @foreach($tabos['expenses'] as $expense)
                            <tr>
                                <td></td>
                                <td>{{$expense['name']}}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{number_format($expense['amount'],2)}}</td>
                                <td></td>
                            </tr>
                            <?php $expenses = $expenses + $expense['amount'];?>
                        @endforeach
                        <tr style="border-top:solid 2px;">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>Total Expenses (less)</b></td>
                            <td>
                                <b>{{number_format($expenses,2)}}</b>
                            </td>

                        </tr>
                        </tbody>

                    </table>
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>Gross</b></td>

                            <td><b>{{number_format($gross-$expenses,2)}}</b></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>Price (less)</td>
                            <td>{{number_format($tabos['price'],2)}}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>TOTAL PROFIT</b></td>

                            <td><b>{{number_format($gross-$expenses-$tabos['price'],2)}}</b></td>
                        </tr>
                        </tbody>
                    </table>

                </div>

            </div>

        </div>

    </div>


</div>