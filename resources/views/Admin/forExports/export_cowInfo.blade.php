<div class="container">
    <div class="row">
        <div class="col-md-8 col-sm-8 col-lg-8">
            <div class="table table-responsive">
                <table class="table table-bordered">
                    <tbody>
                    <tr>
                        <td><b>Date Added</b></td>
                        <td><b>Body#</b></td>
                        <td><b>Gender</b></td>
                        <td><b>Color</b></td>
                        <td><b>Live Wt.</b></td>
                        <td><b>Slaugther Wt.</b></td>
                        <td><b>MeatShop Wt.</b></td>
                    </tr>
                    <tr style="border-top:solid 2px;">
                        <td>{{$cows['date']}}</td>
                        <td>{{$cows['id']}}</td>
                        <td>{{$cows['sex']}}</td>
                        <td>{{$cows['color']}}</td>
                        <td>{{$cows['lw']}}</td>
                        <td>{{$cows['sw']}}</td>
                        <td>{{$cows['mw']}}</td>
                    </tr>
                    </tbody>
                </table>

            </div>
        </div>


        <div class="col-lg-2 col-md-2 col-sm-2 col-lg-offset-1">
            <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                <tbody>
                <tr>
                    <td><b>Beginning of Inventory From Slaughter</b></td>
                </tr>
                <tr>
                    <td><b>Article</b></td>
                    <td><b>Quantity</b></td>
                </tr>
                @foreach($cows['main_parts'] as $part)
                    <tr>
                        <td>{{$part['article']}}</td>
                        <td>{{$part['quantity']}}</td>
                    </tr>
                @endforeach
                <tr style="border-top:solid 2px;">
                    <td><b>Total</b></td>
                    <td><b>{{$cows['mw']}} Kls</b></td>
                </tr>
                <tbody>
            </table>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8">
            <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                <tbody>
                <tr>
                    <td><b>Cut Types</b></td>
                    <td><b>Articles</b></td>
                    <td><b>Quantity</b></td>
                    <td><b>Remaining</b></td>
                    <td><b>Ordered</b></td>
                    <td><b>Pack</b></td>
                    <td><b>Price</b></td>
                    <td><b>Sales</b></td>
                    <td><b>Total</b></td>
                </tr>
                <tr>
                    <td><b>Choice Cuts</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php $gross = 0; ?>
                @foreach($cows['article_resources'] as $article)
                    @if($article['type']=='Choice Cuts')
                        @if($article['unit_price']!='0')
                            <tr>
                                <td></td>
                                <td>{{$article['article']['name']}}
                                </td>
                                <td>{{$article['quantity']}}</td>
                                <td>{{$article['remain_qty']}}</td>
                                <td>{{$article['sales_qty']}}</td>
                                <td></td>
                                <td>{{number_format($article['unit_price'],2)}}</td>
                                <td>{{number_format($article['total_sales'],2)}}</td>
                                <td></td>
                            </tr>
                        @else
                            <?php $date[] = array(); ?>
                            @foreach($article['orderlist'] as $list)
                                @if($list['order']['status']=='cancelled')
                                @else
                                    <tr>
                                        <td></td>
                                        @if(in_array($article['article']['name'],$date))
                                            <td></td>
                                        @else
                                            <td>{{$article['article']['name']}}</td>
                                        @endif

                                        @if(in_array($article['quantity'],$date))
                                            <td></td>
                                        @else
                                            <td>{{$article['quantity']}}</td>
                                        @endif

                                        @if(in_array($article['remain_qty'],$date))
                                            <td></td>
                                        @else
                                            <td>{{$article['remain_qty']}}</td>
                                        @endif
                                        <td>{{$list['qty']}}</td>
                                        <td></td>
                                        <td>{{$list['unit_price']}}</td>
                                        <td>{{$list['amount']}}</td>
                                    </tr>
                                    <?php $date = array($article['article']['name'], $article['quantity'], $article['remain_qty']); ?>
                                @endif
                            @endforeach
                        @endif
                        <?php $gross = $gross + $article['total_sales'];?>
                    @else
                    @endif
                @endforeach
                <tr style="border-top:solid 2px;">
                    <td></td>
                    <td></td>
                    <td></td>

                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>{{number_format($gross,2)}}</b></td>
                </tr>

                <tr>
                    <td><b>Special Cuts</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php $gross2 = 0; ?>
                @foreach($cows['article_resources'] as $article)
                    @if($article['type']=='Special Cuts')
                    @if($article['unit_price']!='0')
                        <tr>
                            <td></td>
                            <td>{{$article['article']['name']}}
                            </td>
                            <td>{{$article['quantity']}}</td>
                            <td>{{$article['remain_qty']}}</td>
                            <td>{{$article['sales_qty']}}</td>
                            <td></td>
                            <td>{{number_format($article['unit_price'],2)}}</td>
                            <td>{{number_format($article['total_sales'],2)}}</td>
                            <td></td>
                        </tr>
                    @else
                        <?php $date[] = array(); ?>
                        @foreach($article['orderlist'] as $list)
                            @if($list['order']['status']=='cancelled')
                            @else
                                <tr>
                                    <td></td>
                                    @if(in_array($article['article']['name'],$date))
                                        <td></td>
                                    @else
                                        <td>{{$article['article']['name']}}</td>
                                    @endif

                                    @if(in_array($article['quantity'],$date))
                                        <td></td>
                                    @else
                                        <td>{{$article['quantity']}}</td>
                                    @endif

                                    @if(in_array($article['remain_qty'],$date))
                                        <td></td>
                                    @else
                                        <td>{{$article['remain_qty']}}</td>
                                    @endif
                                    <td>{{$list['qty']}}</td>
                                    <td></td>
                                    <td>{{$list['unit_price']}}</td>
                                    <td>{{$list['amount']}}</td>
                                </tr>
                                <?php $date = array($article['article']['name'], $article['quantity'], $article['remain_qty']); ?>
                            @endif
                        @endforeach
                    @endif
                    <?php $gross2 = $gross2 + $article['total_sales'];?>
                    @else
                    @endif
                @endforeach
                <tr style="border-top:solid 2px;">
                    <td></td>
                    <td></td>
                    <td></td>

                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>{{number_format($gross2,2)}}</b></td>
                </tr>


                <tr>
                    <td><b>Other Cuts</b></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <?php $gross3 = 0; ?>
                @foreach($cows['article_resources'] as $article)
                    @if($article['type']=='Other Cuts')
                    @if($article['unit_price']!='0')
                        <tr>
                            <td></td>
                            <td>{{$article['article']['name']}}
                            </td>
                            <td>{{$article['quantity']}}</td>
                            <td>{{$article['remain_qty']}}</td>
                            <td>{{$article['sales_qty']}}</td>
                            <td></td>
                            <td>{{number_format($article['unit_price'],2)}}</td>
                            <td>{{number_format($article['total_sales'],2)}}</td>
                            <td></td>
                        </tr>
                    @else
                        <?php $date[] = array(); ?>
                        @foreach($article['orderlist'] as $list)
                            @if($list['order']['status']=='cancelled')
                            @else
                                <tr>
                                    <td></td>
                                    @if(in_array($article['article']['name'],$date))
                                        <td></td>
                                    @else
                                        <td>{{$article['article']['name']}}</td>
                                    @endif

                                    @if(in_array($article['quantity'],$date))
                                        <td></td>
                                    @else
                                        <td>{{$article['quantity']}}</td>
                                    @endif

                                    @if(in_array($article['remain_qty'],$date))
                                        <td></td>
                                    @else
                                        <td>{{$article['remain_qty']}}</td>
                                    @endif
                                    <td>{{$list['qty']}}</td>
                                    <td></td>
                                    <td>{{$list['unit_price']}}</td>
                                    <td>{{$list['amount']}}</td>
                                </tr>
                                <?php $date = array($article['article']['name'], $article['quantity'], $article['remain_qty']); ?>
                            @endif
                        @endforeach
                    @endif
                    <?php $gross3 = $gross3 + $article['total_sales'];?>
                    @else
                    @endif
                @endforeach
                <tr style="border-top:solid 2px;">
                    <td></td>
                    <td></td>
                    <td></td>

                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>{{number_format($gross3,2)}}</b></td>
                </tr>


                <tr>
                    <?php $gross3 = $gross + $gross2 + $gross3 ?>
                    <td><b>TOTAL SALES</b></td>
                    <td></td>
                    <td></td>

                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><b>{{number_format($gross3,2)}}</b></td>
                </tr>
                <tbody>
            </table>
        </div>
    </div>
</div>