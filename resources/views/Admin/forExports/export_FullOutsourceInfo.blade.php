<div class="container-fluid">
    <div class="row">
        @if($type ==0)
            <div class="col-md-2 col-lg-2 col-sm-2 col-lg-offset-1">
                <div class="col-md-8 col-sm-8 col-lg-8">
                    <div class="table table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td><b>Date</b></td>
                                <td><b>Ordered From</b></td>
                                <td><b>Dr</b></td>
                                <td><b>OR</b></td>
                                <td><b>Amount</b></td>
                                <td><b>Due Date</b></td>
                                <td><b>Status</b></td>
                            </tr>
                            <tr style="border-top:solid 2px;">
                                <td>{{$outsources['date']}}</td>
                                <td>{{$outsources['ordered_from']}}</td>
                                <td>{{$outsources['dr']}}</td>
                                <td>{{$outsources['or']}}</td>
                                <td>{{$outsources['amount']}}</td>
                                <td>{{$outsources['due_date']}}</td>
                                <td>{{$outsources['status']}}</td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
                <div class="col-md-8 col-sm-8 col-lg-8">
                    <div class="table table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td></td>
                                <td><b>Articles</b></td>
                                <td><b>Quantity</b></td>
                                <td><b>Unit Price</b></td>
                                <td><b>Total Price</b></td>
                                <td><b>Total</b></td>
                            </tr>
                            <tr>
                                <td><b>From Supplier - {{$outsources['ordered_from']}} </b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @foreach($outsources['article_resources'] as $source)
                                <tr>
                                    <td></td>
                                    <td>{{$source['article']['name']}}</td>
                                    <td>{{$source['quantity']}}</td>
                                    <td>{{number_format($source['unit_price'],2)}}</td>
                                    <td>{{number_format($source['total_price'],2)}}</td>
                                    <td></td>
                                </tr>
                            @endforeach
                            <tr style="border-top:solid 2px;">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Total Dues</b></td>
                                <td>
                                    <b>{{number_format($outsources['amount'],2)}}</b>
                                </td>

                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>

                <div class="col-md-8 col-sm-8 col-lg-8">
                    <div class="table table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td></td>
                                <td><b>Articles</b></td>
                                <td><b>Quantity</b></td>
                                <td><b>Unit Price</b></td>
                                <td><b>Total Price</b></td>
                            </tr>
                            <tr>
                                <td><b>Sales</b></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php $tot = 0; ?>
                            @foreach($outsources['article_resources'] as $source)
                                @foreach($source['orderlist'] as $list)
                                    <tr>
                                        <td></td>
                                        <td>{{$list['article']['name']}}</td>
                                        <td>{{$list['qty']}}</td>
                                        <td>{{number_format($list['unit_price'],2)}}</td>
                                        <td>{{number_format($list['amount'],2)}}</td>
                                        <td></td>
                                    </tr>
                                @endforeach       <?php $tot = $tot + $source['total_sales']; ?>
                            @endforeach
                            <tr style="border-top:solid 2px;">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Total Sales</b></td>
                                <td>
                                    <b>{{number_format($tot,2)}}</b>
                                </td>
                            </tr>
                            <tr style="border-top:solid 2px;">
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Gross Profit</b></td>
                                <td>
                                    <b>{{number_format($tot-$outsources['amount'],2)}}</b>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @else
            <div class="col-md-2 col-lg-2 col-sm-2 col-lg-offset-1">
                <div class="col-md-8 col-sm-8 col-lg-8">
                    <div class="table table-responsive">
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td><b>Date</b></td>
                                <td><b>OR</b></td>
                                <td><b>DR</b></td>
                                <td><b>Ordered From</b></td>
                                <td><b>Due Date</b></td>
                                <td><b>Amount</b></td>
                                <td><b>Gross Sales</b></td>
                                <td><b>Income</b></td>
                            </tr>
                            <?php $tot_amount = 0; $tot_gross = 0; $tot_income = 0; ?>
                            @foreach($outsources as $sources)
                                <tr>
                                    <td>{{$sources['date']}}</td>
                                    <td>{{$sources['or']}}</td>
                                    <td>{{$sources['dr']}}</td>
                                    <td>{{$sources['ordered_from']}}</td>
                                    <td>{{$sources['due_date']}}</td>
                                    <td>{{number_format($sources['amount'],2)}}</td>
                                    <?php $gross = 0; ?>
                                    @foreach($sources['article_resources'] as $source )
                                        <?php $gross = $gross + $source['total_sales']; ?>
                                    @endforeach
                                    <?php $profit = $gross-$sources['amount']; ?>
                                    <td>{{number_format($gross,2)}}</td>
                                    <td>{{number_format($profit,2)}}</td>
                                </tr>
                                <?php $tot_amount = $tot_amount + $sources['amount']; $tot_gross = $tot_gross + $gross;  $tot_income = $tot_income + $profit; ?>
                            @endforeach
                            <tr>
                                <td></td>
                                <td></td>

                                <td></td>
                                <td></td>
                                <td><b>TOTAL</b></td>
                                <td><b>{{number_format($tot_amount,2)}}</b></td>
                                <td><b>{{number_format($tot_gross,2)}}</b></td>
                                <td><b>{{number_format($tot_income,2)}}</b></td>
                            </tr>
                            </tbody>
                        </table>
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Gross</b></td>

                                <td><b>{{number_format($tot_gross,2)}}</b></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td><b>Capital(Less)</b></td>

                                <td><b>{{number_format($tot_amount,2)}}</b></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>

                                <td><b>TOTAL PROFIT</b></td>

                                <td><b>{{number_format($tot_income,2)}}</b></td>
                            </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

        @endif


    </div>
</div>




