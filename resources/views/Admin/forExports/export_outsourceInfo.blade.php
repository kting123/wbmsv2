<div class="container-fluid">
    <div class="row">
        <div class="col-md-2 col-lg-2 col-sm-2 col-lg-offset-1">
            <div class="col-md-8 col-sm-8 col-lg-8">
                <div class="table table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td><b>Date</b></td>
                            <td><b>Ordered From</b></td>
                            <td><b>Dr</b></td>
                            <td><b>OR</b></td>
                            <td><b>Amount</b></td>
                            <td><b>Due Date</b></td>
                            <td><b>Status</b></td>
                        </tr>
                        <tr style="border-top:solid 2px;">
                            <td>{{$outsources['date']}}</td>
                            <td>{{$outsources['ordered_from']}}</td>
                            <td>{{$outsources['dr']}}</td>
                            <td>{{$outsources['or']}}</td>
                            <td>{{$outsources['amount']}}</td>
                            <td>{{$outsources['due_date']}}</td>
                            <td>{{$outsources['status']}}</td>
                        </tr>
                        </tbody>
                    </table>

                </div>
            </div>
            <div class="col-md-8 col-sm-8 col-lg-8">
                <div class="table table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td></td>
                            <td><b>Articles</b></td>
                            <td><b>Quantity</b></td>
                            <td><b>Unit Price</b></td>
                            <td><b>Total Price</b></td>
                            <td><b>Total</b></td>
                        </tr>
                        <tr>
                            <td><b>From Supplier - {{$outsources['ordered_from']}} </b></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        @foreach($outsources['article_resources'] as $source)
                            <tr>
                                <td></td>
                                <td>{{$source['article']['name']}}</td>
                                <td>{{$source['quantity']}}</td>
                                <td>{{number_format($source['unit_price'],2)}}</td>
                                <td>{{number_format($source['total_price'],2)}}</td>
                                <td></td>
                            </tr>
                        @endforeach
                        <tr style="border-bottom:solid 2px;">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>Total Dues</b></td>
                            <td>
                                <b>{{number_format($outsources['amount'],2)}}</b>
                            </td>

                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>

            <div class="col-md-8 col-sm-8 col-lg-8">
                <div class="table table-responsive">
                    <table class="table table-bordered">
                        <tbody>
                        <tr>
                            <td></td>
                            <td><b>Articles</b></td>
                            <td><b>Quantity</b></td>
                            <td><b>Unit Price</b></td>
                            <td><b>Total Price</b></td>
                        </tr>
                        <tr>
                            <td><b>Sales</b></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <?php $tot = 0; ?>
                        @foreach($outsources['article_resources'] as $source)
                            @foreach($source['orderlist'] as $list)
                                <tr>
                                    <td></td>
                                    <td>{{$list['article']['name']}}</td>
                                    <td>{{$list['qty']}}</td>
                                    <td>{{number_format($list['unit_price'],2)}}</td>
                                    <td>{{number_format($list['amount'],2)}}</td>
                                    <td></td>
                                </tr>
                                <?php $tot = $tot + $list['amount']; ?>
                            @endforeach
                        @endforeach
                        <tr style="border-top:solid 2px;">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>Total Sales</b></td>
                            <td>
                                <b>{{number_format($tot,2)}}</b>
                            </td>
                        </tr>
                        <tr style="border-top:solid 2px;">
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>Gross Profit</b></td>
                            <td>
                                <b>{{number_format($tot-$outsources['amount'],2)}}</b>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </div>


        </div>

    </div>

</div>
