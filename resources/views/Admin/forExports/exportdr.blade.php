<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="table-responsive">
                <table class="table table-hover col-lg-12 col-md-12 col-xs-12 results">
                    <tbody>
                        <tr>
                            <td><b>Date</b></td>
                            <td><b>DR#</b></td>
                            <td><b>Delivered to</b></td>
                            <td><b>Articles</b></td>
                            <td><b>Quantity</b></td>
                            <td><b>Amount</b></td>
                            <td><b>Total</b></td>
                            <td><b>Due Date</b></td>

                        </tr>
                        <?php $total=0; ?>
                        @foreach($order as $data)
                        <?php $date[] = array(); ?>
                        @foreach($data['orderlists'] as $list)
                        <tr>
                            @if(in_array($data['date'],$date))
                            <td></td>
                            @else
                            <td>{{$data['date']}}</td>
                            @endif

                            @if(in_array($data['dr'],$date))
                            <td></td>
                            @else
                            <td>{{$data['dr']}}</td>
                            @endif

                            @if(in_array($data['customer']['name'],$date))
                            <td></td>
                            @else
                            <td>{{$data['customer']['name']}}</td>
                            @endif

                            <td>
                                {{$list['article']['name']}}({{$list['article__resource_id']}})<br>
                            </td>
                            <td>
                                {{$list['qty']}}
                            </td>
                            <td>
                                Php {{$list['amount']}}<br>
                            </td>
                            @if(in_array($data['total_due'],$date))
                            <td></td>
                            @else
                           <td>Php {{number_format($data['total_due'], 2)}}</td>
                            @endif

                            @if(in_array($data['due_date'],$date))
                            <td></td>
                            @else
                            <td>{{$data['due_date']}}</td>
                            @endif
                        </tr>
                        <?php $date = array($data['date'], $data['total_due'], $data['customer']['name'], $data['due_date'], $data['dr']); ?>
                        @endforeach
                        <?php $total = $total + $data['total_due'];?>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>TOTAL</b></td>
                            <td><b>Php {{number_format($total, 2)}}</b></td>
                        </tr>
                    </tbody>
                </table>


            </div>
        </div>

    </div>
</div>
