@extends('appv20')

@section('content')
<br>

<div id="page-wrapper">
   <br>
        <div class="row">
          <div class= "panel panel-{{Auth::user()->panels}}">
            <div class="panel-body">
  <center><h4>Meatshop Inventory  from {{$date_from}} to {{$date_to}}</h4></center>
              <div class="col-md-12 col-lg-12">
                  <?php $sales_cow_tot = 0; $paid_cow_tot = 0; $receivable_cow_tot = 0;?>
                  @foreach($cows as $cow)
                      <?php $sales = 0; $paid = 0; ?>
                      @foreach($cow['article_resources'] as $source)
                          <?php $sales = $sales + $source['total_sales'];
                          $paid = $paid + $source['payment_posted']; ?>
                      @endforeach
                      <?php $sales_cow_tot = $sales_cow_tot + $sales;
                      $paid_cow_tot = $paid_cow_tot + $paid; ?>
                  @endforeach
                  <?php $receivable_cow_tot = $sales_cow_tot - $paid_cow_tot; ?>
                  <?php $sales_tabo_tot = 0; $paid_tabo_tot = 0; $expenses_tabo_tot = 0; $receivable_tabo_tot = 0; $profit_tabo = 0; $profit_tabo_current = 0;?>
                  @foreach($tabos as $tabo)
                      <?php $sales = 0; $paid = 0; $expenses = 0; $capital = 0; ?>
                      @foreach($tabo['article_resources'] as $source)
                          <?php $sales = $sales + $source['total_sales'];
                          $paid = $paid + $source['payment_posted']; ?>
                      @endforeach
                      <?php $sales_tabo_tot = $sales_tabo_tot + $sales;
                      $paid_tabo_tot = $paid_tabo_tot + $paid; ?>
                      @foreach($tabo['expenses'] as $expense)
                          <?php $expenses = $expenses + $expense['amount'];?>
                      @endforeach
                      <?php  $expenses_tabo_tot = $expenses_tabo_tot + $tabo['price'] + $expenses; ?>
                  @endforeach
                  <?php $receivable_tabo_tot = $sales_tabo_tot - $paid_tabo_tot;
                  $profit_tabo = $sales_tabo_tot - $expenses_tabo_tot;
                  $profit_tabo_current = $paid_tabo_tot - $expenses_tabo_tot;?>
                  <?php $sales_out_tot = 0; $paid_out_tot = 0; $expenses_out_tot = 0; $receivable_out_tot = 0; $profit_out = 0; $profit_out_current = 0;?>
                  @foreach($outsources as $outsource)
                      <?php $sales = 0; $paid = 0; $capital = 0; ?>
                      @foreach($outsource['article_resources'] as $source)
                          <?php $sales = $sales + $source['total_sales'];
                          $paid = $paid + $source['payment_posted']; ?>
                      @endforeach
                      <?php $sales_out_tot = $sales_out_tot + $sales;
                      $paid_out_tot = $paid_out_tot + $paid; ?>
                      <?php  $expenses_out_tot = $outsource['amount'] + $expenses_out_tot; ?>
                  @endforeach
                  <?php $receivable_out_tot = $sales_out_tot - $paid_out_tot;
                  $profit_out = $sales_out_tot - $expenses_out_tot;
                  $profit_out_current = $paid_out_tot - $expenses_out_tot;?>
                  <div class="table-responsive" style="border: solid 2px">
                      <table class="table table-hover col-lg-12 col-md-12 col-xs-12 results">
                          <tbody>
                          <tr>
                              <td><b>Sources</b></td>
                              <td><b>Expected Sales</b></td>
                              <td><b>Expenses/Capital</b></td>
                              <td><b>Expected Profit</b></td>
                          </tr>

                          <tr>
                              <td>From Farm</td>
                              <td>{{number_format($sales_cow_tot,2)}}</td>
                              <td></td>
                              <td>{{number_format($sales_cow_tot,2)}}</td>
                          </tr>

                          <tr>
                              <td>From Tabo Tabo</td>
                              <td>{{number_format($sales_tabo_tot,2)}}</td>
                              <td>{{number_format($expenses_tabo_tot,2)}}</td>
                              <td>{{number_format($profit_tabo,2)}}</td>
                          </tr>
                          <tr>
                              <td>From Vendor</td>
                              <td>{{number_format($sales_out_tot,2)}}</td>
                              <td>{{number_format($expenses_out_tot,2)}}</td>
                              <td>{{number_format($profit_out,2)}}</td>
                          </tr>
                          <tr style="font-family: bold;border:2px solid">
                              <td><b>TOTAL</b></td>
                              <td><b>{{number_format($sales_out_tot+$sales_tabo_tot+$sales_cow_tot,2)}}</b></td>
                              <td><b>{{number_format($expenses_tabo_tot+$expenses_out_tot,2)}}</b></td>
                              <td><b>{{number_format($sales_cow_tot+$profit_tabo+$profit_out,2)}}</b></td>
                              <?php $expected_profit = $sales_cow_tot + $profit_tabo + $profit_out; ?>
                          </tr>
                          <tr></tr>
                          <tr></tr>
                          <tr style="font-weight: bold;">
                              <td>Total Expected Sales</td>
                              <td>{{number_format($sales_out_tot+$sales_tabo_tot+$sales_cow_tot,2)}}</td>
                          </tr>
                          <tr style="font-weight: bold;">
                              <td>Total Receivables (Less)</td>
                              <td style="border-bottom: 2px solid">{{number_format($tot_receivables,2)}}</td>
                          </tr>
                          <tr style="font-weight: bold;">

                              <td><b>Current Sales</b></td>
                              <td></td>
                              <td><b>{{number_format($sales_out_tot+$sales_tabo_tot+$sales_cow_tot-$tot_receivables,2)}}</b>
                              </td>
                          </tr>
                          <tr style="font-weight: bold;">

                              <td>Expenses+Capital (less)</td>
                              <td></td>
                              <td style="border-bottom: 2px solid">{{number_format($expenses_tabo_tot+$expenses_out_tot,2)}}</td>
                          </tr>
                          <tr style="font-family: bold;">

                              <td><b>CURRENT PROFIT</b></td>
                              <td></td>
                              <td></td>
                              <?php $current_prof = ($sales_out_tot + $sales_tabo_tot + $sales_cow_tot - $tot_receivables) - ($expenses_tabo_tot + $expenses_out_tot);?>
                              <td style="border-bottom: 2px solid">
                                  <b>{{number_format($current_prof,2)}}</b>
                              </td>
                          </tr>
                          </tbody>
                      </table>
                  </div>
                  <form action="./exportAll" method="POST">
                      <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                      <input type="hidden" name="receivables" value="{{$tot_receivables}}"/>
                      <input type="hidden" name="date_from" value="{{$date_from}}"/>
                      <input type="hidden" name="date_to" value="{{$date_to}}"/>
                      <button type="submit" class="btn btn-{{Auth::user()->buttons}}  btn-lg">Export To Excel File</button>
                  </form>
              </div>
            </div>


    </div>
      </div>
        </div>
@endsection
