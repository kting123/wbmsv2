@extends('appv20')

@section('content')
    <style>
        @media (max-width: 1366px){
            .table-responsive {
                width: 100%;
                margin-bottom: 15px;
                overflow-y: hidden;
                overflow-x: scroll;
                -ms-overflow-style: -ms-autohiding-scrollbar;
                border: 1px solid #ddd;
                -webkit-overflow-scrolling: touch;
            }
        }

    </style>
    <div id="page-wrapper">
        <br>
        <div class="panel panel-{{Auth::user()->panels}}">
            <div class="panel-body">
               <div class="panel-heading ">
                   <center><h4>Report of {{$customer}} from {{$date_from}} to {{$date_to}}</h4></center>
               </div>
                <div class="table-responsive">
                    <table class="table table-bordered">
                        <thead>
                        <th>#</th>
                        <th>Date</th>
                        <th>Body ID</th>
                        <th>Sex</th>
                        <th>Live Weight</th>
                        <th>Slaugther Weight</th>
                        <th>Mall Weight</th>
                        <th>PO Number</th>
                        <th>Invoice Number</th>
                        <th>Recovery Rate</th>
                        <th>Gross</th>
                        <th>Less 0.01%</th>
                        <th>NET</th>
                        <th>Due Date</th>
                        <th>Voucher Number</th>
                        </thead>
                        <tbody>
                        @foreach($orders as $order)
                            <tr>
                                <td>{{$order['id']}}</td>
                                <td>{{$order['date']}}</td>
                                @foreach($order['orderlists'] as $list)
                                    <?php
                                    $bodyNumber = $list['article_resource']['tabo_tabo_id'];
                                    $sex = $list['article_resource']['tabo_tabo']['sex'];
                                    $lw = $list['article_resource']['tabo_tabo']['lw'];
                                    $sw = $list['article_resource']['tabo_tabo']['sw'];
                                    $mw = $list['qty'];
                                    $amount = $list['amount'];
                                    $unit_price = $list['unit_price'];?>
                                @endforeach
                                <?php $rate = $mw / $lw; $total_gross = 0;?>
                                <td>{{$bodyNumber}}</td>
                                <td>{{$sex}}</td>
                                <td>{{$lw}}</td>
                                <td>{{$sw}}</td>
                                <td>{{$mw}}</td>
                                <td>{{$order['dr']}}</td>
                                <td>{{$order['or']}}</td>
                                <td>{{number_format($rate*100,2)}} %</td>
                                <td>{{number_format($unit_price*$mw,2)}}</td>
                                <td>{{number_format(($unit_price*$mw)-$amount,2)}}</td>
                                <td>{{number_format($amount,2)}}</td>
                                <td>{{$order['due_date']}}</td>
                                <td>{{$order['voucher_no']}}</td>
                                <?php $total_gross = $total_gross + ($unit_price * $mw); ?>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <br>
                <center>
                    <form action="./exportCusts{{$id}}" method="POST">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <input type="hidden" name="date_from" value="{{$date_from}}"/>
                        <input type="hidden" name="date_to" value="{{$date_to}}"/>
                        <button type="submit" class="btn btn-{{Auth::user()->buttons}}  btn-lg">Export To Excel File
                        </button>
                    </form>
                </center>
            </div>
        </div>
    </div>
@endsection
