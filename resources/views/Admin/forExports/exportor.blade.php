<div class="container">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            @if($type=='partial')
                <div class="table-responsive">
                    <table class="table table-hover col-lg-12 col-md-12 col-xs-12 results">
                        <tbody>
                        <tr>
                            <td><b>Ref_DR #</b></td>
                            <td><b>OR#</b></td>
                            <td><b>Received From</b></td>
                            <td><b>Amount</b></td>
                            <td><b>Bank</b></td>
                            <td><b>Check #</b></td>
                            <td><b>Date Of Check</b></td>

                        </tr>
                        <?php $total = 0; ?>
                        @foreach($order as $data)
                            <tr>
                                <td>{{$data['order']['dr']}}</td>
                                <td>{{$data['or']}}</td>
                                <td>{{$data['customer']['name']}}</td>
                                <td>Php {{number_format($data['amount_paid'],2)}}</td>
                                <td>{{$data['bank']}}</td>
                                <td>{{$data['check']}}</td>
                                <td>{{$data['date_of_check']}}</td>
                            </tr>
                            <?php $total = $total + $data['amount_paid'];?>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td><b>TOTAL</b></td>
                            <td><b>Php {{number_format($total, 2)}}</b></td>
                        </tr>
                        </tbody>
                    </table>


                </div>
            @elseif($type =="walked-in")
                <div class="table-responsive">
                    <table class="table table-hover col-lg-12 col-md-12 col-xs-12 results">
                        <tbody>
                        <tr>
                            <td><b>Date</b></td>
                            <td><b>OR#</b></td>
                            <td><b>Received From</b></td>
                            <td><b>Articles</b></td>
                            <td><b>Quantity</b></td>
                            <td><b>Amount</b></td>
                            <td><b>Total Due</b></td>
                        </tr>
                        <?php $total = 0; ?>
                        @foreach($order as $data)
                            <?php $date[] = array(); ?>

                            @foreach($data['orderlists'] as $list)
                                <tr>
                                    @if(in_array($data['date'],$date))
                                        <td></td>
                                    @else
                                        <td>{{$data['date']}}</td>
                                    @endif

                                    @if(in_array($data['or'],$date))
                                        <td></td>
                                    @else
                                        <td>{{$data['or']}}</td>
                                    @endif

                                    @if(in_array($data['customer']['name'],$date))
                                        <td></td>
                                    @else
                                        <td>{{$data['customer']['name']}}</td>
                                    @endif

                                    <td>
                                        {{$list['article']['name']}}({{$list['article__resource_id']}})<br>
                                    </td>
                                    <td>
                                        {{$list['qty']}}
                                    </td>
                                    <td>
                                        Php {{$list['amount']}}<br>
                                    </td>
                                    @if(in_array($data['total_due'],$date))
                                        <td></td>
                                    @else
                                        <td><b>Php {{number_format($data['total_due'], 2)}}</b></td>
                                    @endif
                                </tr>
                                <?php $date = array($data['date'], $data['total_due'], $data['customer']['name'], $data['due_date'], $data['dr']); ?>

                            @endforeach
                            <?php $total = $total + $data['total_due'];?>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td><b>TOTAL</b></td>
                            <td><b>Php {{number_format($total, 2)}}</b></td>
                        </tr>

                        </tbody>
                    </table>


                </div>
            @elseif($type =="both")
                <div class="table-responsive">
                    <table class="table table-hover col-lg-12 col-md-12 col-xs-12 results">
                        <tbody>
                        <tr>
                            <td><b>Date</b></td>
                            <td><b>OR#</b></td>
                            <td><b>Received From</b></td>
                            <td><b>Amount</b></td>
                            <td><b>Bank</b></td>
                            <td><b>Check #</b></td>
                            <td><b>Date Of Check</b></td>
                            <td><b>Type</b></td>

                        </tr>
                        <?php $total = 0; ?>
                        @foreach($order as $data)
                            <tr>
                                <td>{{$data['date']}}</td>
                                <td>{{$data['or']}}</td>
                                <td>{{$data['customer']['name']}}</td>
                                <td>Php {{number_format($data['total_due'],2)}}</td>
                                <td>{{$data['bank']}}</td>
                                <td>{{$data['check']}}</td>
                                <td>{{$data['date_of_check']}}</td>
                                <td>{{$data['type']}}</td>
                            </tr>
                            <?php $total = $total + $data['total_due'];?>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td><b>TOTAL</b></td>
                            <td><b>Php {{number_format($total, 2)}}</b></td>
                        </tr>
                        </tbody>
                    </table>


                </div>
            @else
                <div class="table-responsive">
                    <table class="table table-hover col-lg-12 col-md-12 col-xs-12 results">
                        <tbody>
                        <tr>
                            <td><b>Date</b></td>
                            <td><b>OR#</b></td>
                            <td><b>Received From</b></td>
                            <td><b>Amount</b></td>
                            <td><b>Bank</b></td>
                            <td><b>Check #</b></td>
                            <td><b>Date Of Check</b></td>

                        </tr>
                        <?php $total = 0; ?>
                        @foreach($order as $data)
                            <tr>
                                <td>{{$data['date']}}</td>
                                <td>{{$data['or']}}</td>
                                <td>{{$data['customer']['name']}}</td>
                                <td>Php {{number_format($data['total_due'],2)}}</td>
                                <td>{{$data['bank']}}</td>
                                <td>{{$data['check']}}</td>
                                <td>{{$data['date_of_check']}}</td>
                            </tr>
                            <?php $total = $total + $data['total_due'];?>
                        @endforeach
                        <tr>
                            <td></td>
                            <td></td>
                            <td><b>TOTAL</b></td>
                            <td><b>Php {{number_format($total, 2)}}</b></td>
                        </tr>
                        </tbody>
                    </table>


                </div>
            @endif
        </div>

    </div>
</div>
