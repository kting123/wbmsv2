<div class="container-fluid">
    <div class="row">
        <div class="col-md-12 col-lg-12">
            <div class="table-responsive" style="border: solid 2px">
                <table class="table table-bordered col-lg-12 col-md-12 col-xs-12 results">
                  <thead>
                    <tr>
                      <th></th>
                      <th></th>
                      <th style="bottom-border:solid 2px;">Body/</th>
                      <th></th>
                      <th>Live</th>
                      <th>Slaugther</th>
                      <th>Mall</th>
                      <th>PO</th>
                      <th>Invoice</th>
                      <th>Recovery</th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th>Due</th>
                      <th></th>
                      <th></th>
                    </tr>
                    <tr>
                      <th>#</th>
                      <th>Date</th>
                      <th>ID</th>
                      <th>Sex</th>
                      <th>Weight</th>
                      <th>Weight</th>
                      <th>Weight</th>
                      <th>Number</th>
                      <th>Number</th>
                      <th>Rate</th>
                      <th>Gross</th>
                      <th>Less 0.01%</th>
                      <th>NET</th>
                      <th>Date</th>
                      <th>Voucher Number &nbsp;&nbsp;&nbsp;  &nbsp;&nbsp;&nbsp;</th>
                      <th>Remarks</th>
                    </tr>
                  </thead>
                    <tbody>
                      @foreach($orders as $order)
                      <tr>
                        <td>{{$order['id']}}</td>
                        <td>{{$order['date']}}</td>
                        @foreach($order['orderlists'] as $list)
                                <?php
                                $bodyNumber =  $list['article_resource']['tabo_tabo_id'];
                                $sex = $list['article_resource']['tabo_tabo']['sex'];
                                $lw = $list['article_resource']['tabo_tabo']['lw'];
                                $sw = $list['article_resource']['tabo_tabo']['sw'];
                                $mw = $list['qty'];
                                $amount = $list['amount'];
                                $unit_price = $list['unit_price'];
                                ?>
                        @endforeach
                        <?php $rate = $mw/$lw; $total_gross = 0;?>
                        <td>{{$bodyNumber}}</td>
                        <td>{{$sex}}</td>
                        <td>{{$lw}}</td>
                        <td>{{$sw}}</td>
                        <td>{{$mw}}</td>
                        <td>{{$order['dr']}}</td>
                        <td>{{$order['or']}}</td>
                        <td>{{number_format($rate*100,2)}} %</td>
                        <td>{{number_format($unit_price*$mw,2)}}</td>
                        <td>{{number_format(($unit_price*$mw)-$amount,2)}}</td>
                        <td>{{number_format($amount,2)}}</td>
                        <td>{{$order['due_date']}}</td>
                        <td>{{$order['voucher_no']}}</td>
                        <td></td>
                      </tr>
                      <?php $total_gross = $total_gross + ($unit_price*$mw); ?>
                      @endforeach
                    </tbody>
                </table>
            </div>
            <br>
        </div>
    </div>
</div>
