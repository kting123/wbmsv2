@extends('appv20')
@section('content')
        <!-- Morris Chart JavaScript -->
<script src="./assets/bower_components/raphael/raphael-min.js"></script>
<script src="./assets/bower_components/morrisjs/morris.min.js"></script>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header" style="color:{{Auth::user()->h}}">MEAT SHOP <span data-toggle="modal"
                                                                                      data-target="#editHeader"
                                                                                      class="glyphicon glyphicon-edit"></span>
            </h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    @include('editHeaderModal')
            <!-- /.row -->
    <div class="row">
        <div class="col-lg-4 col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-book fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div id="or" class="huge">{{$data['or']}}</div>
                            <div><h4>Official Reciepts</h4></div>
                        </div>
                    </div>
                </div>
                <a href="./or">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>

        <div class="col-lg-4 col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-shopping-cart fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div id="dr" class="huge">{{$data['dr']}}</div>
                            <div><h4>Delivery Receipts</h4></div>
                        </div>
                    </div>
                </div>
                <a href="./dr">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa  fa-list-alt fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div id="farm" class="huge">{{$data['farm']}}</div>
                            <div><h4>From Farm</h4></div>
                        </div>
                    </div>
                </div>
                <a href="./own">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa  fa-list-alt fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div id="outsource" class="huge">{{$data['outsource']}}</div>
                            <div><h4>From Vendors</h4></div>
                        </div>
                    </div>
                </div>
                <a href="./outsource">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="panel panel-danger">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa  fa-list-alt fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div id="tabo" class="huge">{{$data['tabotabo']}}</div>
                            <div><h4>From Tabo Tabo</h4></div>
                        </div>
                    </div>
                </div>
                <a href="./view_tabo_tabo">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <div class="row">
                        <div class="col-xs-3">
                            <i class="fa fa-database fa-5x"></i>
                        </div>
                        <div class="col-xs-9 text-right">
                            <div id="stocks" class="huge">{{$data['stocks_current']}}</div>
                            <div><h4>Current Stocks </h4></div>
                        </div>
                    </div>
                </div>
                <a href="./showStock">
                    <div class="panel-footer">
                        <span class="pull-left">View Details</span>
                        <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                        <div class="clearfix"></div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="panel panel-{{Auth::user()->panels}}">
        <div class="panel-heading">
            Revenue from Farm, Tabo tabo & Outsources Per month
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <div id="meatshop"></div>
        </div>
        <!-- /.panel-body -->
    </div>
    <!-- /.panel -->
    <input type="hidden" id="Walking" value="">
    <input type="hidden" id="delivery" value="">
    <div class="row">
        <div class="col-lg-6">
            <div class="panel panel-{{Auth::user()->panels}}">
                <div class="panel-heading">
                    Statistics : Delivery Vs. Walked In
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="sales"></div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->
        <div class="col-lg-6">
            <div class="panel panel-{{Auth::user()->panels}}">
                <div class="panel-heading">
                    Statistics : Farm Vs. Tabo Vs. Outsource
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="expenseVSprofit"></div>
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-6 -->
        <!-- /.col-lg-6 -->
    </div>
</div>
<script type="text/javascript">
    $(function () {
        setInterval(function () {
            $.ajax({
                url: 'API/getstats',
                type: "get",
                success: function (data) {
                    for (datus in data) {
                        $('#stocks').html(data.stocks_current);
                        $('#tabo').html(data.tabotabo);
                        $('#dr').html(data.dr);
                        $('#or').html(data.or);
                        $('#farm').html(data.farm);
                        $('#outsource').html(data.outsource);
                    }
                }
            });
        }, 5000)
    });
    Morris.Donut({
        element: 'sales',
        data: [{
            label: "Delivery",
            value: {{$delivery}}
        }, {
            label: "Walked In",
            value: {{$Walkein}}
        }],
        resize: true
    });

    Morris.Donut({
        element: 'expenseVSprofit',
        data: [{
            label: "Farm",
            value: {{$data['farm']}}
        }, {
            label: "TaboTabo",
            value: {{$data['tabotabo']}},
        },
            {
                label: "Outsource",
                value: {{$data['outsource']}}
            }
        ],
        resize: true
    });

    var month_data = [
            @foreach($cow as $key=>$c)
        {
            "period": "{{$key}}", "cow": {{$c}}},
            @endforeach
            @foreach($outsource as $key=>$c)
        {
            "period": "{{$key}}", "outsource": {{$c}}},
            @endforeach
            @foreach($tabotabo as $key=>$c)
        {
            "period": "{{$key}}", "tabotabo": {{$c}}},
        @endforeach
    ];
    Morris.Line({
        element: 'meatshop',
        data: month_data,
        xkey: 'period',
        ykeys: ['outsource', 'cow', 'tabotabo'],
        labels: ['Outsource', 'Farm', 'TaboTabo'],
        pointSize: 5,
        smooth: true,
        hideHover: 'auto',
        resize: true
    });
</script>
@endsection
