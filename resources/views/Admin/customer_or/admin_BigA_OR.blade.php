@extends('appv20')

@section('content')
    <div id="page-wrapper">
      <br>

        <div class="row">
                <div class="panel panel-{{Auth::user()->panels}}">
                  <div class="panel-heading">
                    <h4>Official Reciepts <i data-toggle="modal" data-target="#export" href = "#" class="pull-right fa fa-download fa-2x"></i> </h4>
                  </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12" id="full1">
                                <thead>
                                <th>Date of Order</th>
                                <th>OR#</th>
                                <th>Received From</th>
                                <th>Amount</th>
                                <th>Bank</th>
                                <th>Check#</th>
                                <th>Date Of Check</th>
                                <th>Action</th>

                                </thead>
                                <tbody>
                               
                                    <tr>
                                        <td>01/01/2016</td>
                                        <td><a class="btn btn-outline btn-{{Auth::user()->buttons}}"
                                               href="./BigA_or_breakdown" data-toggle="tool-tip"
                                               title="see details">0001</a></td>
                                        <td>Ann</td>
                                        <td>₱900.00</td>
                                        <td>BDO</td>
                                        <td>009811265300</td>
                                        <td>01/01/2016</td>
                                        
                                        <td><a href="#" data-toggle="modal"
                                                   data-target="#edit"><span
                                                            class="glyphicon glyphicon-edit"></span></a> |
                                                <a href="#" data-toggle="modal"
                                                   data-target="#remove"><span
                                                            class="glyphicon glyphicon-trash"></span></a></td>
                                      
                                    </tr>
                             
                                </tbody>
                            </table>
                            <table class="table table-hover col-lg-12 col-md-12 col-xs-12" id="partial1" hidden>
                                <thead>
                                <th>Ref_DR#</th>
                                <th>OR#</th>
                                <th>Received From</th>
                                <th>Amount</th>
                                <th>Bank</th>
                                <th>Check#</th>
                                <th>Date Of Check</th>
                                <th>Action</th>

                                </thead>
                                <tbody>
                                
                                        <tr>
                                            <td><a class="btn btn-outline btn-{{Auth::user()->buttons}} btn-xs"
                                                   href="./BigA_dr_breakdown" data-toggle="tool-tip"
                                                   title="see details">0001</a></td>
                                            <td>
                                                <label class="label label-default">00001</label>
                                            </td>
                                            <td>Ann</td>
                                            <td>₱900.00</td>
                                            <td>BDO</td>
                                            <td>98550298710</td>
                                            <td>01/01/2016</td>
                                            
                                      <td><a href="#" data-toggle="modal"
                                                   data-target="#edit1"><span
                                                            class="glyphicon glyphicon-edit"></span></a> |
                                                <a href="#" data-toggle="modal"
                                                   data-target="#remove1"><span
                                                            class="glyphicon glyphicon-trash"></span></a></td>
                                      

                                        </tr>
                                   
                                </tbody>
                            </table>

                            <script>
                                $(document).ready(function () {
                                    $(function () {
                                        $('#datetimepicker4').datepicker();
                                        $('#datetimepicker5').datepicker();
                                        $('#datetimepicker125').datepicker();
                                        $('#datetimepicker111').datepicker();
                                        $('#datetimepicker112').datepicker();

                                    });
                                });
                            </script>
                        </div>


                    </div>
                    <div class="panel-footer">
                            <label style="font-size:medium;">
                                <input type="radio" name="radBtn" id="full" value="1" checked> <b>
                                    Fully Paid</b>
                            </label>
                            &nbsp;&nbsp;
                            <label style="font-size:medium;">
                                <input type="radio" name="radBtn" id="partial" value="0"> <b>Partially Paid</b>
                            </label>
                      <label style = "padding-bottom: 0x;" class="pull-right">
                           
                      </label>
                    </div>
                    </div>

        </div>

        <!-- /.row -->
    </div>



        <div class="modal fade" id="edit" role="dialog">
            <div class="modal-dialog modal-lg">

                <!-- Modal content -->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><span class="glyphicon glyphicon-edit"></span> Edit Transaction</h4>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post" action="#" id="form1"/>
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                        <input type="hidden" name="month" value="#"/>
                        <input type="hidden" name="year" value="#"/>
                        <div class="container col-lg-12  col-md-12">

                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Date">Date</label>

                                <input disabled type='text' class="form-control" id='datetimepicker4' name="date"
                                       value="#"/>

                            </div>
                            <div class="form-group col-lg-3 col-md-6">
                                <label for="DR">OR No.</label>
                                <input type="text" value="#" placeholder="" class="form-control"
                                       name="or">
                            </div>
                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Deliver">Received From</label>
                                <input type="text" value="#" placeholder="" class="form-control"
                                       name="customer" disabled>
                            </div>

                            <div class="form-group col-lg-3  col-md-6">
                                <label for="gender">Amount</label>
                                <input type="text" value="#" placeholder="" class="form-control"
                                       name="amount" disabled>
                            </div>
                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Qty">Bank</label>
                                <input type="text" value="#" placeholder="" class="form-control"
                                       name="bank">
                            </div>
                            <div class="form-group col-lg-3  col-md-6">
                                <label for="gender">Check No.</label>
                                <input type="text" value="#" placeholder="" class="form-control"
                                       name="checkno">
                            </div>
                            <div class="form-group col-lg-3  col-md-6">
                                <label for="Qty">Date Of Check</label>
                                <input type="text" id='datetimepicker5' value="#"
                                       class="form-control" name="dateofcheck">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-outline btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-save"></span>
                            Save
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>
        <div class="modal fade" id="remove" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content -->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title"><span class="glyphicon glyphicon-remove"></span> Remove Transaction</h4>
                    </div>
                    <div class="modal-body">
                        <form type="hidden" method="post" action="#" id="form1"/>
                        <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                        <div class="container col-lg-12  col-md-12">
                            <h5> Are you sure you want to delete this transaction?</h5>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button data-dismiss="modal" class="btn btn-outline btn-danger"><span
                                    class="glyphicon glyphicon-remove"></span> Cancel
                        </button>
                        <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                    class="glyphicon glyphicon-ok"></span>
                            Confirm
                        </button>
                    </div>
                </div>
                </form>
            </div>

        </div>
   

    
            <div class="modal fade" id="edit1" role="dialog">
                <div class="modal-dialog modal-lg">

                    <!-- Modal content -->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><span class="glyphicon glyphicon-edit"></span> Edit Transaction</h4>
                        </div>
                        <div class="modal-body">
                            <form type="hidden" method="post" action="#" id="form1"/>
                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                            <div class="container col-lg-12  col-md-12">
                                <div class="form-group col-lg-3 col-md-6">
                                    <label for="DR">OR No.</label>
                                    <input type="text" value="#" placeholder="" class="form-control"
                                           name="or">
                                </div>
                                <div class="form-group col-lg-3  col-md-6">
                                    <label for="Deliver">Received From</label>
                                    <input type="text" value="#" placeholder=""
                                           class="form-control"
                                           name="customer" disabled>
                                </div>

                                <div class="form-group col-lg-3  col-md-6">
                                    <label for="gender">Amount</label>
                                    <input type="text" value="#" placeholder=""
                                           class="form-control"
                                           name="amount" disabled>
                                </div>
                                <div class="form-group col-lg-3  col-md-6">
                                    <label for="Qty">Bank</label>
                                    <input type="text" value="#" placeholder="" class="form-control"
                                           name="bank">
                                </div>
                                <div class="form-group col-lg-3  col-md-6">
                                    <label for="gender">Check No.</label>
                                    <input type="text" value="#" placeholder="" class="form-control"
                                           name="checkno">
                                </div>
                                <div class="form-group col-lg-3  col-md-6">
                                    <label for="Qty">Date Of Check</label>
                                    <input type="text" id='datetimepicker125' value="#"
                                           class="form-control" name="dateofcheck">
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-outline btn-danger"><span
                                        class="glyphicon glyphicon-remove"></span> Cancel
                            </button>
                            <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                        class="glyphicon glyphicon-save"></span>
                                Save
                            </button>
                        </div>
                    </div>
                    </form>
                </div>

            </div>
            <div class="modal fade" id="remove1" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content -->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title"><span class="glyphicon glyphicon-remove"></span> Remove Transaction
                            </h4>
                        </div>
                        <div class="modal-body">
                            <form type="hidden" method="post" action="#" id="form1"/>
                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                            <div class="container col-lg-12  col-md-12">
                                <h5> Are you sure you want to delete this transaction?</h5>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button data-dismiss="modal" class="btn btn-outline btn-danger"><span
                                        class="glyphicon glyphicon-remove"></span> Cancel
                            </button>
                            <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                        class="glyphicon glyphicon-ok"></span>
                                Confirm
                            </button>
                        </div>
                    </div>
                    </form>
                </div>

            </div>
       

    <div class="modal fade" id="export" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Export OR's</h4>
                </div>
                <div class="modal-body">
                    <form type="hidden" method="post" action="./exportOR" id="form1"/>
                    <input type="hidden" name="_token" value="{{{ csrf_token() }}}"/>
                    <div class="modal-body">
                        <div class="form-group col-lg-6 col-md-6 col-sm-6">
                            <label for="">Type</label>
                            <select type='text' name="type" class="form-control">
                                <option value = "1" >Fully Paid - Delivery</option>
                                <option value = "0" >Partially Paid - Delivery</option>
                                <option value = "2" >Walked-In</option>
                                <option value = "3" >Both Delivery & WalkedIn</option>

                            </select>
                        </div>
                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="">Specify Range</label>
                            <input type='text' name="date_from" class="form-control" placeholder="from"
                                   id='datetimepicker111' required/>
                        </div>

                        <div class="form-group col-lg-3 col-md-3 col-sm-3">
                            <label for="">&copy;</label>
                            <input type='text' name="date_to" class="form-control" placeholder="to"
                                   id='datetimepicker112' required/>
                        </div>

                        <div class="form-group col-lg-12 col-md-12">
                            <p><b>Note : </b> This will generate an excel file of Official Receipts.</p>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button data-dismiss="modal" class="btn btn-outline btn-danger"><span
                                class="glyphicon glyphicon-remove"></span>
                        Cancel
                    </button>
                    <button type="submit" class="btn btn-outline btn-{{Auth::user()->buttons}}"><span
                                class="glyphicon glyphicon-export"></span>
                        Export
                    </button>
                </div>
            </div>
            </form>
        </div>
    </div>
    </div>
    <script>
        $(document).ready(function () {
            // $(".search").keyup(function () {
            //     var searchTerm = $(".search").val();
            //     var listItem = $('.results tbody').children('tr').children('td');
            //     var searchSplit = searchTerm.replace(/ /g, "'):containsi('");
            //     $.extend($.expr[':'], {
            //         'containsi': function (elem, i, match, array) {
            //             return (elem.textContent || elem.innerText || '').toLowerCase().indexOf((match[3] || "").toLowerCase()) >= 0;
            //         }
            //     });
            //     $(".results tbody tr").not(":containsi('" + searchSplit + "')").each(function (e) {
            //         $(this).attr('visible', 'false');
            //     });
            //     $(".results tbody tr:containsi('" + searchSplit + "')").each(function (e) {
            //         $(this).attr('visible', 'true');
            //     });
            //     var jobCount = $('.results tbody tr[visible="true"]').length;
            //     $('.counter').text(jobCount + ' item');
            //     if (jobCount == '0') {
            //         $('.no-result').show();
            //     }
            //     else {
            //         $('.no-result').hide();
            //     }
            // });
        });
        $(document).ready(function () {
            $('input[type="radio"]').click(function () {
                if ($(this).attr('id') == 'full') {
                    $('#full1').show();
                    $('#partial1').hide();
                }
                else {
                    $('#full1').hide();
                    $('#partial1').show();
                }
            });
        });
    </script>
@endsection
