@extends('appv20')

@section('content')
    <div id="page-wrapper">
        <br>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading"><h4><span class="glyphicon glyphicon-info-sign"></span> Transaction
                            Breakdown</h4></div>
                    <div class="panel-body">
                        <label data-toggle="modal" data-target="#editList" class="label label-{{Auth::user()->labels}}"
                               style="font-size:medium;"> Order List &nbsp;<i
                                    class="fa fa-edit fa-fw"></i></label>
                        </br>
                        <div class="container col-lg-12 col-md-12 col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-hover col-lg-12 col-md-12 col-xs-12">
                                    <thead>
                                    <th>Medicines</th>
                                    <th># of Bottles</th>
                                    <th>Unit Price</th>
                                    <th>Amount</th>
                                    </thead>
                                    <tbody>
                                    
                                        <tr>
                                            <td> (<i href="#" data-toggle="modal"
                                                                                 data-target="#"
                                                                                 title="">Paracetamol </i>)
                                            </td>
                                            <td>7</td>
                                            <td>₱270.00</td>
                                            <td>₱1890.00</td>
                                        </tr>
                                   
                                    <tr>

                                        <td></td>
                                        <td></td>
                                        <td><strong>Total Dues</strong></td>
                                        <td><u><strong>₱1890.00</strong></u>
                                        </td>
                                    </tr>
                                    <tbody>
                                </table>

                            </div>
                            <div class="table-responsive">
                                <td><h5><b>History of Payments &nbsp;<i class="fa fa-edit fa-fw"></i></b></h5></td>
                                <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                                    <thead style="border-bottom:solid 2px;">
                                    <th></th>
                                    <th>Date of Payment</th>
                                    <th>Amount (Php)</th>
                                    <th>OR #</th>
                                    <th>Bank Name</th>
                                    <th>Check No.</th>
                                    <th>Balance (Php)</th>
                                    </thead>
                                    <tbody>
                                    
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    
                                    <tr style="border-top:solid 2px;">
                                        <td style="background-color: #c6cad5;"><h5><b>Current Balance</b></h5></td>
                                        <td style="background-color: #c6cad5;"></td>
                                        <td style="background-color: #c6cad5;"></td>
                                        <td style="background-color: #c6cad5;"></td>
                                        <td style="background-color: #c6cad5;"></td>
                                        <td style="background-color: #c6cad5;"></td>
                                        <td style="background-color: #c6cad5;">
                                            <b><br>0.00</b></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="pull-right">
                            <label class="label label-{{Auth::user()->labels}}" style="font-size:medium;">Payment
                                Posted </label>
                        </div>

                    </div>
                    <pre style="text-align: center">Customer : <b>Ann</b>  |  Date : <b>01/01/2016</b>  |  DR : <b>0001</b>  |  OR : <b>0001</b>  |  Status : <b>Paid</b>  |  Payment Posted : <b>01/01/2016</b></pre>
                </div>
            </div>

        </div>
    </div>
   
        <div class="modal fade" id="" role="dialog">
            <div class="modal-dialog modal-md">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Item Details</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            This item is coming from <b></b> which listed as part of: <b><br>
                                Farm Body Number : 
                                <br>Tabo Tabo Body Number : <br>
                                Vendor Id :</b>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    

        <div class="modal fade" id="editList" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit List</h4>
                    </div>
                    <form type="hidden" method="post" action="#" id="form1"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <input type="hidden" id="nowdate" name="nowdate" value=""/>
                    <input type="hidden" id="nowhr" name="hr" value=""/>
                    <input type="hidden" id="nowmin" name="min" value=""/>
                    <input type="hidden" id="nowsecs" name="secs" value=""/>
                    <input type="hidden" id="timestamp" name="timestamp" value=""/>
                   
                            <tr>
                                <td>
                                    <select style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'  name="" class="form-control">
                                        <option> </option>
                                       
                                            <option value=""></option>
                                       
                                    </select></td>
                                <td>
                                    <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' onkeyup="" class="form-control" value=""
                                           id=""
                                           name="" type="text"/>
                                    <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'  class="form-control"
                                           />
                                    <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'  class="form-control"
                                          />
                                </td>
                                <td>
                                    <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' onkeyup="" class="form-control"
                                           value="" id=""
                                           name="" type="text"/>
                                </td>
                                <td>
                                    <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' class="form-control" value="" name=""
                                           id=""
                                           type="text" readonly/>
                                </td>
                                <td>
                                    <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' class="form-control" value="" name=""
                                           id=""
                                           type="text" readonly/></td>
                                <td>
                                    <label id="" href = "#"data-toggle="tooltip" class = "label label-danger"title="Remove Item">X</label>
                                </td>
                            </tr>

                            <script>
                                
                            </script>
                           
                            
                            <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                            </table>
                    <div class="col-lg-6">
                        <input type="hidden" disabled id = "amount_due">
                        <b></b> <input type="hidden" disabled style  = "font-size:20px;"class="form-control" name = "total" id = "totalD" value = ""type="text">
                    </div>
                   </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-danger">Discard</button>
                <button class="btn btn-outline btn-{{Auth::user()->buttons}}">Save</button>
            </div>
            </form>
        </div>
        </div>
        </div>
 
@endsection
