@extends('appv20')

@section('content')
    <div id="page-wrapper">
        <br>

        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading"><h4><span class="glyphicon glyphicon-info-sign"></span> Transaction
                            Breakdown</h4></div>
                    <div class="panel-body">
                        <label data-toggle="modal" data-target="#editList" class="label label-{{Auth::user()->labels}}"
                               style="font-size:medium;"> Order List &nbsp;<i
                                    class="fa fa-edit fa-fw"></i></label>
                        </br>
                        <div class="container col-lg-12 col-md-12 col-xs-12">
                            <div class="table-responsive">
                                <table class="table table-hover col-lg-12 col-md-12 col-xs-12">
                                    <thead>
                                    <th>Article</th>
                                    <th>Quantity</th>
                                    <th>Unit Price</th>
                                    <th>Amount</th>
                                    </thead>
                                    <tbody>
                                    @foreach($data as $list)
                                        <tr>
                                            <td>{{$list['article']['name']}} (<i href="#" data-toggle="modal"
                                                                                 data-target="#{{$list['id']}}"
                                                                                 title="{{$list['from_article']}} ">{{$list['article__resource_id']}} </i>)
                                            </td>
                                            <td>{{$list['qty']}}</td>
                                            <td>₱{{number_format($list['unit_price'],2)}}</td>
                                            <td>₱{{number_format($list['amount'],2)}}</td>
                                        </tr>
                                    @endforeach
                                    <tr>

                                        <td></td>
                                        <td></td>
                                        <td><strong>Total Dues</strong></td>
                                        <td><u><strong>₱{{number_format($list['order']['total_due'],2)}}</strong></u>
                                        </td>
                                    </tr>
                                    <tbody>
                                </table>

                            </div>
                            <div class="table-responsive">
                                <td><h5><b>History of Payments &nbsp;<i class="fa fa-edit fa-fw"></i></b></h5></td>
                                <table class="table table-bordered col-lg-12 col-md-12 col-xs-12">
                                    <thead style="border-bottom:solid 2px;">
                                    <th></th>
                                    <th>Date of Payment</th>
                                    <th>Amount (Php)</th>
                                    <th>OR #</th>
                                    <th>Bank Name</th>
                                    <th>Check No.</th>
                                    <th>Balance (Php)</th>
                                    </thead>
                                    <tbody>
                                    @foreach($history as $histories)
                                        <tr>
                                            <td></td>
                                            <td>{{$histories['date_of_check']}}</td>
                                            <td>{{number_format($histories->amount_paid,2)}}</td>
                                            <td>{{$histories->or}}</td>
                                            <td>{{$histories->bank}}</td>
                                            <td>{{$histories->check}}</td>
                                            <td>{{number_format($histories->balance,2)}}</td>
                                        </tr>
                                    @endforeach
                                    <tr style="border-top:solid 2px;">
                                        <td style="background-color: #c6cad5;"><h5><b>Current Balance</b></h5></td>
                                        <td style="background-color: #c6cad5;"></td>
                                        <td style="background-color: #c6cad5;"></td>
                                        <td style="background-color: #c6cad5;"></td>
                                        <td style="background-color: #c6cad5;"></td>
                                        <td style="background-color: #c6cad5;"></td>
                                        <td style="background-color: #c6cad5;">
                                            <b><br>{{number_format($list['order']['balance'],2)}}</b></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="pull-right">
                            <label class="label label-{{Auth::user()->labels}}" style="font-size:medium;">Payment
                                Posted {{$list['order']['date_of_check']}}</label>
                        </div>

                    </div>
                    <pre style="text-align: center">Customer : <b>{{$list['order']['customer']['name']}}</b>  |  Date : <b>{{$list['order']['date']}}</b>  |  DR : <b>{{$list['order']['dr']}}</b>  |  OR : <b>{{$list['order']['or']}}</b>  |  Status : <b>{{$list['order']['status']}}</b>  |  Payment Posted : <b>{{$list['order']['date_of_check']}}</b></pre>
                </div>
            </div>

        </div>
    </div>
    @foreach($data as $list)
        <div class="modal fade" id="{{$list['id']}}" role="dialog">
            <div class="modal-dialog modal-md">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Item Details</h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            This item is coming from <b>{{$list['from_article']}}</b> which listed as part of: <b><br>
                                Farm Body Number : {{$list['article_resource']['cow_id']}}
                                <br>Tabo Tabo Body Number :{{$list['article_resource']['tabo_tabo_id']}} <br>
                                Vendor Id :{{$list['article_resource']['outsource_id']}}</b>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    @endforeach

        <div class="modal fade" id="editList" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Edit List</h4>
                    </div>
                    <form type="hidden" method="post" action="./edit_order{{$id}}" id="form1"/>
                    <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                    <input type="hidden" id="nowdate" name="nowdate" value=""/>
                    <input type="hidden" id="nowhr" name="hr" value=""/>
                    <input type="hidden" id="nowmin" name="min" value=""/>
                    <input type="hidden" id="nowsecs" name="secs" value=""/>
                    <input type="hidden" id="timestamp" name="timestamp" value=""/>
                        @include('reusable.order_add_edit')
                        <?php $i = 0; ?>
                        @foreach($data as $list)
                            <tr>
                                <td>
                                    <select style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'  name="articleD{{$i}}" class="form-control">
                                        <option value="{{$list['article']['name']}}">{{$list['article']['name']}}</option>
                                        @foreach($articles as $article)
                                            <option value="{{$article['name']}}">{{$article['name']}}</option>
                                        @endforeach
                                    </select></td>
                                <td>
                                    <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' onkeyup="ComputeTotal{{$i}}()" class="form-control" value="{{$list['qty']}}"
                                           id="qtyD{{$i}}"
                                           name="qtyD{{$i}}" type="text"/>
                                    <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'  class="form-control"
                                           @if($list['article_resource']['cow_id']!=Null)
                                           value="Farm"
                                           @elseif($list['article_resource']['tabo_tabo_id']!=Null)
                                           value="TaboTabo"
                                           @elseif($list['article_resource']['outsource_id']!=Null)
                                           value="Vendors"
                                           @endif
                                           id="type{{$i}}"
                                           name="type{{$i}}" type="hidden"/>
                                    <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'  class="form-control"
                                           @if($list['article_resource']['cow_id']!=Null)
                                           value="{{$list['article_resource']['cow_id']}}"
                                           @elseif($list['article_resource']['tabo_tabo_id']!=Null)
                                           value="{{$list['article_resource']['tabo_tabo_id']}}"
                                           @elseif($list['article_resource']['outsource_id']!=Null)
                                           value="{{$list['article_resource']['outsource_id']}}"
                                           @endif
                                           id="id{{$i}}"
                                           name="id{{$i}}" type="hidden"/>
                                </td>
                                <td>
                                    <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' onkeyup="ComputeTotal{{$i}}()" class="form-control"
                                           value="{{$list['unit_price']}}" id="unitPD{{$i}}"
                                           name="unitPD{{$i}}" type="text"/>
                                </td>
                                <td>
                                    <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' class="form-control" value="{{$list['amount']}}" name="TotalPD{{$i}}"
                                           id="TotalPD{{$i}}"
                                           type="text" readonly/>
                                </td>
                                <td>
                                    <input style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;' class="form-control" value="{{$list['article__resource_id']}}.{{$list['from_article']}}" name="articlesFrD{{$i}}"
                                           id="articlesFrD{{$i}}"
                                           type="text" readonly/></td>
                                <td>
                                    <a id="{{$list['amount']}}" href = "#"data-toggle="tooltip" class = "btn btn-outline btn-danger"title="Remove Item">X</a>
                                </td>
                            </tr>

                            <script>
                                //                                        var total = 0;
                                function ComputeTotal{{$i}}() {
                                    var qty = document.getElementById('qtyD{{$i}}').value;
                                    var unit_price = document.getElementById('unitPD{{$i}}').value;
                                    var amount = qty * unit_price;
//                                            console.log(amount);
//                                            total = Number(total) - Number(amount);
//                                            document.getElementById('total').value = total;
                                    document.getElementById('TotalPD{{$i}}').value = amount;

                                }
                            </script>
                            <?php $i++; ?>
                            @endforeach
                            <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                                <td></td>
                            </tr>
                            </tbody>
                            </table>
                    <div class="col-lg-6">
                        <input type="hidden" disabled id = "amount_due">
                        <b></b> <input type="hidden" disabled style  = "font-size:20px;"class="form-control" name = "total" id = "totalD" value = "{{$list['order']['total_due']}}"type="text">
                    </div>
                   </div>
                </div>
            </div>
            <div class="modal-footer">
                <button data-dismiss="modal" class="btn btn-danger">Discard</button>
                <button class="btn btn-outline btn-{{Auth::user()->buttons}}">Save</button>
            </div>
            </form>
        </div>
        </div>
        </div>
    @include('reusable.JsForAdd_Edit_Order')
    @include('reusable.getClientTime')
@endsection
