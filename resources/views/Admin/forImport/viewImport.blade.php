@extends('appv20')

@section('content')
    <style>
        @media (max-width: 1366px) {
            .table-responsive {
                width: 100%;
                margin-bottom: 15px;
                overflow-y: hidden;
                overflow-x: scroll;
                -ms-overflow-style: -ms-autohiding-scrollbar;
                border: 1px solid #ddd;
                -webkit-overflow-scrolling: touch;
            }
        }

    </style>
    <div id="page-wrapper">
        <br>
        <div class="panel panel-{{Auth::user()->panels}}">
            <div class="panel-body">
                <div class="panel-heading ">
                </div>
                <form type="hidden" method="post"
                      action="./importFile" id="form1" enctype="multipart/form-data"/>
                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                <input type='file' name = "file">
                <button type = "submit"> Upload </button>
                </form>
                <br>

            </div>
        </div>
    </div>
@endsection
