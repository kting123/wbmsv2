@extends('appv20')
@section('content')
    <style>
        .avatar {
            border-radius: 50%;
        }

        #link {
            color: RoyalBlue;
            font-weight: normal;
            text-decoration: none;

        }

        .avatar:hover {
            color: RoyalBlue;
            box-shadow: 0px 0px 3px 3px;
            border-radius: 50%;

        }
    </style>

    <div id="page-wrapper">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header" style="color:{{Auth::user()->h}}">User Profile <span data-toggle="modal"
                                                                                             data-target="#editHeader"
                                                                                             class="glyphicon glyphicon-edit"></span>
                </h1></div>
        </div>
        @include('editHeaderModal')

        <div class="row">
            <div class="container col-lg-12 col-md-12 col-sm-12">
                <div class="panel panel-{{Auth::user()->panels}}">
                    <div class="panel-heading"><h4>Edit Profile</h4></div>
                    <div class="panel-body">
                        <form type="hidden" method="post" action="./editProfileAdmin" id="form1"
                              enctype="multipart/form-data"/>
                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                        <br>
                        <div class="col-lg-12">
                            <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                <label class="label label-{{Auth::user()->labels}}"
                                       style="font-size: medium"> Basic Information</label></div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <input type="file" id="fileLoader" name="file" style="display:none"/>
                                <a href="#" id="link"> <img src="./assets/img/avatar{{Auth::user()->id}}.jpg"
                                                            class="avatar" width="128" height="128"
                                                            onclick="openfileDialog();"><img></a>
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="Date">Name</label>
                                <input type='text' name="name" class="form-control" value="{{Auth::user()->name}}"/>
                            </div>

                        </div>
                        <div class="col-lg-12">
                            <div class="form-group col-lg-12 col-md-12 col-sm-12">
                                <label class="label label-{{Auth::user()->labels}}" style="font-size: medium"> Login
                                    Information</label>
                            </div>
                            <div class="form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="dueDate">Email Address</label>
                                <input type='text' class="form-control" value="{{Auth::user()->email}}"
                                       name="email"/>
                            </div>
                            <div class="form-group form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="dueDate">New Password</label>
                                <input type='password' placeholder="*************" class="form-control" value=""
                                       name="old_password"/>
                            </div>
                            <div class="form-group form-group col-lg-3 col-md-3 col-sm-3">
                                <label for="Qty2">Confirm New Password</label>
                                <input type="password" placeholder="*************" value="" class="form-control"
                                       name="new_password">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-lg-offset-5"
                             style="padding-top:20px;padding-bottom:20px;">
                            <button class="btn btn-outline btn-{{Auth::user()->buttons}} btn-lg" type="submit"> Save
                                Changes
                            </button>

                        </div>

                    </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    </div>
    <script>
        function openfileDialog() {
            $('#fileLoader').click();
        }

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('.avatar').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
        $("#fileLoader").change(function () {
            readURL(this);
        });

    </script>
@endsection
