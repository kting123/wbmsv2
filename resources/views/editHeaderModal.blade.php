<div class="modal fade" id = "editHeader">
    <div class="modal-dialog">
        <button class = "pull-right"data-dismiss="modal">X</button>
        <form  action="./editHeader_{{Auth::user()->id}}" method="post">
            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
            <div class="col-lg-3 col-lg-offset-3">
                <label style="color:white">Select Color :</label>
                <input type="color" name="header" value="{{Auth::user()->h}}">
                <br>
                <br>
                <button type="submit" class = "btn btn-{{Auth::user()->buttons}} btn-lg"> Save</button>
            </div>

        </form>
    </div>
</div>