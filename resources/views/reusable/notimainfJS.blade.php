<span id = "time1_{{$notify->id}}" class="text-muted small"></span>
<script>
      /* get the client time */
      var n = new Date();
      var hour = n.getHours();
      var minutes = n.getMinutes();
      var seconds = n.getSeconds();
      var timestamp = Math.floor(Number(n.getTime()/1000));
      var timenow = timestamp - Number({{$notify->timestamp}});
      if(timenow>86400 && timenow<172800){
         document.getElementById('time1_{{$notify->id}}').innerHTML = 'Yesterday';
      }
      else if(timenow<86400){
        if(hour>={{$notify->hr}}){
            timelaps = ((hour*60)+minutes)-Number({{$notify->min}}+(60*{{$notify->hr}}));
        if(timelaps<60){
           document.getElementById('time1_{{$notify->id}}').innerHTML = timelaps + " minutes ago";
        }
        else{
          var now = Math.floor(timelaps/60);
          if(now<=1){
             document.getElementById('time1_{{$notify->id}}').innerHTML =  now + " hour ago";
           }
           else {
             document.getElementById('time1_{{$notify->id}}').innerHTML =  now + " hours ago";
           }
        }
      }
      else{
        document.getElementById('time1_{{$notify->id}}').innerHTML = 'Yesterday';
      }
      }
    else if(timenow>172800){
        if(Number({{$notify->hr}})>=12){
             document.getElementById('time1_{{$notify->id}}').innerHTML =  "{{$notify->date}}" + " at " + Number({{$notify->hr}}-12) +":"+Number({{$notify->min}})+"PM";
           }
           else {
             document.getElementById('time1_{{$notify->id}}').innerHTML =  "{{$notify->date}}" + " at " + Number({{$notify->hr}}) +":"+Number({{$notify->min}})+"AM";
           }
      }
    //  alert(timenow);
  </script>
