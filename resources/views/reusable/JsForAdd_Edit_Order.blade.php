<script type="text/javascript">
    var totalD = 0;
    var i =  {{$i}};
    var check = 0;
    var articlesFrom;
    var qtyFrom;
    var articles;
    var qty;
    var type1;
    var TotalD = 0;
    var selected1;
    $('table').on('click', 'tr a', function (e) {
        e.preventDefault();
        var minus = $(this).attr("id");
        //  console.log(minus);
        TotalD = TotalD - minus;
        document.getElementById("totalD").value = TotalD;
        $(this).parents('tr').remove();

    });
    $('#list1').on('click', 'tr label', function (e) {
        e.preventDefault();
        var minus = $(this).attr("id");
        console.log(minus);
        TotalD = document.getElementById("totalD").value
        TotalD = Number(TotalD) - Number(minus);
        document.getElementById("totalD").value = TotalD;
        $(this).parents('tr').remove();

    });
    function AddData() {
        var rows = "";
        var totalP = "";
        var articles = document.getElementById("articles").value;
        var qty = document.getElementById("Qty").value;
        var unit = document.getElementById("unit").value;
        var type = type1;
        if (selected1 == '') {
            alert("Please input valid Ref_ID.");
        }
        else if (qty == '') {
            alert("Please input valid quantiy.");
        }
        else if (unit == '') {
            alert("Please input valid unit price.");
        }
        else {
            if (check === 1) {
                articlesFrom = document.getElementById("articlesFrom").value;
            }
            else if (check === 0) {
                articlesFrom = articles;
            }
            var con = parseInt(totalD);

            totalP = qty * unit;
            var TotalP = totalP.toFixed(2);
            totalD = document.getElementById("totalD").value;
            totalD = Number(totalD) + totalP;
            TotalD = Number(totalD).toFixed(2);
            rows += "<tr><td><input hidden name=id" + i + " value='" + selected1 + "'> <input hidden name=type" + i + " value='" + type + "'> <input name=articleD" + i + " value='" + articles + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=qtyD" + i + " value='" + qty + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=unitPD" + i + " value='" + unit + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input readonly name=TotalPD" + i + " value='" + TotalP + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td><td><input name=articlesFrD" + i + " value='" + articlesFrom + "' style='width: 100px;color: #000000; background-color: transparent;height: 100%;font-size:14px; width: 100%; border: 0;padding: 0px 0px 0px;'></td> <td> <a id=" + TotalP + " data-toggle='tooltip' class = 'btn btn-outline btn-danger'title='Remove Item'>X</a></td></tr>";
            $(rows).appendTo("#list1 tbody");
            //   alert(totalD);
            document.getElementById("totalD").value = TotalD;
            document.getElementById("amount_due").value = TotalD;
            i++;

        }
    }

    $(document).ready(function () {

        $('#getFrom').click(function () {
            if ($(this).is(":checked")) {
                check = 1;
                $('#sd').show();

            }
            else {
                check = 0;
                $('#sd').hide();
            }
        });
        function logOptgroupLabel(id) {
            var elt = $('#' + id)[0];
            var label = $(elt.options[elt.selectedIndex]).closest('optgroup').prop('label');
            return label;
        }

        $("#changeCow").change(function () {
            type1 = logOptgroupLabel(this.id);
            selected1 = $(this).val();
            /* ajax request in getting the  notifications*/
            $.ajax({
                url: 'API/getArticles',
                type: "get",
                data: {'id': selected1, 'type': type1},
                success: function (data) {
                    var i = 0;
                    $("#articlesFrom").find('option').remove().end().append('<option value="Select">Select</option>')
                    for (info in data) {
                        if (data[i].remain_qty <= 0) {

                        }
                        else {
                            $('#articlesFrom').append('<option id = "' + data[i].id + '"value ="' + data[i].id + '.' + data[i].article_name + '">' + data[i].article_name + '</option>');
                        }
                        i++;
                    }
                }
            });

        });
        $("#articlesFrom").change(function () {
            var resource_id = $(this).children(":selected").attr("id");
            //  console.log(resource_id);
            /* ajax request in getting the  notifications*/
            $.ajax({
                url: 'API/getQty',
                type: "get",
                data: {'id': resource_id},
                success: function (data) {
//                    console.log(data);
                    document.getElementById("Qty").value = data;
                }
            });

        });
    });
    function ResetTable() {
        i = 0;
        $('#list1 tbody tr').remove();
        document.getElementById("totalD").value = '';
        totalD = 0;
    }
</script>