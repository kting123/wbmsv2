<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="form-group col-lg-6 col-md-6 col-sm-6">
        <label class="checkbox-inline" style="margin-left: 10px;"> <input
                    type="checkbox"
                    id="getFrom"><b> Get
                from
                Available Articles&copy;</b></input>
        </label>
    </div>
</div>
<div class="col-lg-12 col-md-12 col-sm-12">
    <div class="col-lg-2 col-md-2 col-sm-2">
        <label for="dueDate">Ref_ID</label>
        <select id="changeCow"
                class="form-control">
            <option value="">Sources</option>
            <optgroup id="farm1" label="Farm">
                @foreach($cow as $cows)
                    <?php $i = 0; ?>
                    @foreach($cows['article_resources'] as $item)
                        <?php $i = $i + $item->remain_qty; ?>
                    @endforeach
                    @if($i<='0')
                    @else
                        <option value="{{$cows->id}}">{{$cows->id}}</option>
                    @endif
                @endforeach
            </optgroup>
            <optgroup label="TaboTabo">
                @foreach($tabo_tabos as $tabo)
                    <?php $k = 0; $article_id = "";?>
                    @foreach($tabo['article_resources'] as $item1)
                        <?php $k = $k + $item1->remain_qty; $article_id = $item1->article_id;?>
                    @endforeach
                    @if($k<='0')

                    @else
                        @if($article_id=='1')
                        @else
                            <option value="{{$tabo->id}}">{{$tabo->id}}</option>
                        @endif
                    @endif
                @endforeach
            </optgroup>
            <optgroup label="Vendors">
                @foreach($outsources as $outsource)
                    <?php $j = 0; ?>
                    @foreach($outsource['article_resources'] as $item2)
                        <?php $j = $j + $item2->remain_qty; ?>
                    @endforeach
                    @if($j<=0)
                    @endif
                    @if($j>0)
                        <option value="{{$outsource->id}}">{{$outsource->id}}</option>
                    @endif
                @endforeach
            </optgroup>
        </select>
    </div>
    <div class="container col-lg-2 col-md-2 col-sm-2" id="sd" hidden>
        <label for="articleF">Articles From</label>
        <select class="form-control" id="articlesFrom">
            <option value="null">Select</option>
        </select>
    </div>
    <div class="form-group col-lg-2 col-md-2 col-sm-2">
        <label for="gender">Articles to</label>
        <select id="articles" class="form-control" size="1">
            @foreach($articles as $article)
                <option value="{{$article->name}}">{{$article->name}}</option>
            @endforeach
        </select>
    </div>

    <div class="form-group col-lg-2 col-md-2 col-sm-2">
        <label for="Qty">Qty</label>
        <input type="text" value="" placeholder="kg" class="form-control"
               data-toggle = "tool-tip" title = "Available quantity" id="Qty">
    </div>
    <div class="form-group col-lg-2 col-md-2 col-sm-2">
        <label for="unit">Unit Price</label>
        <input type="text" value="" placeholder="" class="form-control"
               id="unit">
    </div>
    <div class="form-group col-lg-2 col-md-2 col-sm-2" style="margin-top: 22px;">
        <button class="btn btn-{{Auth::user()->buttons}}" type="button"
                onclick="AddData()">
            <span class="glyphicon glyphicon-plus"></span> Item
        </button>
        <!-- <input id = "button" type = "button" value = " Add " onclick = "AddData()"> -->
    </div>
    <div class="col-lg-12 col-md-12 col-sm-12">
        <div class="table table-responsive">
            <table class="table table-condensed" id="list1">
                <thead id="tblHead">
                <tr>
                    <th>Articles</th>
                    <th>Qty. (Kg)</th>
                    <th>Unit Price</th>
                    <th>Total Price (Php)</th>
                    <th>Articles From</th>
                </tr>
                </thead>
                <tbody>

