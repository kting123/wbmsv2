<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Cow extends Model
{

    protected $table = 'cows';
    public $timestamps = false;
    //
    public function article_resources()
    {
        return $this->hasMany('\App\Article_Resource');
    }

    public function main_parts()
    {
        return $this->hasMany('\App\MainParts');
    }


}
