<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TaboTabo extends Model {

	//
    protected $table = 'tabo_tabos';
    public $timestamps = false;
    public function article_resources(){
        return $this->hasMany('\App\Article_Resource');
    }

    public function expenses() {
        return $this->hasMany('\App\Expense');
    }
    protected $fillable = ['status','customer'];
    
}
