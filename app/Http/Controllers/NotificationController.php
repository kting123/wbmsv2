<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class NotificationController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	 public function __construct()
	 {
			 $this->middleware('auth');
	 }
	 public function getNotifications()
	{
    /*notifications HERE */
    $notifications= \App\Nofication::where('seen','!=',\Auth::user()->id)->orderBy('id','desc')->take(5)->get()->toArray();
    /*notifications HERE */
			return response()->json($notifications);
	}

	public function getNotificationsCounts()
 {
 	/*notifications HERE */
 	$notifications_not_seen = \App\Nofication::where('seen','!=',\Auth::user()->id)->count();
 	/*notifications HERE */
 	//dd($notifications);
 		return response()->json($notifications_not_seen);
 }

}
