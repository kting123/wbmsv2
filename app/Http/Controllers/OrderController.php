<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class OrderController extends Controller
{

    /**
     * Created by PhpStorm.
     * User: Sui~yans
     * Date: 5/22/2016
     * Time: 5:56 PM
     */


    public function AddOrder()
    {
        $order = \Input::all();
         // dd($order);
        $Order = new \App\Order;
        $articles = array_slice($order, 14, -1);
       // dd($articles);
        $count = count($articles);
        $k = ($count) / 7;
        $w = 0;
        $l = 0;
        $s = 6;
        foreach ($articles as $key => $item) {
            if (substr($key, 0, 2) == "id") {
                $arti['id' . $l] = $item;
            }
            if (substr($key, 0, 4) == "type") {
                $arti['type' . $l] = $item;
            }
            if (substr($key, 0, 8) == "articleD") {
                $arti['articleD' . $l] = $item;
            }
            if (substr($key, 0, 4) == "qtyD") {
                $arti['qtyD' . $l] = $item;
            }
            if (substr($key, 0, 6) == "unitPD") {
                $arti['unitPD' . $l] = $item;
            }
            if (substr($key, 0, 7) == "TotalPD") {
                $arti['TotalPD' . $l] = $item;
            }
            if (substr($key, 0, 11) == "articlesFrD") {
                $arti['articlesFrD' . $l] = $item;
            }
            if ($w == $s) {
                $l++;
                $s = $s + 7;
            }
            $w++;
        }
       // dd($arti);
        if ($order['radBtn'] == '1') {
            if ($order['deliverto'] == '' || $order['dr'] == '') {
                \Flash::warning('Missing required inputs.');
                return \Redirect::back();
            }
            $Order->date = $order['date1'];
            $Order->dr = $order['dr'];
            $cust = \App\Customer::firstOrCreate(array('name' => $order['deliverto'], 'customer_type' => '2'));
            $Order->customer_id = $cust->id;
            $Order->due_date = $order['duedate'];
            $Order->type = 'delivery';
            $Order->status = 'pending';
            $Order->balance = $order['Tamount'];


        } else if ($order['radBtn'] == '0') {
//
            if ($order['or'] == '' || $order['sold'] == '') {
                \Flash::warning('Missing required inputs.');
                return \Redirect::back();
            }
            $Order->date = $order['date3'];
            $Order->or = $order['or'];
            $cust = \App\Customer::firstOrCreate(array('name' => $order['sold'], 'customer_type' => '0'));
            $Order->customer_id = $cust->id;
            $Order->type = 'walked-in';
            $Order->status = 'paid';
            $Order->bank = 'cash';
            $Order->amount_paid = $order['Tamount'];
        }

        for ($i = 0; $i < $k; $i++) {
            $article_id_to = $this->getArticleId($arti['articleD' . $i]);
            $article_id_from = $this->getArticleId($arti['articlesFrD' . $i]);
            $from = explode('.', $arti['articlesFrD' . $i]);
            //  dd($from);
            if ($arti['type' . $i] == 'Farm') {
                if ($article_id_to == $article_id_from) {
                    $article_from = $arti['articleD' . $i];
                    $cow_article = \App\Article_Resource::where('cow_id', $arti['id' . $i])->where('article_id', $article_id_to)->get();
                } else {
                    $resource_id = $from['0'];
                    $article_from = $from['1'];
                    $cow_article = \App\Article_Resource::where('cow_id', $arti['id' . $i])->where('id', $resource_id)->get();
                }
            } elseif ($arti['type' . $i] == 'TaboTabo') {
                if ($article_id_to == $article_id_from) {
                    $article_from = $arti['articleD' . $i];
                    $cow_article = \App\Article_Resource::where('tabo_tabo_id', $arti['id' . $i])->where('article_id', $article_id_to)->get();
                } else {
                    $resource_id = $from['0'];
                    $article_from = $from['1'];
                    $cow_article = \App\Article_Resource::where('tabo_tabo_id', $arti['id' . $i])->where('id', $resource_id)->get();
                }
            } elseif ($arti['type' . $i] == 'Vendors') {
                if ($article_id_to == $article_id_from) {
                    $article_from = $arti['articleD' . $i];
                    $cow_article = \App\Article_Resource::where('outsource_id', $arti['id' . $i])->where('article_id', $article_id_to)->get();
                } else {
                    $resource_id = $from['0'];
                    $article_from = $from['1'];
                    $cow_article = \App\Article_Resource::where('outsource_id', $arti['id' . $i])->where('id', $resource_id)->get();
                }
            }
            $check1 = json_decode(json_encode($cow_article), true);
            //  dd($check1);
            if ($check1 == null) {
                \Flash::warning('No articles found for the specific Body Number, Please add it first.Thanks!');
                return \Redirect::back();
            } else {
                foreach ($cow_article as $article) {
                    //  dd($article->sales_qty);
                    $article->sales_qty = $article->sales_qty + $arti['qtyD' . $i];
                    $article->remain_qty = $article->remain_qty - $arti['qtyD' . $i];
                    $article->total_sales = $article->total_sales + $arti['TotalPD' . $i];
                    $article->save();
                }
                $Order->total_due = $order['Tamount'];
                $Order->save();
                $orderlist = new \App\Orderlist;
                $orderlist->order_id = $Order->id;
                $orderlist->qty = $arti['qtyD' . $i];
                $orderlist->amount = $arti['TotalPD' . $i];
                $orderlist->unit_price = $arti['unitPD' . $i];
                $orderlist->article_id = $article_id_to;
                $orderlist->from_article = $article_from;
                $orderlist->article__resource_id = $article->id;
                $orderlist->save();
            }
        }
        if ($order['radBtn'] == '1') {
            $notify = new \App\Nofication;
            $notify->description = "Meat shop : Place a delivery order with total amount of Php " . number_format($order['Tamount'], 2) . " to  " . $order['deliverto'];
            $notify->user_id = \Auth::user()->id;
            $notify->date = $order['nowdate'];
            $notify->hr = $order['hr'];
            $notify->min = $order['min'];
            $notify->name = \Auth::user()->name;
            $notify->sec = $order['secs'];
            $notify->ref_id = $Order->id;
            $notify->type = 'order_dr';
            $notify->timestamp = $order['timestamp'];
            $notify->save();

        } else if ($order['radBtn'] == '0') {
            $notify = new \App\Nofication;
            $notify->description = "Meat shop : Place a walked-in order with total amount of Php " . number_format($order['Tamount'], 2) . " to  " . $order['sold'];
            $notify->user_id = \Auth::user()->id;
            $notify->date = $order['nowdate'];
            $notify->hr = $order['hr'];
            $notify->min = $order['min'];
            $notify->sec = $order['secs'];
            $notify->ref_id = $Order->id;
            $notify->name = \Auth::user()->name;
            $notify->type = 'order_or';
            $notify->timestamp = $order['timestamp'];
            $notify->save();
        }
        flash('Successfully Recorded!');
        return \Redirect::back();
    }

    public
    function AddPurchase()
    {
        $purchase = \Input::all();
        //dd($purchase);
        $buy = new \App\Outsource;
        $buy->date = $purchase['datePurchased'];
        $buy->ordered_from = $purchase['supplier'];
        $buy->dr = $purchase['dr#'];
        $buy->amount = $purchase['Tamnt'];
        $buy->balance = $purchase['Tamnt'];
        $buy->due_date = $purchase['dueDate'];
        $buy->status = 'pending';
        $buy->save();
        $outsource_id = $buy->id;
        //dd($outsource_id);

        //$count = count($purchase);
        $articles = array_slice($purchase, 10, -1);
        $count = count($articles);
        $k = ($count) / 4;
        //dd($articles);
        //change the change of the array
        $w = 0;
        $l = 0;
        $s = 3;
        foreach ($articles as $key => $item) {
            if (substr($key, 0, 8) == "articleV") {
                $arti['articleV' . $l] = $item;
            }
            if (substr($key, 0, 4) == "qtyV") {
                $arti['qtyV' . $l] = $item;
            }
            if (substr($key, 0, 6) == "unitPV") {
                $arti['unitPV' . $l] = $item;
            }
            if (substr($key, 0, 7) == "TotalPV") {
                $arti['TotalPV' . $l] = $item;
            }
            if ($w == $s) {
                $l++;
                $s = $s + 4;
            }
            $w++;
        }
        //dd($arti);
        for ($i = 0; $i < $k; $i++) {
            $addArticle = new \App\Article_Resource;
            $addArticle->outsource_id = $outsource_id;
            //$articleNew = $purchase['articleV'.$i];
            $article_id = $this->getArticleId($arti['articleV' . $i]);
            $addArticle->article_id = $article_id;
            $addArticle->quantity = $arti['qtyV' . $i];
            $addArticle->unit_price = $arti['unitPV' . $i];
            $addArticle->total_price = $arti['TotalPV' . $i];
            $addArticle->remain_qty = $arti['qtyV' . $i];
            //dd($article_id);
            $addArticle->save();
        }
        // dd($article_id);
        $notify = new \App\Nofication;
        $notify->description = "Meat shop : Add outsource purchase from " . $purchase['supplier'] . " with amount of Php " . number_format($purchase['Tamnt'], 2);
        $notify->user_id = \Auth::user()->id;
        $notify->date = $purchase['nowdate'];
        $notify->hr = $purchase['hr'];
        $notify->min = $purchase['min'];
        $notify->sec = $purchase['secs'];
        $notify->timestamp = $purchase['timestamp'];
        $notify->name = \Auth::user()->name;
        $notify->ref_id = $buy->id;
        $notify->type = 'outsource';
        $notify->save();

        flash('Successfully Added!');
        return \Redirect::back();
    }

    public
    function Add_outsourceOrder()
    {
        $purchase = \Input::all();
        //dd($purchase);
        $buy = new \App\Order;
        $buy->date = $purchase['date1'];
        $cust = \App\Customer::firstOrCreate(array('name' => $purchase['deliver'], 'customer_type' => '2'));
        $buy->customer_id = $cust->id;
        $buy->dr = $purchase['dr'];
        $buy->total_due = $purchase['Tamount'];
        $buy->due_date = $purchase['date2'];
        $buy->balance = $purchase['Tamount'];
        $buy->status = 'pending';
        $buy->type = 'delivery';
        $buy->save();
        $order_id = $buy->id;
        $articles = array_slice($purchase, 11, -1);
        //dd($articles);
        $count = count($articles);
        $k = ($count) / 4;

        /*modify the array */
        $w = 0;
        $l = 0;
        $s = 3;
        foreach ($articles as $key => $item) {
            if (substr($key, 0, 8) == "articleV") {
                $arti['articleV' . $l] = $item;

            }

            if (substr($key, 0, 4) == "qtyV") {
                $arti['qtyV' . $l] = $item;
            }
            if (substr($key, 0, 6) == "unitPV") {
                $arti['unitPV' . $l] = $item;
            }
            if (substr($key, 0, 7) == "TotalPV") {
                $arti['TotalPV' . $l] = $item;
            }
            if ($w == $s) {
                $l++;
                $s = $s + 4;
            }
            $w++;
        }
        //dd($arti);
        for ($i = 0; $i < $k; $i++) {
            $addArticle = new \App\Orderlist;
            $addArticle->order_id = $order_id;
            //$articleNew = $purchase['articleV'.$i];
            $article_id = $this->getArticleId($arti['articleV' . $i]);
            $addArticle->article_id = $article_id;
            $addArticle->qty = $arti['qtyV' . $i];
            $addArticle->unit_price = $arti['unitPV' . $i];
            $addArticle->amount = $arti['TotalPV' . $i];
            $qtyLeft_new = \App\Article_Resource::where('outsource_id', $purchase['id'])->where('article_id', $article_id)->get();
            //dd($qtyLeft_new);
            foreach ($qtyLeft_new as $count1) {
                $count1->remain_qty = $count1->remain_qty - $arti['qtyV' . $i];
                $addArticle->article__resource_id = $count1->id;
                $count1->sales_qty = $count1->sales_qty + $arti['qtyV' . $i];
                $count1->total_sales = $count1->total_sales + $arti['TotalPV' . $i];
                $count1->save();
            }
            $addArticle->from_article = $arti['articleV' . $i];
            $addArticle->save();
            //dd($qtyLeft_new->remain_qty);
        }
        // dd($article_id);
        $notify = new \App\Nofication;
        $notify->description = "Meat shop : Deliver from vendor to " . $purchase['deliver'] . " with amount of Php " . number_format($purchase['Tamount'], 2);
        $notify->user_id = \Auth::user()->id;
        $notify->date = $purchase['nowdate'];
        $notify->hr = $purchase['hr'];
        $notify->min = $purchase['min'];
        $notify->sec = $purchase['secs'];
        $notify->name = \Auth::user()->name;
        $notify->ref_id = $buy->id;
        $notify->type = 'order_dr';
        $notify->timestamp = $purchase['timestamp'];
        $notify->save();
        flash('Successfully Added!');
        return \Redirect::back();
    }

    public
    function GetMonth($mon)
    {
        //$month;
        if ($mon == "01") {
            $month = "January";
        } elseif ($mon == "02") {
            $month = "Febuary";
        } elseif ($mon == "03") {
            $month = "March";
        } elseif ($mon == "04") {
            $month = "April";
        } elseif ($mon == "05") {
            $month = "May";
        } elseif ($mon == "06") {
            $month = "June";
        } elseif ($mon == "07") {
            $month = "July";
        } elseif ($mon == "08") {
            $month = "August";
        } elseif ($mon == "09") {
            $month = "September";
        } elseif ($mon == "10") {
            $month = "October";
        } elseif ($mon == "11") {
            $month = "November";
        } elseif ($mon == "12") {
            $month = "December";
        } else {
            $month = null;
        }
        return $month;

    }

    public
    function getArticleId($name)
    {
        $id = \App\Article::where('name', $name)->first();
        return $id['id'];
        //return $id;
    }

}
