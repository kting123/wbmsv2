<?php


namespace App\Http\Controllers;

use Illuminate\Http\Request;
Use Validator;

class ReceiptController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------

      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function SaveOr($id)
    {
        $data = \Input::all();
        //dd($data);
        $order = \App\Order::find($id);
        $order->check = $data['checkno'];
        $order->bank = $data['bank'];
        $order->or = $data['or'];
        $order->date_of_check = $data['dateofcheck'];
        flash('Successfully Edited!');
        $order->save();
        return \Redirect::back();
    }

    public function ViewOR()
    {
        $pagination = \App\Order::where('status', '=', 'paid')->orderBy('or', 'desc')->paginate(8);
        $paginations = \App\Payment_History::where('balance', '>', '0.0')->where('order_id', '!=', '0')->orderBy('or', 'desc')->get();
        $pagination->setPath('http://wbms.x10.bz/or');
        return view('Admin.customer_or.admin_OR')
            ->with('paginations', $paginations)
            ->with('order', $pagination);
    }

    public function ViewOrBreakdown($id)
    {
        $orderslist = \App\Orderlist::where('order_id', '=', $id)->get();
        //    dd($articles);
        $history = \App\Payment_History::where('order_id', '=', $id)->get();
        $cow = \App\Cow::where('tsk', '0')->orderBy('id', 'desc')->take(7)->get();
        $tabo_tabo = \App\TaboTabo::where('tsk', '0')->orderBy('id', 'desc')->take(7)->get();
        $outsource = \App\Outsource::where('tsk', '0')->orderBy('id', 'desc')->take(7)->get();
        $artcles = \App\Article::where('tsk', '0')->orderBy('name')->get();

        return view('Admin.customer_or.admin_or_breakdown')
            ->with('history', $history)
            ->with('data', $orderslist)
            ->with('id', $id)
            ->with('cow', $cow)
            ->with('tabo_tabos', $tabo_tabo)
            ->with('outsources', $outsource)
            ->with('articles', $artcles);
    }

    public function ViewDrBreakdown($id)
    {
        $orderslist = \App\Orderlist::where('order_id', '=', $id)->get();
        $history = \App\Payment_History::where('order_id', '=', $id)->get();
        $cow = \App\Cow::where('tsk', '0')->orderBy('id', 'desc')->take(7)->get();
        $tabo_tabo = \App\TaboTabo::where('tsk', '0')->orderBy('id', 'desc')->take(7)->get();
        $outsource = \App\Outsource::where('tsk', '0')->orderBy('id', 'desc')->take(7)->get();
        $artcles = \App\Article::where('tsk', '0')->orderBy('name')->get();
        return view('Admin.customer_dr.admin_dr_breakdown')
            ->with('history', $history)
            ->with('data', $orderslist)
            ->with('cow', $cow)
            ->with('tabo_tabos', $tabo_tabo)
            ->with('outsources', $outsource)
            ->with('articles', $artcles);
    }

    public function PostPayment($id)
    {
        $data = \Input::all();
        //   dd($data);
        $order = \App\Order::find($id);
        $history = new \App\Payment_History;
        //$order->status = "paid";
        //$order->bank = $data["bank"];
        $order->or = $data['or'];
        $history->or = $data['or'];
        $order->amount_paid = $data['amount'];
        $history->amount_paid = $data['amount'];
        $order->date_of_check = $data['dateofcheck'];
        $history->date_of_check = $data['dateofcheck'];
        $count = count($data);
        $history->order_id = $order->id;
        if ($count > 9) {
            $order->bank = $data['bank'];
            $history->bank = $data['bank'];
            $order->check = $data['checkno'];
            $history->check = $data['checkno'];
        } else {

        }
        //dd($post_payment->id);
        $order->balance = $order->balance - $order->amount_paid;
        $history->balance = $order->balance;
        //dd($order->balance);
        if ($order->balance > 0.0) {
            $order->status = 'partial';
        } elseif ($order->balance == 0.0) {
            $order->status = 'paid';
        }
        $notify = new \App\Nofication;
        $notify->description = "Meat shop : Posted a payment with amount of Php " . number_format($data['amount'], 2) . " to " . $order->customer->name . " order.";
        $notify->user_id = \Auth::user()->id;
        $notify->date = $data['nowdate'];
        $notify->hr = $data['hr'];
        $notify->min = $data['min'];
        $notify->sec = $data['secs'];
        $notify->ref_id = $order->id;
        $notify->name = \Auth::user()->name;
        $notify->type = 'order_or';
        $notify->timestamp = $data['timestamp'];
        $notify->save();
        //dd($order->status);
        $order->save();
        $history->save();

        flash('Payment Posted!');
        return \Redirect::to('/dr_order' . $id);
    }

    public function SaveDr($id)
    {
        $data = \Input::all();
        $order = \App\Order::find($id);
        if ($data['status'] == 'cancelled') {
            foreach ($order['orderlists'] as $list) {
                $resource = \App\Article_Resource::where('id', $list['article__resource_id'])->get();
                //dd($resource);
                foreach ($resource as $item) {
                    $item->sales_qty = $item->sales_qty - $list['qty'];
                    $item->remain_qty = $item->remain_qty + $list['qty'];
                    $item->total_sales = $item->total_sales - $list['amount'];
                    $item->save();
                }
            }
            $order->due_date = $data['due_date'];
            $order->dr = $data['dr'];
            $order->date = $data['date'];
            $order->status = $data['status'];
            $order->save();
            \Flash::warning('You just cancelled out an order.');
            return \Redirect::back();
        } elseif ($order['status'] == 'cancelled') {
            foreach ($order['orderlists'] as $list) {
                $resource = \App\Article_Resource::where('id', $list['article__resource_id'])->get();
                //dd($resource);
                foreach ($resource as $item) {
                    $item->sales_qty = $item->sales_qty + $list['qty'];
                    $item->remain_qty = $item->remain_qty - $list['qty'];
                    $item->total_sales = $item->total_sales + $list['amount'];
                    $item->save();
                }
                //dd('save!');
            }
            $order->due_date = $data['due_date'];
            $order->dr = $data['dr'];
            $order->date = $data['date'];
            $order->status = $data['status'];
            $order->save();
            \Flash::success('The order status was changed to' . $data['status']);
            return \Redirect::back();
        } else {
            $order->due_date = $data['due_date'];
            $order->dr = $data['dr'];
            $order->date = $data['date'];
            $order->status = $data['status'];
            $order->save();
            flash('Successfully Edited!');
            return \Redirect::back();
        }

    }

    public function deleteOrder($id)
    {
        $order = \App\Order::find($id);
        foreach ($order['orderlists'] as $list) {
            $resource = \App\Article_Resource::where('id', $list['article__resource_id'])->get();
            $resource1 = json_decode(json_encode($resource), true);
            // dd($resource1);
            if ($resource1 == []) {

            } else {
                if (
                    $resource[0]['tabo_tabo_id'] != null && $resource[0]['article_id'] == '1'
                ) {
                    $tabo = \App\TaboTabo::find($resource[0]['tabo_tabo_id']);
                    $tabo->status = '0';
                    $tabo->save();
                }
                foreach ($resource as $item) {
                    $item->sales_qty = $item->sales_qty - $list['qty'];
                    $item->remain_qty = $item->remain_qty + $list['qty'];
                    $item->total_sales = $item->total_sales - $list['amount'];
                    $item->save();
                }
            }
            $list->delete();
        }
        foreach ($order['payment_history'] as $history) {
            $history->delete();
        }
        $order->delete();
        \Flash::warning('Ops, All Records related to this item were deleted and updated.');
        return \Redirect::back();
    }


    public function editOrder($id)
    {
        $in = \Input::all();
        //dd($in);
        $articles = array_slice($in, 6);
      //  dd(json_decode(json_encode($articles),true));
        if($articles==[]){
            \Flash::warning('Input is empty, or Are you trying to delete this order? you  can do this in the official receipts.');
            return \Redirect::back();
        }
        $count = count($articles);
        $k = ($count) / 7;
        $w = 0;
        $l = 0;
        $s = 6;
        foreach ($articles as $key => $item) {
            if (substr($key, 0, 2) == "id") {
                $arti['id' . $l] = $item;
            }
            if (substr($key, 0, 4) == "type") {
                $arti['type' . $l] = $item;
            }
            if (substr($key, 0, 8) == "articleD") {
                $arti['articleD' . $l] = $item;
            }
            if (substr($key, 0, 4) == "qtyD") {
                $arti['qtyD' . $l] = $item;
            }
            if (substr($key, 0, 6) == "unitPD") {
                $arti['unitPD' . $l] = $item;
            }
            if (substr($key, 0, 7) == "TotalPD") {
                $arti['TotalPD' . $l] = $item;
            }
            if (substr($key, 0, 11) == "articlesFrD") {
                $arti['articlesFrD' . $l] = $item;
            }
            if ($w == $s) {
                $l++;
                $s = $s + 7;
            }
            $w++;
        }
        $order = \App\Order::find($id);
        foreach ($order['orderlists'] as $list) {
            $resource = \App\Article_Resource::where('id', $list['article__resource_id'])->get();
            $resource1 = json_decode(json_encode($resource), true);
            // dd($resource1);
            if ($resource1 == []) {

            } else {
                foreach ($resource as $item) {
                    $item->sales_qty = $item->sales_qty - $list['qty'];
                    $item->remain_qty = $item->remain_qty + $list['qty'];
                    $item->total_sales = $item->total_sales - $list['amount'];
                    $item->save();
                }
                $list->delete();
            }
        }
        $total = 0;
        for ($i = 0; $i < $k; $i++) {
            $article_id_to = $this->getArticleId($arti['articleD' . $i]);
            $article_id_from = $this->getArticleId($arti['articlesFrD' . $i]);
            $from = explode('.', $arti['articlesFrD' . $i]);
            //  dd($from);
            if ($arti['type' . $i] == 'Farm') {
                if ($article_id_to == $article_id_from) {
                    $article_from = $arti['articleD' . $i];
                    $cow_article = \App\Article_Resource::where('cow_id', $arti['id' . $i])->where('article_id', $article_id_to)->get();
                } else {
                    $resource_id = $from['0'];
                    $article_from = $from['1'];
                    $cow_article = \App\Article_Resource::where('cow_id', $arti['id' . $i])->where('id', $resource_id)->get();
                }
            } elseif ($arti['type' . $i] == 'TaboTabo') {
                if ($article_id_to == $article_id_from) {
                    $article_from = $arti['articleD' . $i];
                    $cow_article = \App\Article_Resource::where('tabo_tabo_id', $arti['id' . $i])->where('article_id', $article_id_to)->get();
                } else {
                    $resource_id = $from['0'];
                    $article_from = $from['1'];
                    $cow_article = \App\Article_Resource::where('tabo_tabo_id', $arti['id' . $i])->where('id', $resource_id)->get();
                }
            } elseif ($arti['type' . $i] == 'Vendors') {
                if ($article_id_to == $article_id_from) {
                    $article_from = $arti['articleD' . $i];
                    $cow_article = \App\Article_Resource::where('outsource_id', $arti['id' . $i])->where('article_id', $article_id_to)->get();
                } else {
                    $resource_id = $from['0'];
                    $article_from = $from['1'];
                    $cow_article = \App\Article_Resource::where('outsource_id', $arti['id' . $i])->where('id', $resource_id)->get();
                }
            }
            $check1 = json_decode(json_encode($cow_article), true);
            //  dd($check1);
            if ($check1 == null) {
                \Flash::warning('No articles found for the specific Body Number, Please add it first.Thanks!');
                return \Redirect::back();
            } else {
                foreach ($cow_article as $article) {
                    //  dd($article->sales_qty);
                    $article->sales_qty = $article->sales_qty + $arti['qtyD' . $i];
                    $article->remain_qty = $article->remain_qty - $arti['qtyD' . $i];
                    $article->total_sales = $article->total_sales + $arti['TotalPD' . $i];
                    $article->save();
                }
                $orderlist = new \App\Orderlist;
                $orderlist->order_id = $id;
                $orderlist->qty = $arti['qtyD' . $i];
                $orderlist->amount = $arti['TotalPD' . $i];
                $orderlist->unit_price = $arti['unitPD' . $i];
                $orderlist->article_id = $article_id_to;
                $orderlist->from_article = $article_from;
                $orderlist->article__resource_id = $article->id;
                $orderlist->save();
                $total = $total +  $arti['TotalPD' . $i];
            }
        }
        $notify = new \App\Nofication;
        $notify->description = "Meat shop : Edited an order of " .$order['customer']['name']." with an updated total amount of Php " . number_format($total, 2);
        $notify->user_id = \Auth::user()->id;
        $notify->date = $in['nowdate'];
        $notify->hr = $in['hr'];
        $notify->min = $in['min'];
        $notify->name = \Auth::user()->name;
        $notify->sec = $in['secs'];
        $notify->ref_id = $id;
        $notify->type = 'order_dr';
        $notify->timestamp = $in['timestamp'];
        $notify->save();
        $order->total_due = $total;
        $order->save();
        flash('successfully updated.');
        return \Redirect::back();
    }


    public function deleteHistory($id)
    {
        $history = \App\Payment_History::find($id);
        $check = \App\Order::find($history['order_id']);
        if ($check == []) {
        } else {
            $history->order->balance = $history->order->balance + $history->amount_paid;
            $history->order->amount_paid = $history->order->amount_paid - $history->amount_paid;
            $history->order->save();
        }
        $history->delete();
        \Flash::warning('Ops, All Records related to this item were deleted and updated.');
        return \Redirect::back();
    }


    public function ViewDR()
    {
        $now = (int)strtotime(date("m/d/y", time()));
        //  dd(time());
        $pagination = \App\Order::where('type', '=', 'delivery')->orderBy('id', 'desc')->paginate(8);
        //  dd($orders);
        $pagination->setPath('http://wbms.x10.bz/dr');
        return view('Admin.customer_dr.admin_DR')
            ->with('order', $pagination)
            ->with('now', $now);
    }

    public function Post_vendorPayment($id)
    {
        $pay = \Input::all();
        //dd($id);
        $post_payment = \App\Outsource::find($id);
        $history = new \App\Payment_History;
        //dd($post_payment->id);
        $post_payment->amount_paid = $pay['amount'];
        $history->amount_paid = $pay['amount'];
        $post_payment->or = $pay['or'];
        $history->or = $pay['or'];
        $post_payment->date_of_check = $pay['dateofpayment'];
        $history->date_of_check = $pay['dateofpayment'];
        $history->outsource_i = $post_payment->id;
        $count = count($pay);
        if ($count > 9) {
            $post_payment->Bank = $pay['bank'];
            $history->bank = $pay['bank'];
            $post_payment->Check = $pay['checkno'];
            $history->check = $pay['checkno'];
        } else {

        }
        //dd($post_payment->id);
        $post_payment->balance = $post_payment->balance - $post_payment->amount_paid;
        $history->balance = $post_payment->balance;
        //dd($post_payment->balance);
        if ($post_payment->balance > 0) {
            $post_payment->status = 'partial';
        } elseif ($post_payment->balance == 0) {
            $post_payment->status = 'paid';
        }

        $post_payment->save();
        $history->save();
        flash('Payment Posted!');
        return \Redirect::back();
    }

    public function getArticleId($name)
    {
        $id = \App\Article::where('name', $name)->first();
        return $id['id'];
        //return $id;
    }

    public function validator(array $data)
    {
        return Validator::make($data, [
            'id0' => 'required',
            'type0' => 'required',
            'unitPD0' => 'required',
            'TotalPD0' => 'required',
            'articleD0' => 'required',
            'articlesFrD0' => 'required',
        ]);
    }

}
