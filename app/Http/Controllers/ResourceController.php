<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ResourceController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------

      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function ViewResourcesTabo()
    {
        $pagination = \App\TaboTabo::where('tsk', '=', '0')->orderBy('id', 'desc')->paginate(8);
        $pagination->setPath('http://wbms.x10.bz/view_tabo_tabo');
        return view('Admin.resources.tabotabo')
            ->with('tabotabos', $pagination);
    }

    public function ViewFromOutsource()
    {
        $pagination = \App\Outsource::where('tsk', '=', '0')->orderBy('id', 'desc')->paginate(8);
        $pagination->setPath('http://wbms.x10.bz/outsource');
        return view('Admin.resources.outsource')
            ->with('outsource', $pagination);
    }

    public function ViewFromOwnFarm()
    {
        $pagination = \App\Cow::where('tsk', '=', '0')->orderBy('id', 'desc')->paginate(8);
        $pagination->setPath('http://wbms.x10.bz/own');
        return view('Admin.resources.own')
            ->with('own', $pagination);
    }

//    new--- Cindy
    public function ViewParts($id)
    {
        $cow = \App\Cow::find($id);
        $resource = \App\Article_Resource::where('cow_id', $id)->orderBy('id', 'desc')->get();
        $resource_choice_cuts = \App\Article_Resource::where('cow_id', $id)->where('type', 'Choice Cuts')->orderBy('id', 'desc')->paginate(5);
        $resource_choice_cuts->setPath('http://wbms.x10.bz/viewParts' . $id);
        $resource_special_cuts = \App\Article_Resource::where('cow_id', $id)->where('type', 'Special Cuts')->orderBy('id', 'desc')->paginate(5);
        $resource_special_cuts->setPath('http://wbms.x10.bz/viewParts' . $id);
        $resource_other_cuts = \App\Article_Resource::where('cow_id', $id)->where('type', 'Other Cuts')->orderBy('id', 'desc')->paginate(5);
        $resource_other_cuts->setPath('http://wbms.x10.bz/viewParts' . $id);
        $articles = \App\Article::all();
        $mainparts = \App\MainParts::where('cow_id', $id)->orderBy('id', 'desc')->paginate(5);
        $mainparts->setPath('http://wbms.x10.bz/viewParts' . $id);


        return view('subuser.cow_parts_cuts')->with('lists', $resource)
            ->with('choice_cuts', $resource_choice_cuts)
            ->with('special_cuts', $resource_special_cuts)
            ->with('other_cuts', $resource_other_cuts)
            ->with('articles', $articles)
            ->with('cow', $cow)
            ->with('mainparts', $mainparts);

    }

    public function DeleteParts($id)
    {
        $in = \Input::all();
//        dd($id);
        $mainparts = \App\MainParts::where('article_id', $id)
            ->where('cow_id', $in['cow_id'])
            ->first();
//        dd($mainparts);
        $mainparts->delete();
        \Flash::success('Successfully deleted!');
        return \Redirect::back();
    }

    public function DeleteCuts($id)
    {
        $in = \Input::all();
//        dd($id);
        $article = \App\Article_Resource::where('article_id', $id)
            ->where('cow_id', $in['cow_id'])
            ->first();
//        dd($article);
        $article->delete();
        \Flash::success('Successfully deleted!');
        return \Redirect::back();
    }

    public function EditPart($id)
    {
        $in = \Input::all();
//        dd($in);
        $new_article_id = $this->getArticleId($in['part']);

        if ($id == $new_article_id && $in['old_qty'] == $in['qty']) {
//            dd($in);
            flash('You have no changes.');
            return \Redirect::back();
        } else {
            if ($id == $new_article_id) {
                $mainparts = \App\MainParts::where('article_id', $id)
                    ->where('cow_id', $in['cow_id'])
                    ->first();
                $mainparts->quantity = $in['qty'];
                $mainparts->save();
                \Flash::success('Successfully Updated!.');
                return \Redirect::back();
            } else {
                //check if new part already exist
                $check = \App\MainParts::where('cow_id', $in['cow_id'])
                    ->where('article_id', $new_article_id)->get();
                $var = json_decode(json_encode($check), TRUE);
                if ($var == NULL) {
                    //  dd($var);
                    $mainparts = \App\MainParts::where('article_id', $id)
                        ->where('cow_id', $in['cow_id'])
                        ->first();
                    $mainparts->article = $in['part'];
                    $mainparts->article_id = $new_article_id;
                    $mainparts->quantity = $in['qty'];
//            dd($mainparts);
                    $mainparts->save();

                    \Flash::success('Successfully Updated!.');
                    return \Redirect::back();
                } else {
                    \Flash::warning('Selected article already exist.');
                    return \Redirect::back();
                }
            }
        }
    }

    public function EditCut($id)
    {
        $in = \Input::all();
//        dd($in);
        $new_article_id = $this->getArticleId($in['article']);
//dd($new_article_id);
        if ($id == $new_article_id && $in['old_qty'] == $in['qty'] && $in['old_type'] == $in['type']) {
//            dd($in);
            flash('You have no changes.');
            return \Redirect::back();
        } else {
            if ($id == $new_article_id) {
                $article_resource = \App\Article_Resource::where('article_id', $id)
                    ->where('cow_id', $in['cow_id'])
                    ->first();
                $article_resource->quantity = $in['qty'];
                $article_resource->type = $in['type'];
                $article_resource->remain_qty = $in['qty'];
                $article_resource->save();
                flash('Successfully Updated!.');
                return \Redirect::back();
            } else {
                //check if new part already exist
                $check = \App\Article_Resource::where('cow_id', $in['cow_id'])
                    ->where('article_id', $new_article_id)->get();
                $var = json_decode(json_encode($check), TRUE);
                if ($var == NULL) {
                    //  dd($var);
                    $article_resource = \App\Article_Resource::where('article_id', $id)
                        ->where('cow_id', $in['cow_id'])
                        ->first();
                    $article_resource->article_id = $new_article_id;
                    $article_resource->quantity = $in['qty'];
                    $article_resource->remain_qty = $in['qty'];
                    $article_resource->type = $in['type'];
//            dd($article_resource);
                    $article_resource->save();

                    flash('Successfully Updated!.');
                    return \Redirect::back();
                } else {
                    flash('Selected article already exist.');
                    return \Redirect::back();
                }
            }
        }
    }

    public function AddTaboArticles($id)
    {
        $in = \Input::all();
//        dd($in);
        $count = count($in);
//        dd($count);
        $k = ($count - 2) / 2;
//        dd($k);
        for ($j = 0; $j < $k; $j++) {
//            dd($i);
            if ($in['article' . $id . $j] == '' || $in['qty' . $id . $j] == '') {
//                dd($i);
            } else {
                $article_id = $this->getArticleId($in['article' . $in['tabo_id'] . $j]);
                $check = \App\Article_Resource::where('tabo_tabo_id', $id)
                    ->where('article_id', $article_id)
                    ->get();
                $result = json_decode(json_encode($check), TRUE);
                if ($result == NULL) {
                    $article_id = $this->getArticleId($in['article' . $id . $j]);
                    $resource = new \App\Article_Resource;
                    $resource->tabo_tabo_id = $in['tabo_id'];
                    $resource->article_id = $article_id;
                    $resource->quantity = $in['qty' . $id . $j];
                    $resource->remain_qty = $in['qty' . $id . $j];
                    $resource->save();
                } else {
                    $resource = \App\Article_Resource::find($result[0]['id']);
                    $resource->remain_qty = $in['qty' . $id . $j];
                    $resource->quantity = $in['qty' . $id . $j];
                    $resource->save();
                }
            }
        }
        \Flash::success('Successfully Added!');
        return \Redirect::back();
    }

    public function ViewTaboArticles($id)
    {
//        dd($id);
        $tabo_tabo = \App\TaboTabo::find($id);
//        dd($tabo_tabo);
        $resource = \App\Article_Resource::where('tabo_tabo_id', $id)
            ->orderBy('id', 'desc')->paginate(10);
        $resource->setPath('http://wbms.x10.bz/viewTaboArticles' . $id);
//        dd($resource);
        $articles = \App\Article::all();
        return view('subuser.Tabotabo_articles')->with('resource', $resource)
            ->with('tabo_tabo', $tabo_tabo)
            ->with('articles', $articles);
    }

    public function DeleteTaboArticle($id)
    {
        $in = \Input::all();
//        dd($in);
        $resource = \App\Article_Resource::where('article_id', $id)
            ->where('tabo_tabo_id', $in['tabo_tabo_id'])
            ->first();
//        dd($mainparts);
        $resource->delete();
        \Flash::success('Successfully deleted!');
        return \Redirect::back();
    }

    public function EditTaboArticle($id)
    {
        $in = \Input::all();
//        dd($in);
        $new_article_id = $this->getArticleId($in['article']);
//        dd($new_article_id);

        if ($id == $new_article_id && $in['old_qty'] == $in['qty']) {
//          dd($new_article_id);
            flash('You have no changes.');
            return \Redirect::back();
        } else {
            if ($id == $new_article_id) {
                $article_resource = \App\Article_Resource::where('article_id', $id)
                    ->where('tabo_tabo_id', $in['tabo_tabo_id'])
                    ->first();
                $article_resource->quantity = $in['qty'];
                $article_resource->remain_qty = $in['qty'];
                $article_resource->save();
//                dd($article_resource);
                \Flash::success('Successfully Updated!.');
                return \Redirect::back();
            } else {
                //check if new article already exist
                $check = \App\Article_Resource::where('tabo_tabo_id', $in['tabo_tabo_id'])
                    ->where('article_id', $new_article_id)->get();
                $var = json_decode(json_encode($check), TRUE);
                if ($var == NULL) {
                    //  dd($var);
                    $article_resource = \App\Article_Resource::where('article_id', $id)
                        ->where('tabo_tabo_id', $in['tabo_tabo_id'])
                        ->first();
                    $article_resource->article_id = $new_article_id;
                    $article_resource->quantity = $in['qty'];
                    $article_resource->remain_qty = $in['qty'];
                    $article_resource->save();

                    \Flash::success('Successfully Updated!.');
                    return \Redirect::back();
                } else {
                    \Flash::warning('Selected article already exist.');
                    return \Redirect::back();
                }
            }
        }
    }

    public function SaveBaka($id)
    {
        $input = \Input::all();
        //  dd($input);
        // dd($input['month']);
        $cow = \App\Cow::find($id);
        $cow->id = $input['id'];
        $cow->sex = $input['sex'];
        $cow->color = $input['color'];
        $cow->date = $input['date_added'];
        flash('Successfully edited!');
        $cow->save();
        return \Redirect::back();
    }
    public function DeleteOutsource($id)
    {
        $order = \App\Outsource::find($id);
        foreach ($order->article_resources as $article_recsource) {
                  foreach ($article_recsource['orderlist'] as $list){
                      $list->order->delete();
                      $list->delete();
                  }
                  $article_recsource->delete();
                }
        $order->delete();
        flash('Successfully deleted!');
        return \Redirect::back();
    }
    public function EditOutsource($id)
    {
        $data = \Input::all();
        //  dd($data);
        $order = \App\Outsource::find($id);
        $order->date = $data['date'];
        $order->dr = $data['dr'];
        $order->or = $data['or'];
        $order->ordered_from = $data['ordered_from'];
        $order->amount = $data['amount'];
        $order->status = $data['status'];
        $order->due_date = $data['due_date'];
        $order->save();
        flash('Successfully edited!');
        return \Redirect::back();
    }

    public function DeleteTabo($id)
    {
        $tabo = \App\TaboTabo::find($id);
        // dd($order);
        foreach ($tabo->article_resources as $article_recsource) {
                  foreach ($article_recsource['orderlist'] as $list){
                    if($list==null){
                    }
                    else{
                      $list->order->delete();
                      $list->delete();
                    }
                  }
            $article_recsource->delete();
        }
        foreach ($tabo->expenses as $expense) {
            $expense->delete();
        }
        $tabo->delete();
        \Flash::warning('Ops, Just deleted one item. All records related to this item also deleted.');
        return \Redirect::back();
    }

    public function EditTabo($id)
    {
        $input = \Input::all();
        // dd($input);
        $tabo = \App\TaboTabo::find($id);
        $tabo->date = $input['dateofpurchased'];
        $tabo->sex = $input['gender'];
        $tabo->lw = $input['lw'];
        $tabo->sw = $input['sw'];
        $tabo->mw = $input['mw'];
        $tabo->id = $input['bodyNum'];
        $tabo->price = $input['perKilo'] * $input['lw'];
        $tabo->price_per_kilo = $input['perKilo'];
        $tabo->owner = $input['owner'];
        $tabo->area = $input['area'];
        $tabo->color = $input['color'];
        $tabo->save();
        flash('Item successfully updated!');
        return \Redirect::back();
    }

    public function DeleteOwn($id)
    {
        $cows = \App\Cow::find($id);
        foreach ($cows->article_resources as $article_recsource) {
                  foreach ($article_recsource['orderlist'] as $list){
                      $list->order->delete();
                      $list->delete();
                  }
            $article_recsource->delete();
        }
        $cows->delete();
        \Flash::warning('Ops,record deleted');
        return \Redirect::back();
    }

    public function EditOwn($id)
    {
        $input = \Input::all();
        //  dd($input);
        $cow = \App\Cow::find($id);
        $cow->id = $input['bodyNum'];
        $cow->sex = $input['gender'];
        $cow->color = $input['color'];
        $cow->lw = $input['lw'];
        $cow->sw = $input['sw'];
        $cow->date = $input['dateofpurchased'];
        flash('Item successfully updated!');
        $cow->save();
        return \Redirect::back();
    }

    public function AddTabo()
    {
        //Tabo - Tabo
        $input = \Input::all();
        //dd($input);
        $tabo = new \App\TaboTabo;
        $tabo->date = $input['dateofpurchased'];
        $tabo->sex = $input['gender'];
        $tabo->lw = $input['lw'];
        $tabo->id = $input['bodyNumber'];
        $tabo->sw = $input['sw'];
        $tabo->mw = $input['mw'];
        $tabo->price = $input['perKilo'] * $input['lw'];
        $tabo->price_per_kilo = $input['perKilo'];
        $tabo->owner = $input['owner'];
        $tabo->area = $input['area'];
        $tabo->color = $input['color'];
        $tabo->status = '0';
        $tabo->save();
        /*for notifications */
        $notify = new \App\Nofication;
        $notify->description = "Added new item in tabotabo with price of Php" .number_format($tabo->price,2)." from ".$tabo->owner;
        $notify->user_id = \Auth::user()->id;
        $notify->date = $input['nowdate'];
        $notify->hr = $input['hr'];
        $notify->min = $input['min'];
        $notify->sec = $input['secs'];
        $notify->name = \Auth::user()->name;
        $notify->ref_id = $tabo->id;
        $notify->type = 'tabotabo';
        $notify->timestamp = $input['timestamp'];
        $notify->save();
        $resource = new \App\Article_Resource;
        $resource->tabo_tabo_id = $input['bodyNumber'];
        $resource->quantity = $input['mw'];
        $resource->article_id = '1';
        $resource->remain_qty = $input['mw'];
        $resource->save();
        flash('Successfully Added');
        return \Redirect::back();
    }

    public function AddDeliveryTabo()
    {
        $in = \Input::all();
        //  dd($in[]);
        $customer = \App\Customer::firstOrCreate(array('name' => $in['customer2'], 'customer_type' => '1'));
        // dd($customer);
        $amount = $in['qty1'] * $in['amount1'];
        $les = $amount * (0.01);
        $due = $amount - $les;
        $resource = \App\Article_Resource::updateOrCreate(array('quantity' => $in['qty1'], 'tabo_tabo_id' => $in['id'], 'article_id' => 1), array('sales_qty' => $in['qty1'], 'remain_qty' => 0, 'total_sales' => $due,
            'unit_price' => $in['amount1']));
        $tabo = \App\TaboTabo::find($in['id']);
        $tabo->status = '1';
        $tabo->save();
        $order = new \App\Order;
        $order->date = $in['date1'];
        $order->due_date = $in['duedate1'];
        $order->dr = $in['dr1'];
        $order->customer_id = $customer->id;
        $order->type = 'delivery';
        $order->status = "pending";
        $order->total_due = $due;
        $order->balance = $due;
        $order->voucher_no = $in['voucher'];
        $order->save();
        $notify = new \App\Nofication;
        $notify->description = "Place a delivery order to ".$customer->name." from tabotabo with amount of Php ".number_format($due,2);
        $notify->user_id = \Auth::user()->id;
        $notify->date = $in['nowdate'];
        $notify->hr = $in['hr'];
        $notify->min = $in['min'];
        $notify->sec = $in['secs'];
        $notify->name = \Auth::user()->name;
        $notify->ref_id = $order->id;
        $notify->type = 'order_dr';
        $notify->timestamp = $in['timestamp'];
        $notify->save();
        $orderlist = new \App\Orderlist;
        $orderlist->qty = $in['qty1'];
        $orderlist->order_id = $order->id;
        $orderlist->article__resource_id = $resource->id;
        $orderlist->article_id = 1;
        $orderlist->unit_price = $in['amount1'];
        $orderlist->amount = $due;
        $orderlist->save();
        flash('Successfully Added And Updated the articles quantities.');
        return \Redirect::back();
    }


    public function CowInfo($id)
    {
        $cow = \App\Cow::find($id);
        return view('Admin.resources.own_breakdown')
            ->with('cow', $cow);
    }

    public function ViewTaboBr($id)
    {
        $tabo = \App\TaboTabo::find($id);
        $articles = \App\Article::all();

        $cust = \App\Customer::where('customer_type', '1')->orderBy('id', 'desc')->get();
        return view('Admin.resources.tabotabo_breakdown')
            ->with('tabos', $tabo)
            ->with('articles', $articles)
            ->with('cust', $cust);
    }

    public function AddExpense()
    {
        $in = \Input::all();
        $count = count($in);
        $k = ($count - 2) / 2;
        // dd($k);
        for ($i = 0; $i < $k; $i++) {
            if ($in['name' . $i] == '' || $in['amount' . $i] == '') {

            } else {
                $expense = new \App\Expense;
                $expense->name = $in['name' . $i];
                $expense->amount = $in['amount' . $i];
                $expense->tabo_tabo_id = $in['id'];
                $expense->save();
            }
        }
        return \Redirect::back();
    }

    public function AddArticle()
    {
        $in = \Input::all();
        //   dd($in);
        $count = count($in);
        $k = ($count - 2) / 5;
        // dd($k);
        for ($i = 0; $i < $k; $i++) {
            if ($in['article' . $i] == '' || $in['qty' . $i] == '') {
                \Flash::warning('Ops, Seems that you forgot something.');
                return \Redirect::back();
            } else {
                $resource = new \App\Article_Resource;
                $article = explode('_', $in['article' . $i]);
                $art_id = $this->getArticleId($article['0']);
                $resource->article_id = $art_id;
                $resource->quantity = $in['qty' . $i];
                $resource->tabo_tabo_id = $in['id'];
                if ($in['status' . $i] == 'Sold') {
                    $resource->sales_qty = $in['qty' . $i];
                    $resource->total_sales = $in['total_due' . $i];
                    $resource->unit_price = $in['unit_price' . $i];
                } elseif ($in['status' . $i] == 'Available') {
                    $resource->remain_qty = $in['qty' . $i];
                    $resource->sales_qty = 0;
                    $resource->total_sales = 0;
                }
                $resource->save();
            }
        }
        flash('Successfully Added.');
        return \Redirect::back();
    }

    public function AddBaka()
    {
        $in = \Input::all();
        // dd($in);
        $cow = new \App\Cow;
        $cow->date = $in['date'];
        $date = explode("/", $in['date']);
        // dd($date);
        $mon = $this->GetMonth($date[0]);
        $cow->year = $date[2];
        $cow->month = $mon;
        $cow->color = $in['color'];
        $cow->sex = $in['gender'];
        $cow->lw = $in['lw'];
        $cow->sw = $in['sw'];
        $cow->cw = $in['cw'];
        $cow->save();
        /* for addidng baka for notifications */
        $notify = new \App\Nofication;
        $notify->description = "Meat shop : Added a new slaugthered cow for Meat shop";
        $notify->user_id = \Auth::user()->id;
        $notify->date = $in['nowdate'];
        $notify->hr = $in['hr'];
        $notify->min = $in['min'];
        $notify->sec = $in['secs'];
        $notify->name = \Auth::user()->name;
        $notify->ref_id = $cow->id;
        $notify->type = 'farm';
        $notify->timestamp = $in['timestamp'];
        $notify->save();


        flash('Successfully Added!');
        return \Redirect::back();
    }

    public function AddParts($id)
    {
        $in = \Input::all();
//         dd($in);
        $count = count($in);
        // dd($count);
        $k = ($count - 2) / 2;
        // dd($k);
        for ($i = 0; $i < $k; $i++) {
            // dd($i);
            if ($in['part' . $in['cow_id'] . $i] == '' || $in['qty' . $in['cow_id'] . $i] == '') {

            } else {

                $cow = \App\Cow::find($id);
                $article_id = $this->getArticleId($in['part' . $in['cow_id'] . $i]);
//                dd($article_id);
                $check = \App\MainParts::where('cow_id', $id)
                    ->where('article_id', $article_id)->get();
                $var = json_decode(json_encode($check), TRUE);
                if ($var == null) {
                    $mainparts = new \App\MainParts;
                    $mainparts->cow_id = $in['cow_id'];
                    $mainparts->article = $in['part' . $in['cow_id'] . $i];
                    $mainparts->quantity = $in['qty' . $in['cow_id'] . $i];
                    $cow->mw = $cow->mw + $in['qty' . $in['cow_id'] . $i];
                    $mainparts->article_id = $article_id;
                    $mainparts->save();
                    $cow->save();
                    // dd( $mainparts);
                } else {
                    $mainparts = \App\MainParts::find($var['0']['id']);
//                    dd($mainparts);
                    $mainparts->quantity = $in['qty' . $in['cow_id'] . $i];
                    $cow->mw = $cow->mw + $in['qty' . $in['cow_id'] . $i];
                    $mainparts->save();
                    $cow->save();
                    flash('The article already exist, And now it was override by new input!');
                    return \Redirect::back();
//                    \Flash::overlay($in['part' . $in['cow_id'] . $i].' already exists','Warning');
//                    return \Redirect::back();
                }
            }
        }
        flash('Successfully Added!');
        return \Redirect::back();
    }

    //new---Cindy
    public function AddCuts($id)
    {
        $in = \Input::all();
//        dd($in);
        $count = count($in);
//        dd($count);
        $k = ($count - 2) / 3;
//        dd($k);
        for ($i = 0; $i < $k; $i++) {
            if ($in['part' . $in['cow_id'] . $i] == '' || $in['qty' . $in['cow_id'] . $i] == '' || $in['type' . $in['cow_id'] . $i] == '') {
//                dd($in['part'.$in['cow_id'].$i]);
            } else {
                $art_id = $this->getArticleId($in['part' . $in['cow_id'] . $i]);
//                dd($art_id);
                $check_article_if_exist = \App\Article_Resource::where('cow_id', $id)
                    ->where('article_id', $art_id)->get();
                $var = json_decode(json_encode($check_article_if_exist), TRUE);
//                dd($var);
                if ($var == null) {
//                    dd($in['qty' . $in['cow_id'] . $i]);
                    $article_resource = new \App\Article_Resource;
                    $article_resource->cow_id = $in['cow_id'];
                    $article_resource->quantity = $in['qty' . $in['cow_id'] . $i];
                    $article_resource->remain_qty = $in['qty' . $in['cow_id'] . $i];
                    $article_resource->type = $in['type' . $in['cow_id'] . $i];
                    $article_resource->article_id = $art_id;
                    $article_resource->save();
//                dd($article);
                } else {
                    $article_resource = \App\Article_Resource::find($var['0']['id']);
//                    dd($article_resource);
                    $article_resource->quantity = $in['qty' . $in['cow_id'] . $i];
                    $article_resource->remain_qty = $in['qty' . $in['cow_id'] . $i];
                    $article_resource->type = $in['type' . $in['cow_id'] . $i];
                    $article_resource->save();
                }//
            }
        }
        flash('Successfully Added!');
        return \Redirect::back();
    }

    public function getArticleId($name)
    {
        $id = \App\Article::where('name', $name)->first();
        return $id['id'];
        //return $id;
    }

    public function GetMonth($mon)
    {
        //$month;
        if ($mon == "01") {
            $month = "January";
        } elseif ($mon == "02") {
            $month = "Febuary";
        } elseif ($mon == "03") {
            $month = "March";
        } elseif ($mon == "04") {
            $month = "April";
        } elseif ($mon == "05") {
            $month = "May";
        } elseif ($mon == "06") {
            $month = "June";
        } elseif ($mon == "07") {
            $month = "July";
        } elseif ($mon == "08") {
            $month = "August";
        } elseif ($mon == "09") {
            $month = "September";
        } elseif ($mon == "10") {
            $month = "October";
        } elseif ($mon == "11") {
            $month = "November";
        } elseif ($mon == "12") {
            $month = "December";
        } else {
            $month = null;
        }
        return $month;
    }

    public function ViewReports($id)
    {
        $articles = \App\Article_Resource::where('outsource_id', '=', $id)->orderBy('id')->get();
        $history = \App\Payment_History::where('outsource_i', '=', $id)->orderBy('id')->get();
        return view('Admin.resources.outsource_reports')
            ->with('history', $history)
            ->with('articles', $articles);
    }

}
