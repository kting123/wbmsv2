<?php namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Support\Facades\Storage;
Use Validator;
use Illuminate\Http\Request;

class AdminController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function EditHeader($id)
    {
        $in = \Input::get('header');
        //dd($in);
        // dd($_FILES["file"]["tmp_name"]);
        $user = \App\User::find(\Auth::user()->id);
        //  dd($user);
        $user->h = $in;
        $user->save();
        return \Redirect::back();
    }

    /* show the profile of the user which set the action to editprofile */
    public function ShowProfile()
    {
        $action = "editprofile";
        \Session::put('action', $action);

        return view('Admin.preference')
            ->with('action', $action);
    }

    /* show the profile of the user which set the action to editprofile */
    public function ShowProfileAdmin()
    {
        return view('profile_admin');
    }

    /* when saving changes in profile */
    public function EditProfileAdmin()
    {
        $in = \Input::all();
        //  dd($in);
        $user = \App\User::find(\Auth::user()->id);
        $user->name = $in['name'];
        $user->email = $in['email'];
        if ($in['new_password'] == '' || $in['old_password'] == '') {

        } else {
            if ($in['new_password'] == $in['old_password']) {
                //hash password for security
                $user->password = \Hash::make($in['new_password']);
            } else {
                \Flash::warning('Password doesnt match!');
                return \Redirect::back();
            }
        }
        $user->save();
        $target_file = "assets/img/avatar" . \Auth::user()->id . ".jpg";
        if ($_FILES["file"]["size"] > 2000000) {
            \Flash::warning('Image is too large (>2MB), try another or resize your image.');
            return \Redirect::back();
        }
        if ($_FILES["file"]["tmp_name"] == "") {
            flash('Successfully updated.');
            return \Redirect::back();
        }
        //compress the file upto 70%
        $newfile = $this->compress($_FILES["file"]["tmp_name"], $target_file, 50);
        move_uploaded_file($newfile, $target_file);
        flash('Successfully updated.');
        return \Redirect::back();


    }

    /* when saving changes in profile */
    public function EditProfile()
    {
        $in = \Input::all();
        // dd($in);
        $user = \App\User::find(\Auth::user()->id);
        $user->name = $in['name'];
        $user->email = $in['email'];
        if ($in['new_password'] == '' || $in['old_password'] == '') {

        } else {
            if ($in['new_password'] == $in['old_password']) {
                //hash password for security
                $user->password = \Hash::make($in['new_password']);
            } else {
                \Flash::warning('Password doesnt match!');
                return \Redirect::back();
            }
        }
        $user->save();
        flash('Successfully change!');
        return \Redirect::back();
    }

    /* function in showing the themes/the views */
    public function ShowThemes()
    {
        $action = "themes";
        \Session::put('action', $action);

        return view('Admin.preference')
            ->with('action', $action);
    }

    /* function in showing the themes/the views */
    public function ShowThemesAdmin()
    {
        return view('themes_admin');
    }

    /*when saving changes of themes */
    public function EditThemes()
    {
        $in = \Input::all();
         // dd($in);
        $user = \App\User::find(\Auth::user()->id);
        $user->labels = $in['label'];
        $user->panels = $in['panel'];
        $user->buttons = $in['button'];
        $user->bc = $in['bc'];
        $user->bi = $in['bi'];
        if ($user->bi == 'default') {
            $user->lc = "#777";
            $user->wc = "white";
        } elseif ($user->bi == 'inverse') {
            $user->lc = "#999;";
            $user->wc = "#222";
        }
        $user->opacity = $in['opa'];
        if ($in['pref'] == '0') {
            $user->backimage = "0";
            $user->save();
        } else {
            $user->backimage = "1";
            $user->save();
            $target_file = "assets/img/b" . \Auth::user()->id . ".jpg";
            if ($_FILES["file"]["size"] > 2000000) {
                \Flash::warning('Image is too large (>2MB), try another or resize your image.');
                return \Redirect::back();
            }
            if ($_FILES["file"]["tmp_name"] == "") {
                flash('Successfully updated.');
                return \Redirect::back();
            }
            //compress the file upto 70%
            $newfile = $this->compress($_FILES["file"]["tmp_name"], $target_file, 50);
            move_uploaded_file($newfile, $target_file);
            flash('Successfully updated.');
            return \Redirect::back();
        }
        flash('successfully changed');
        $action = "themes";
        if (\Auth::user()->userType == 'Admin') {
            return \Redirect::to('/Adminthemes' . \Auth::user()->id);
        } else {
            return \Redirect::to('/preferences')
                ->with('action', $action);
        }
    }

    /*function or method on compressing image */
    function compress($source, $destination)
    {
        $info = getimagesize($source);
        if ($info['mime'] == 'image/jpeg')
            $image = imagecreatefromjpeg($source);
        elseif ($info['mime'] == 'image/gif')
            $image = imagecreatefromgif($source);
        elseif ($info['mime'] == 'image/png')
            $image = imagecreatefrompng($source);
        imagejpeg($image, $destination);
        return $destination;
    }

    /*when showing articles in preferences /views */
    public function ShowArticles()
    {
        $article = \App\Article::all();
        return view('articles')->with('articles', $article);
    }

    /*when saving changes in editing articles lik names */
    public function EditArticles($id)
    {
        $article = \App\Article::find($id);
        $article->name = \Input::get('article');
        $article->save();
        flash('Successfully change!');
        return \Redirect::back();
    }

    /*when deleting articles, this will be the function */
    public function DeleteArticles($id)
    {
        $article = \App\Article::find($id);
        $article->delete();
        flash('Successfully delete!');
        return \Redirect::back();
    }

    /* function in adding an articless */
    public function AddArticles()
    {
        $article = new \App\Article;
        $article->name = \Input::get('article');
        $article->save();
        flash('Successfully added!');
        return \Redirect::back();
    }

    /*showing up users in prefrences */
    public function ShowUsers()
    {
        $user = \App\User::all();
        return view('users')
            ->with('users', $user);
    }

    public function EditUser($id)
    {
        $user = \App\User::find($id);
        $user->name = \Input::get('name');
        $user->email = \Input::get('email');
        $user->userType = \Input::get('userType');
        $user->save();
        flash('Successfully change!');
        return \Redirect::back();
    }
    //
    //
    // public function ShowRecords()
    // {
    //     $action = "records";
    //     $user = \App\User::all();
    //
    //     return view('Admin.preference')
    //         ->with('action', $action)
    //         ->with('users', $user);
    // }

    public function EditRecords($id)
    {
        $user = \App\User::find($id);
        $user->name = \Input::get('name');
        $user->email = \Input::get('email');
        $user->userType = \Input::get('userType');
        if (\Input::get('userType') == 'Admin') {
            $user->admin = 1;
        } elseif (\Input::get('userType') == 'user') {
            $user->admin = 0;
        }
        $user->save();
        flash('Successfully change!');
        return \Redirect::back();
    }

    /* TODO : Adding clean up and baking up Databases */
    public function DeleteRecords()
    {
        /*
                $softTrash = \App\TaboTabo::withTrashed()->get();
                dd($softTrash);
                \DB::table('main_parts')->truncate();
                \DB::table('cows')->truncate();
                \DB::table('expenses')->truncate();
                \DB::table('article__resources')->truncate();
                \DB::table('orders')->truncate();
                \DB::table('orderlists')->truncate();
                \DB::table('outsources')->truncate();
                \DB::table('payment__histories')->truncate();
                \DB::table('tabo_tabos')->truncate();
                $delete = \App\Customer::where('customer_type','0')->get();
                foreach ($delete as $del){
                    $del->delete();
                }*/
        \Flash::warning('Database successfully clean up.');
        return \Redirect::back();
    }

    public function BackupRecords()
    {

        return \Redirect::back();
    }
    /*TODO End Here */


    /*function ing deleting an user account*/
    public function DeleteUser($id)
    {
        $user = \App\User::find($id);
        if (\Auth::user()->id == $id) {
            $user->delete();
            \Flash::warning('You just delete your account. :(');
            return \Redirect::to('auth/login');
        }
        $user->delete();
        flash('Successfully delete!');
        return \Redirect::back();
    }

    public function ResetUser($id)
    {
        $user = \App\User::find($id);
        $newpass = \Hash::make('123');
        $user->password = $newpass;
        $user->save();
        flash('Successfully change!');
        return \Redirect::back();
    }

    public function AddUser()
    {
        $in = \Input::all();
        $check = $this->validator($in);
        if ($check->fails()) {
            \Flash::warning('*Unsuccessful, Check your inputs. - Email must be unique/required  - Password must match   - name is required');
            return \Redirect::back();
        }
        $user = new \App\User;
        $user->name = $in['name'];
        $user->email = $in['email'];
        $user->userType = $in['userType'];
        if ($user->userType = 'Admin') {
            $user->admin = '1';
        }
        $user->labels = 'default';
        $user->panels = 'default';
        $user->buttons = 'default';
        $user->bc = 'white';
        $user->bi = 'default';
        $user->backimage = '0';
        if ($in['password'] == $in['password_confirmation']) {
            $user->password = \Hash::make($in['password']);
        } else {
            \Flash::warning('Password Does Not Match!');
            return \Redirect::back();
        }
        $user->save();
        flash('Successfully added!');
        return \Redirect::back();
    }

    public function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /*when saving changes in editing articles lik names */
    public function EditCustomer($id)
    {
        $article = \App\Customer::find($id);
        $article->name = \Input::get('customer');
        $article->save();
        flash('Successfully change!');
        return \Redirect::back();
    }

    /*when deleting articles, this will be the function */
    public function DeleteCustomer($id)
    {
        $article = \App\Customer::find($id);
        $article->delete();
        \Flash::warning('Successfully delete!');
        return \Redirect::back();
    }

}
