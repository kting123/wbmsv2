<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class RealtimeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function Statistics()
    {
        $data = array(
            'or' => \App\Order::where('status', 'paid')->count(),
            'dr' => \App\Order::where('type', 'delivery')->count(),
            'farm' => \App\Cow::all()->count(),
            'outsource' => \App\Outsource::all()->count(),
            'tabotabo' => \App\TaboTabo::all()->count(),
            'stocks_current' => \DB::table('article__resources')->sum('remain_qty'),
            'delivery' => \App\Order::where('type', 'delivery')->count(),
            'Walkein' => \App\Order::where('type', 'walked-in')->count()
        );
        return response()->json($data);
    }

    public function getArticles()
    {
        $type = \Input::get('type');
        $id = \Input::get('id');
        if ($type == 'Farm') {
            $articles = \App\Article_Resource::where('cow_id', $id)->get();
            foreach ($articles as $articled) {
                $articled->article_name = $articled->article->name;
            }
            // dd($articles);
        }
        if ($type == 'Vendors') {
            $articles = \App\Article_Resource::where('outsource_id', $id)->get();
            foreach ($articles as $articled) {
                $articled->article_name = $articled->article->name;
            }
        }

        if ($type == 'TaboTabo') {
            $articles = \App\Article_Resource::where('tabo_tabo_id', $id)->get();
            foreach ($articles as $articled) {
                $articled->article_name = $articled->article->name;
            }
        }
        return response()->json($articles);
    }

    public function getQty()
    {
        $id = \Input::get('id');
        $resource = \App\Article_Resource::where('id', $id)->get();
        foreach ($resource as $source) {
            $qty = $source->remain_qty;
        }
        return response()->json($qty);
    }

    /* BIG A LOGIC STARTS HERE */
    public function getMedicineInfo()
    {
        $id = \Input::get('id');
        $med = \App\Medicine::where('client_id',$id)->get();
        return response()->json($med);
    }
    /* BIG A LOGIC STARTS HERE */
    public function getMedicineInfo1()
    {
        $id = \Input::get('id');
        $supply = \App\Med_Resources::where('big_aoutsources_id',$id)->get();
        foreach($supply as $key=>$sup){
            $newSupply[$key]['medicines'] = $sup->medicine->medicine;
            $newSupply[$key]['medicine_id'] = $sup->medicine->id;
            $newSupply[$key]['qty'] = $sup->remain_qty;
        }
        return response()->json($newSupply);
    }
    public function getUnitPrice()
    {
        $id = \Input::get('id');
        $med = \App\Medicine::find($id);
        $med1 = $med['unit_price'];
        return response()->json($med1);
    }
}
