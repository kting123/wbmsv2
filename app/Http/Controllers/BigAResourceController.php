<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BigAResourceController extends Controller
{

    public function ViewFromSuppliers()
    {

        $clients = \App\Clients::all();
        $resources = \App\BigAoutsource::where('tsk', '0')->orderBy('id', 'desc')->paginate(8);
        $resources->setPath('http://wbms.x10.bz/BigA_Supply');

        //  dd($resources);
        //dd($clients);
        $now = (int)strtotime(date("m/d/y", time()));
        return view('Admin.resources.BigA_suppliers')
            ->with('clients', $clients)
            ->with('resources', $resources)
            ->with('now', $now);
    }

    public function ViewDeliveryStocks()
    {
        $outsource = \App\BigAoutsource::where('tsk','0')->orderBy('id','desc')->get();
        return view('Admin.resources.BigA_deliverStocks')
        ->with('outsources',$outsource);
    }
    
    public function ViewInventory()
    {
        return view('Admin.resources.BigA_inventory');
    }

    public function AddOrder()
    {
        $in = \Input::all();
//        dd($in);
        $order = new \App\Big_A_Orders;
        if($in['radBtn'] == '1'){
            $order->type = 'credit';
            $order->due_date = $in['dueDate'];
            $order->status = 'pending';
            $order->balance = $in['Tamnt'];
        }
        elseif($in['radBtn'] == '0'){
            $order->type = 'cash';
            $order->status = 'paid';
            $order->amount_paid = $in['Tamnt'];
        }
       $cust = \App\Customer::firstOrCreate(array('name' => $in['to'], 'customer_type' => '3'));
        $order->customer_id = $cust->id;
        $order->date_of_order = $in['datePurchased'];
        $order->dr = $in['dr'];        
        $order->or = $in['or'];
        $order->total_due = $in['Tamnt'];
        $order->save();
        $medicine = array_slice($in, 13, -1);
//        dd($order['dr']);
        $count = count($medicine);
        $k = ($count) / 6;
        $w = 0;
        $l = 0;
        $s = 5;
        foreach ($medicine as $key => $item) {
            if (substr($key, 0, 8) == "medicine") {
                $med['medicine' . $l] = $item;
            }
            if (substr($key, 0, 6) == "MedQty") {
                $med['MedQty' . $l] = $item;
            }
            if (substr($key, 0, 7) == "MedUnit") {
                $med['MedUnit' . $l] = $item;
            }
            if (substr($key, 0, 7) == "percent") {
                $med['percent' . $l] = $item;
            }
            if (substr($key, 0, 8) == "MedTotal") {
                $med['MedTotal' . $l] = $item;
            }
            if (substr($key, 0, 2) == "id") {
                $med['id' . $l] = $item;
            }
            if ($w == $s) {
                $l++;
                $s = $s + 6;
            }
            $w++;
        }
       //dd($med)

        for ($i = 0; $i < $k; $i++) {
            $med_id = $this->getMedId1($med['medicine'.$i]);
            $resources = \App\Med_Resources::where('medicines_id',$med_id)->where('big_aoutsources_id',$med['id'.$i])->get();
            foreach($resources as $resource){
                $resource->total_sales= $resource->total_sales + $med['MedTotal' . $i];
                $resource->remain_qty = $resource->remain_qty - $med['MedQty' . $i];
                $resource->sales_qty = $resource->sales_qty + $med['MedQty' . $i];
                $resource->save();
            }
            $orderlist = new \App\Big_A_Orderlists;
            $orderlist->big__a__orders_id = $order->id;
            $orderlist->qty = $med['MedQty' . $i];
            $orderlist->total_price = $med['MedTotal' . $i];
            $orderlist->unit_price = $med['MedUnit' . $i];
            $orderlist->percent = $med['percent' . $i];
            $orderlist->medicines__id = $med_id;
            $orderlist->med__resource_id = $resource->id;
            $orderlist->save();
        }

        // dd($article_id);
        if ($order['radBtn'] == '1') {
            $notify = new \App\Nofication;
            $notify->description = "Big-A : (Credit)Place an order with total amount of Php " . number_format($in['Tamount'], 2) . " to  " . $in['to'];
            $notify->user_id = \Auth::user()->id;
            $notify->date = $order['nowdate'];
            $notify->hr = $order['hr'];
            $notify->min = $order['min'];
            $notify->name = \Auth::user()->name;
            $notify->sec = $order['secs'];
            $notify->ref_id = $order->id;
            $notify->type = 'order_bigA_dr';
            $notify->timestamp = $order['timestamp'];
            $notify->save();

        } else if ($order['radBtn'] == '0') {
            $notify = new \App\Nofication;
            $notify->description = "Big-A : (Cash)Place an order with total amount of Php " . number_format($in['Tamount'], 2) . " to  " . $in['to'];
            $notify->user_id = \Auth::user()->id;
            $notify->date = $in['nowdate'];
            $notify->hr = $in['hr'];
            $notify->min = $in['min'];
            $notify->sec = $in['secs'];
            $notify->ref_id = $order->id;
            $notify->name = \Auth::user()->name;
            $notify->type = 'order_bigA_dr';
            $notify->timestamp = $in['timestamp'];
            $notify->save();
        }
        flash('Successfully Recorded!');
        return \Redirect::back();
    }

    public function AddSupply()
    {
        $in = \Input::all();
        //dd($in);
        $supply = new \App\BigAoutsource;
        $supply->date = $in['datePurchased'];
        $supply->client_id = $in['myClient'];
        $supply->dr = $in['dr#'];
        $supply->total_due = $in['Tamnt'];
        $supply->balance = $in['Tamnt'];
        $supply->due_date = $in['dueDate'];
        $supply->status = 'pending';
        $supply->save();
        $med_resources_id = $supply->id;
        $medicine = array_slice($in, 10, -1);
        //   dd($medicine);
        $count = count($medicine);
        $k = ($count) / 7;
        $w = 0;
        $l = 0;
        $s = 6;
        foreach ($medicine as $key => $item) {
            if (substr($key, 0, 8) == "medicine") {
                $med['medicine' . $l] = $item;
            }
            if (substr($key, 0, 6) == "MedQty") {
                $med['MedQty' . $l] = $item;
            }
            if (substr($key, 0, 7) == "MedUnit") {
                $med['MedUnit' . $l] = $item;
            }
            if (substr($key, 0, 8) == "MedTotal") {
                $med['MedTotal' . $l] = $item;
            }
            if (substr($key, 0, 6) == "expiry") {
                $med['expiry' . $l] = $item;
            }
            if (substr($key, 0, 2) == "ml") {
                $med['ml' . $l] = $item;
            }
            if (substr($key, 0, 12) == "manufacturer") {
                $med['manufacturer' . $l] = $item;
            }
            if ($w == $s) {
                $l++;
                $s = $s + 7;
            }
            $w++;
        }
        // dd($med);
        for ($i = 0; $i < $k; $i++) {
            $med_resources = new \App\Med_Resources;
            $medicine = $this->getMedId($med['medicine' . $i], $med['MedUnit' . $i]);
            //$medicine_price = $this->getMedPrice($med['medicine'.$i]);
            $med_resources->big_aoutsources_id = $med_resources_id;
            $med_resources->medicines_id = $medicine['0']['id'];
            $med_resources->quantity = $med['MedQty' . $i];
            $med_resources->total_price = $med['MedTotal' . $i];
            $med_resources->remain_qty = $med['MedQty' . $i];
            $med_resources->manufacturer = $med['manufacturer' . $i];
            $med_resources->ml = $med['ml' . $i];
            $med_resources->expiry_date = $med['expiry' . $i];
            //dd($article_id);
            $med_resources->save();
        }
        flash('Successfully Added!');
        return \Redirect::back();
    }

    public function editSupply($id)
    {
        $in = \Input::all();
        // dd($in);
        $supply = \App\BigAoutsource::find($id);
        $supply->date = $in['date'];
        $supply->client_id = $in['client'];
        $supply->dr = $in['dr'];
        $supply->or = $in['or'];
        $supply->total_due = $in['amount'];
        $supply->balance = $in['amount'];
        $supply->due_date = $in['dueDate'];
        $supply->status = $in['status'];
        $supply->save();
        flash('Sucessfully changed.');
        return \Redirect::back();
    }

    public function supplyBreakdown($id)
    {
        $resources = \App\Med_Resources::where('big_aoutsources_id', $id)->get();
        $histories = \App\Payment_History::where('big_aoutsources_id', $id)->get();
      //dd($resources);
        return view('Admin.resources.BigA_suppliers_br')
            ->with('medicines', $resources)
            ->with('history', $histories);
    }

    public function DeleteSupply($id)
    {
        $order = \App\BigAoutsource::find($id);
      //  dd($order);
        foreach ($order['med_resources'] as $med_resource) {
                  foreach ($med_resource['big_a_orderlists'] as $list){
                      $list->order->delete();
                      $list->delete();
                  }
                  $med_resource->delete();
                }
        $order->delete();
        \Flash::warning('Successfully deleted!');
        return \Redirect::back();
    }
    public function getMedId($name, $price)
    {
        $medicine = \App\Medicine::where('medicine', $name)->get();
        if ($medicine['0']['unit_price'] == $price) {
        } else {
            foreach ($medicine as $med) {
                $med->unit_price = $price;
                $med->save();
            }
        }
        return $medicine;
    }
    public function getMedId1($name)
    {
        $medicine = \App\Medicine::where('medicine', $name)->get();
        return $medicine[0]['id'];
    }
    
    public function ViewInventoryReports()
    {
        return view('Admin.resources.BigA_inventory_reports');
    }
    

}
