<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BigAReceiptController extends Controller
{

    public function ViewOR()
    {
        /*bringing up the OR's n Big A */
        return view('Admin.customer_or.admin_BigA_OR');
    }

    public function ViewDR()
    {
        /*bringing up the DR's n Big A */
        $now = (int)strtotime(date("m/d/y", time()));
        $pagination = \App\Big_A_Orders::where('type', '=', 'credit')->orderBy('id', 'desc')->paginate(8);
//          dd($pagination);       
         $pagination->setPath('http://localhost/wbmsv2_705/public/BigA_dr');
        return view('Admin.customer_dr.admin_BigA_DR')
         ->with('order', $pagination)
            ->with('now', $now);
    }
    
    public function ViewORBreakdown(){
        return view('Admin.customer_or.admin_BigA_OR_breakdown');
    }
    
    public function ViewDRBreakdown($id)
    {
         $orderslist = \App\Big_A_Orderlists::where('big__a__orders_id', '=', $id)->get();
        dd($orderslist);
        return view('Admin.customer_dr.admin_BigA_DR_breakdown');
    }
    /* method for posting payment in Big Suppliers Orders */
    public function PostVendorPayment($id)            {
    {
        $post_payment = \App\BigAoutsource::find($id);
//       dd($resource);
        $pay = \Input::all();
        //dd($id);
        $history = new \App\Payment_History;
        //dd($post_payment->id);
        $post_payment->amount_paid = $pay['amount'];
        $history->amount_paid = $pay['amount'];
        $post_payment->or = $pay['or'];
        $history->or = $pay['or'];
        $post_payment->date_of_check = $pay['dateofpayment'];
        $history->date_of_check = $pay['dateofpayment'];
        $history->big_aoutsources_id = $post_payment->id;
        $count = count($pay);
        if ($count > 9) {
            $post_payment->Bank = $pay['bank'];
            $history->bank = $pay['bank'];
            $post_payment->Check = $pay['checkno'];
            $history->check = $pay['checkno'];
        } else {

        }
        //dd($post_payment->id);
        $post_payment->balance = $post_payment->balance - $post_payment->amount_paid;
        $history->balance = $post_payment->balance;
        //dd($post_payment->balance);
        if ($post_payment->balance > 0) {
            $post_payment->status = 'partial';
        } elseif ($post_payment->balance == 0) {
            $post_payment->status = 'paid';
        }

        $post_payment->save();
        $history->save();
        flash('Payment Successfully Posted!');
        return \Redirect::back();
    }
}

public function SaveDr($id)
{
    $data = \Input::all();
//    dd($data);
    $order = \App\Big_A_Orders::find($id);
        if ($data['status'] == 'cancelled') {
            foreach ($order['big_a_orderlists'] as $list) {
                $resource = \App\Med_Resources::where('id', $list['med__resource_id'])->get();
                //dd($resource);
                foreach ($resource as $item) {
                    $item->sales_qty = $item->sales_qty - $list['qty'];
                    $item->remain_qty = $item->remain_qty + $list['qty'];
                    $item->total_sales = $item->total_sales - $list['amount'];
                    $item->save();
                }
            }
            $order->due_date = $data['due_date'];
            $order->dr = $data['dr'];
            $order->date_of_order = $data['date'];
            $order->status = $data['status'];
            $order->save();
            \Flash::warning('You just cancelled out an order.');
            return \Redirect::back();
        } elseif ($order['status'] == 'cancelled') {
            foreach ($order['big_a_orderlists'] as $list) {
                $resource = \App\Med_Resources::where('id', $list['med__resource_id'])->get();
                //dd($resource);
                foreach ($resource as $item) {
                    $item->sales_qty = $item->sales_qty + $list['qty'];
                    $item->remain_qty = $item->remain_qty - $list['qty'];
                    $item->total_sales = $item->total_sales + $list['amount'];
                    $item->save();
                }
                //dd('save!');
            }
            $order->due_date = $data['due_date'];
            $order->dr = $data['dr'];
            $order->date_of_order = $data['date'];
            $order->status = $data['status'];
            $order->save();
            \Flash::success('The order status was changed to' . $data['status']);
            return \Redirect::back();
        } else {
            $order->due_date = $data['due_date'];
            $order->dr = $data['dr'];
            $order->date_of_order = $data['date'];
            $order->status = $data['status'];
            $order->save();
            flash('Successfully Edited!');
            return \Redirect::back();
        }
}

public function deleteOrder($id)
    {
        $order = \App\Big_A_Orders::find($id);
//        dd($order);
        foreach ($order['big_a_orderlists'] as $list) {
            $resource = \App\Med_Resources::where('id', $list['med__resource_id'])->get();
            
            $resource1 = json_decode(json_encode($resource), true);
            // dd($resource1);
            if ($resource1 == []) {

            } else {               
                foreach ($resource as $item) {
                    $item->sales_qty = $item->sales_qty - $list['qty'];
                    $item->remain_qty = $item->remain_qty + $list['qty'];
                    $item->total_sales = $item->total_sales - $list['total_price'];
                    $item->save();
                }
            }
//            dd($list);
            $list->delete();
        }
        foreach ($order['payment_history'] as $history) {
//            dd($history);
            $history->delete();
        }
        $order->delete();
        \Flash::warning('Ops, All Records related to this item were deleted and updated.');
        return \Redirect::back();
    }

}

