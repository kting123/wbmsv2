<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Home Controller
      |--------------------------------------------------------------------------

      | This controller renders your application's "dashboard" for users that
      | are authenticated. Of course, you are free to change or remove the
      | controller as you wish. It is just here to get your app started!
      |
     */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard to the user.
     *
     * @return Response
     */
    public function index()
    {

        if (\Auth::user()->userType == 'user') {
            return view('Homev1');
        } elseif (\Auth::user()->userType == 'Admin') {
            $notifications_not_seen = \App\Nofication::where('seen', '!=', \Auth::user()->id)->count();
            return view('Home')->with('count', $notifications_not_seen);

        }
    }

    public function changeUser()
    {
        $user = \App\User::find(\Auth::user()->id);
        if (\Auth::user()->admin == '1') {
            $user->admin = "0";
        } elseif (\Auth::user()->admin == '0') {
            $user->admin = "1";
        }
        $user->save();

        return \Redirect::to('/');
    }

    public function ViewTransactions()
    {
        $custs = \App\Customer::where('customer_type', '2')->orderBy('id', 'desc')->take(10)->get();
        $custs1 = \App\Customer::where('customer_type', '0')->orderBy('id', 'desc')->take(10)->get();
        $cow = \App\Cow::where('tsk', '0')->orderBy('id', 'desc')->take(7)->get();
        $tabo_tabo = \App\TaboTabo::where('tsk', '0')->orderBy('id', 'desc')->take(7)->get();
        $outsource = \App\Outsource::where('tsk', '0')->orderBy('id', 'desc')->take(7)->get();
        $artcles = \App\Article::where('tsk', '0')->orderBy('name')->get();

        //dd($cow['article_resources']);
        return view('subuser.user_transactions')
            ->with('cow', $cow)
            ->with('tabo_tabos', $tabo_tabo)
            ->with('outsources', $outsource)
            ->with('articles', $artcles)
            ->with('custs', $custs)
            ->with('custs1', $custs1);

    }

    public function showStock()
    {
        $articles = \App\Article::all();
        return view('Admin.stock')
            ->with('articles', $articles);
    }

    public function showPreferences()
    {
        $action = \Session::get('action');
        //dd($action);
        $article = \App\Article::all();
        if ($action == '') {
            $action = "editprofile";
        }


        return view('Admin.preference')
            ->with('action', $action)
            ->with('articles', $article);
    }

    public function ViewBaka()
    {
        $cow = \App\Cow::orderBy('id', 'desc')->paginate(3);
        $cow->setPath('http://wbms.x10.bz/baka');
        $tabotabo = \App\TaboTabo::orderBy('id', 'desc')->paginate(5);
        $tabotabo->setPath('http://wbms.x10.bz/baka');
        $article = \App\Article::all();

        // dd($mainparts);
        return view('subuser.baka_manage')
            ->with('article', $article)
            ->with('tabos', $tabotabo)
            ->with('cows', $cow);
    }

    public function ViewMeatshop()
    {
        /*reformat the array to monthly for outsource*/
        $outsources = \App\Outsource::all();
        foreach ($outsources as $key => $outsource) {
            $check = explode("/", $outsource['date']);
            $check = $check['2'] . '-' . $check['0'];
            $new[$check][$key] = $outsource;
        }
        //dd($outsources);
        //pass to getNew Array method
        if (json_decode(json_encode($outsources), true) == []) {
            $outsource1 = [];
        } else {
            $outsource1 = $this->getNewArray($new);
        }

        /*reformat the array to monthly for cow*/
        $cows = \App\Cow::all();
        foreach ($cows as $key => $cow) {
            $check = explode("/", $cow['date']);
            $check = $check['2'] . '-' . $check['0'];
            $new1[$check][$key] = $cow;
        }
        //pass to getNew Array method
        if (json_decode(json_encode($cows), true) == []) {
            $cow1 = [];
        } else {
            $cow1 = $this->getNewArray($new1);
        }


        /*reformat the array to monthly for tabotabo*/
        $tabotabos = \App\TaboTabo::all();
        foreach ($tabotabos as $key => $tabotabo) {
            $check = explode("/", $tabotabo['date']);
            $check = $check['2'] . '-' . $check['0'];
            $new2[$check][$key] = $tabotabo;
        }
        //pass to getNew Array method
        //pass to getNew Array method
        if (json_decode(json_encode($tabotabos), true) == []) {
            $tabotabo1 = [];
        } else {
            $tabotabo1 = $this->getNewArray($new2);
        }
        $data = array(
            'or' => \App\Order::where('status', 'paid')->count(),
            'dr' => \App\Order::where('type', 'delivery')->count(),
            'farm' => \App\Cow::all()->count(),
            'outsource' => \App\Outsource::all()->count(),
            'tabotabo' => \App\TaboTabo::all()->count(),
            'stocks_current' => \DB::table('article__resources')->sum('remain_qty'),
        );
        $delivery = \App\Order::where('type', 'delivery')->count();
        $Walkein = \App\Order::where('type', 'walked-in')->count();
        return view('Admin.admin_meatshop')
            ->with('data', $data)
            ->with('tabotabo', $tabotabo1)
            ->with('cow', $cow1)
            ->with('outsource', $outsource1)
            ->with('delivery', $delivery)
            ->with('Walkein', $Walkein);
    }

    function getNewArray($new)
    {
        foreach ($new as $key => $month) {
            $total = 0;
            foreach ($month as $sources) {
                $total_sales = 0;
                foreach ($sources['article_resources'] as $source) {
                    $total_sales = $total_sales + $source['total_sales'];
                }
                $total = $total + $total_sales;
            }
            $source1[$key] = $total;
        }
        return $source1;
    }

    public function ViewVendor()
    {
        $article = \App\Article::all();
        $vendor = \App\Outsource::where('tsk', '0')->orderBy('id', 'desc')->paginate(5);
        $vendor->setPath('http://wbms.x10.bz/vendor');
        // dd($vendor);
        return view('subuser.vendor_to_vendor')
            ->with('article', $article)
            ->with('vendors', $vendor);
    }

    public function ViewVendor_articleOrder($id)
    {
        //  dd($article);
        $custs = \App\Customer::where('customer_type', '2')->orderBy('id', 'desc')->take(10)->get();
        //  dd($custs);
        $articles = \App\Article_Resource::where('outsource_id', '=', $id)->get();
        //dd($article);

        return view('subuser.vendor_articleOrder')
            ->with('articles', $articles)
            ->with('custs', $custs);
    }

    public function seeAllNoti()
    {
        $notifications = \App\Nofication::where('tsk', 0)->orderBy('id', 'desc')->paginate(7);
        $notifications->setPath('http://wbms.x10.bz/seeAllNoti');
        //  dd($notifications);
        return view('notifications')
            ->with('nots', $notifications);
    }

    public function clearAllNoti()
    {
        \DB::table('notifications')->truncate();
        \Flash::warning('All Logs was succesfully cleared. Gratz!');
        return \Redirect::back();
    }

    public function removeLog($id)
    {
        $log = \App\Nofication::find($id);
        $log->delete();
        \Flash::warning('Log was succesfully removed. Gratz!');
        return \Redirect::back();
    }

    public function viewLog($id)
    {
        $log = \App\Nofication::find($id);
        //  dd($log);
        $log->seen = \Auth::user()->id;
        $log->save();
        /*manage redirections of corresponding logs */
        if ($log->type == 'order_dr')
            return \Redirect::to("./dr_order" . $log->ref_id);
        elseif ($log->type == 'order_or')
            return \Redirect::to("./order_or" . $log->ref_id);
        elseif ($log->type == 'outsource')
            return \Redirect::to("./outsource_reports" . $log->ref_id);
        elseif ($log->type == 'farm')
            return \Redirect::to("./own_info" . $log->ref_id);
        elseif ($log->type == 'tabotabo')
            return \Redirect::to("./view_tabo_tabo_br" . $log->ref_id);
        if ($log->type == 'order_bigA_dr')
            return \Redirect::to("./BigA_dr_breakdown" . $log->ref_id);
        elseif ($log->type == 'order_bigA_or')
            return \Redirect::to("./BigA_or_breakdown" . $log->ref_id);
    }
}
