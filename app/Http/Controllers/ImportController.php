<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class ImportController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function viewImport()
    {
        return view('Admin.forImport.viewImport');
    }

    public function ImportFile()
    {
        // Import a user provided file
        $file = \Input::file('file');
        $filename = $this->upload($file);
        \Excel::load($filename, function ($reader) {
//            $reader->formatDates(true, 'm-d-Y');
            // Getting all results
            $results = $reader->all()->toArray();

            // ->all() is a wrapper for ->get() and will work the same
//            $results = $reader->all();
             dd($results);
            foreach ($results as $result) {
                $tabo = new \App\TaboTabo;
                foreach ($result as $res) {
                  //  dd($res['label']);
                    if ($res['label'] == 'Body no.:') {
                        $tabo->body_number = $res['articles'];
                    } else if ($res['label'] == 'Date:') {
                        $tabo->date = $res['articles'];
                    } else if ($res['label'] == 'Tattoo :') {
                        $tabo->tattoo = $res['articles'];
                    } else if ($res['label'] == 'Eartag no.:') {
                        $tabo->eartagNo = $res['articles'];
                    } else if ($res['label'] == 'Live weight :') {
                        $tabo->lw = $res['articles'];
                    } else if ($res['label'] == 'Slaughter weight :') {
                        $tabo->sw = $res['articles'];
                    } else if ($res['label'] == 'Mall weight :') {
                        $tabo->mw = $res['articles'];
                    } else if ($res['label'] == 'Owner :') {
                      $tabo->eartagNo = $res['articles'];
                    } else if ($res['label'] == 'Sex :') {
                        $tabo->lw = $res['articles'];
                    } else if ($res['label'] == 'Area :') {
                        $tabo->sw = $res['articles'];
                    } else if ($res['label'] == 'Price :') {
                        $tabo->mw = $res['articles'];
                    } else if ($res['label'] == 'Per Kilo :') {
                        $tabo->mw = $res['articles'];
                    } else if ($res['label'] == 'Income/diff :') {
                      $tabo->mw = $res['articles'];
                    }else {

                      }
                }
                $tabo->status = '1';
                $tabo->save();
                dd('success!');
            }


        });

    }

    public function upload($file)
    {
        $destinationPath = "assets/excel";
        $filename = "import.xlsx";
        return $file->move($destinationPath, $filename);
    }
}
