<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class ExportController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function ExportOR()
    {
        $in = \Input::all();
        //  dd($in);
        if ($in['type'] == '0') {
            $data1 = \App\Payment_History::where('balance', '>', '0')->whereBetween('date_of_check', [$in['date_from'], $in['date_to']])->orderBy('id', 'desc')->get();
            $label = $in['date_from'] . " to " . $in['date_to'] . " - Partially Paid";
            $type = "partial";
        } elseif ($in['type'] == '1') {
            $data1 = \App\Order::where('status', 'paid')->where('type', "delivery")->whereBetween('date', [$in['date_from'], $in['date_to']])->orderBy('id', 'desc')->get();
            $type = "";
            $label = $in['date_from'] . " to " . $in['date_to'] . " - Fully Paid for Delivery ";
        } elseif ($in['type'] == '2') {
            $type = "walked-in";
            $data1 = \App\Order::where('type', "walked-in")->whereBetween('date', [$in['date_from'], $in['date_to']])->orderBy('id', 'desc')->get();
            $label = $in['date_from'] . " to " . $in['date_to'] . " -  Walkein";
        } elseif ($in['type'] == '3') {
            $type = "both";
            $data1 = \App\Order::where('status', 'paid')->whereBetween('date', [$in['date_from'], $in['date_to']])->orderBy('id', 'desc')->get();
            $label = $in['date_from'] . " to " . $in['date_to'] . " -  Walkein&delivery";
        }
        // dd($data1);
        \Excel::create("OFFICIAL RECEIPT PBF CATTLE RAISER - " . $label, function ($excel) use ($data1, $type) {
            $excel->sheet('Sheet 1', function ($sheet) use ($data1, $type) {
                //$sheet->setWrapText('true');
                $sheet->loadView('Admin.forExports.exportor')->with('order', $data1)
                    ->with('type', $type);
            });
        })->export('xls');
    }


    public function ExportDR()
    {
        $in = \Input::all();
        //  dd($in);
        if ($in['type'] == '0') {
            $type = 'partial';
        } elseif ($in['type'] == '1') {
            $type = 'pending';
        } elseif ($in['type'] == '2') {
            $type = 'paid';
        } elseif ($in['type'] == '3') {
            $type = 'cancelled';
        }
        if ($in['all'] == '1') {
            $data1 = \App\Order::where('type', '=', 'delivery')->
            whereBetween('date', [$in['date_from'], $in['date_to']])
                ->orderBy('id', 'desc')->get();
            $label = $in['date_from'] . " to " . $in['date_to'] . " - All Types";
        } else {
            $data1 = \App\Order::where('status', $type)->where('type', "delivery")->whereBetween('date', [$in['date_from'], $in['date_to']])->orderBy('id', 'desc')->get();
            $label = $in['date_from'] . " to " . $in['date_to'] . " - " . $type;
        }
        //dd();
        \Excel::create("DELIVERY RECEIPT PBF CATTLE RAISER - " . $label, function ($excel) use ($data1) {
            $excel->sheet('Sheet 1', function ($sheet) use ($data1) {
                $sheet->setWidth(array());
                //$sheet->setWrapText('true');
                $sheet->loadView('Admin.forExports.exportdr')->with('order', $data1);
            });
        })->export('xls');
    }

    public function ExportOutsource()
    {
        $in = \Input::all();
        //dd($in);
        $type = $in['type'];
        $data = \App\Outsource::whereBetween('date', [$in['date_from'], $in['date_to']])->orderBy('id')->get();
        $label = $in['date_from'] . " to " . $in['date_to'];
        //  $data1 = json_decode(json_encode($data), true);
        //  dd($data);
        foreach ($data as $key => $outsource) {
            $check = explode("/", $outsource['date']);
            $check = $check['0'] . '"' . $check['2'];
            $new[$check][$key] = $outsource;
        }
        // dd($new);
        \Excel::create("From Outsource Report ( " . $label . " )", function ($excel) use ($new, $data, $type) {
            if ($type == '1') {
                foreach ($new as $key => $item) {
                    $excel->sheet($key, function ($sheet) use ($item, $type) {
                        $sheet->loadView('Admin.forExports.export_FullOutsourceInfo')
                            ->with('outsources', $item)
                            ->with('type', $type);
                    });
                }
            } elseif ($type == '0') {
                foreach ($data as $data) {
                    $excel->sheet('Outsource ID' . $data['id'], function ($sheet) use ($data, $type) {
                        $sheet->loadView('Admin.forExports.export_FullOutsourceInfo')
                            ->with('outsources', $data)
                            ->with('type', $type);
                    });
                }
            } else {
                // dd($data);
                $excel->sheet('Sheet 1', function ($sheet) use ($data, $type) {
                    $sheet->loadView('Admin.forExports.export_FullOutsourceInfo')
                        ->with('outsources', $data)
                        ->with('type', $type);
                });
            }

        })->export('xls');
    }

    public function ExportTabo()
    {
        $in = \Input::all();
        //  dd($in);
        $type = $in['type'];
        $data = \App\TaboTabo::whereBetween('date', [$in['date_from'], $in['date_to']])->orderBy('id', 'desc')->get();
        $label = $in['date_from'] . " to " . $in['date_to'];
        // $data1 = json_decode(json_encode($data), true);
        // dd($data);
        foreach ($data as $key => $tabo) {
            $check = explode("/", $tabo['date']);
            $check = $check['0'] . '"' . $check['2'];
            $new[$check][$key] = $tabo;
            // dd($tabo);
        }
        // dd($new);
        \Excel::create("Tabo Tabo Report ( " . $label . " )", function ($excel) use ($new, $data, $type) {
            if ($type == '1') {
                foreach ($new as $key => $item) {
                    $excel->sheet($key, function ($sheet) use ($item, $type) {
                        $sheet->loadView('Admin.forExports.export_FullTaboInfo')
                            ->with('tabos', $item)
                            ->with('type', $type);
                    });
                }

            } elseif ($type == '0') {
                foreach ($data as $data) {
                    $excel->sheet('Body #' . $data['id'], function ($sheet) use ($data, $type) {
                        $sheet->loadView('Admin.forExports.export_FullTaboInfo')
                            ->with('tabos', $data)
                            ->with('type', $type);
                    });
                }
            } else {
                // dd($data);
                $excel->sheet('Sheet 1', function ($sheet) use ($data, $type) {
                    $sheet->loadView('Admin.forExports.export_FullTaboInfo')
                        ->with('tabos', $data)
                        ->with('type', $type);
                });
            }

        })->export('xls');
    }

    public
    function ExportOwn()
    {
        $in = \Input::all();
        // dd($in);
        $type = $in['type'];
        $data = \App\Cow::whereBetween('date', [$in['date_from'], $in['date_to']])->orderBy('id')->get();
        $label = $in['date_from'] . " to " . $in['date_to'];
        //  $data1 = json_decode(json_encode($data), true);
        //  // dd($data);
        foreach ($data as $key => $cow) {
            $check = explode("/", $cow['date']);
            $check = $check['0'] . '"' . $check['2'];
            $new[$check][$key] = $cow;
        }
        // dd($new);
        \Excel::create("From Farm Report ( " . $label . " )", function ($excel) use ($new, $data, $type) {
            if ($type == '1') {
                foreach ($new as $key => $item) {
                    $excel->sheet($key, function ($sheet) use ($item, $type) {
                        $sheet->loadView('Admin.forExports.export_fullCowInfo')
                            ->with('cows', $item)
                            ->with('type', $type);
                    });
                }
            } elseif ($type == '0') {
                foreach ($data as $data) {
                    // dd($data);
                    $excel->sheet('Body #' . $data['id'], function ($sheet) use ($data, $type) {
                        $sheet->loadView('Admin.forExports.export_fullCowInfo')
                            ->with('cows', $data)
                            ->with('type', $type);
                    });
                }
            } else {
                // dd($data);
                $excel->sheet('Sheet 1', function ($sheet) use ($data, $type) {
                    $sheet->loadView('Admin.forExports.export_fullCowInfo')
                        ->with('cows', $data)
                        ->with('type', $type);
                });
            }

        })->export('xls');
    }

    public
    function printCowReport($id)
    {
        $cow = \App\Cow::find($id);
        // dd($cow);
        \Excel::create("from Farm - Body Number : " . $cow['id'] . "/" . $cow['date'], function ($excel) use ($cow) {
            $excel->sheet('Sheet 1', function ($sheet) use ($cow) {
                $sheet->loadView('Admin.forExports.export_cowInfo')->with('cows', $cow);
            });
        })->export('xls');
    }

    public
    function printTaboReport($id)
    {
        $tabo = \App\TaboTabo::find($id);
        // dd($cow);
        \Excel::create("Tabo Tabo - Body Number : " . $tabo['id'] . "/" . $tabo['date'], function ($excel) use ($tabo) {

            $excel->sheet('Sheet 1', function ($sheet) use ($tabo) {
                $sheet->loadView('Admin.forExports.export_taboInfo')->with('tabos', $tabo);
            });
        })->export('xls');
    }

    function printOutsourceReport($id)
    {
        $outsource = \App\Outsource::find($id);
        // dd($outsource);
        \Excel::create("OutSource - ID  : " . $outsource['id'] . "/" . $outsource['date'], function ($excel) use ($outsource) {

            $excel->sheet('Sheet 1', function ($sheet) use ($outsource) {
                $sheet->loadView('Admin.forExports.export_outsourceInfo')->with('outsources', $outsource);
            });
        })->export('xls');
    }

    /*   end new function */

    public
    function ExportAll()
    {
        $in = \Input::all();
        $cow = \App\Cow::whereBetween('date', [$in['date_from'], $in['date_to']])->orderBy('id')->get();
        $tabo = \App\TaboTabo::whereBetween('date', [$in['date_from'], $in['date_to']])->orderBy('id')->get();
        $outsource = \App\Outsource::whereBetween('date', [$in['date_from'], $in['date_to']])->orderBy('id')->get();
        $label = $in['date_from'] . " to " . $in['date_to'];
        $receivables = $in['receivables'];
        \Excel::create("Inventory For Meatshop( " . $label . " )", function ($excel) use ($cow, $tabo, $outsource, $receivables) {
            // dd($data);
            $excel->sheet('Sheet 1', function ($sheet) use ($cow, $tabo, $outsource, $receivables) {
                $sheet->loadView('Admin.forExports.exports_all')
                    ->with('cows', $cow)
                    ->with('tabos', $tabo)
                    ->with('outsources', $outsource)
                    ->with('tot_receivables', $receivables);
            });
        })->export('xls');
    }

    public function GenerateAll()
    {
        $in = \Input::all();
        $cow = \App\Cow::whereBetween('date', [$in['date_from'], $in['date_to']])->orderBy('id')->get();
        $tabo = \App\TaboTabo::whereBetween('date', [$in['date_from'], $in['date_to']])->orderBy('id')->get();
        $outsource = \App\Outsource::whereBetween('date', [$in['date_from'], $in['date_to']])->orderBy('id')->get();
        $receivables = \App\Order::whereBetween('date', [$in['date_from'], $in['date_to']])->orderBy('id')->get();
        $tot_receivables = 0;
        foreach ($receivables as $receivable) {
            $tot_receivables = $tot_receivables + $receivable['balance'];
        }
        return view('Admin.forExports.view_all')
            ->with('cows', $cow)
            ->with('tabos', $tabo)
            ->with('outsources', $outsource)
            ->with('tot_receivables', $tot_receivables)
            ->with('date_from',$in['date_from'])
            ->with('date_to',$in['date_to']);
    }

    public function ExportCust($id)
    {
      $in = \Input::all();
      $orders = \App\Order::where('customer_id',$id)->whereBetween('date', [$in['date_from'], $in['date_to']])->orderBy('id')->get();
      $customer = \App\Customer::find($id);
      $label = $in['date_from'] . " to " . $in['date_to'];

      \Excel::create("Report of " .$customer['name']." for ( " . $label . " )", function ($excel) use ($orders, $customer,$in) {
          // dd($data);
          $excel->sheet('Sheet 1', function ($sheet) use ($orders, $customer,$in) {
              $sheet->loadView('Admin.forExports.exportCustomer')
                  ->with('orders', $orders)
                  ->with('customer',$customer['name'])
                  ->with('date_from',$in['date_from'])
                  ->with('date_to',$in['date_to']);
          });
      })->export('xls');
    }


    public function viewCust(){
      $customers = \App\Customer::all();
      // dd($customers);
        return view('Admin.customer_report')
        ->with('customers',$customers);
    }
    public function GenerateCust($id){
      $in = \Input::all();
      $orders = \App\Order::where('customer_id',$id)->whereBetween('date', [$in['date_from'], $in['date_to']])->orderBy('id')->get();
      $customer = \App\Customer::find($id);
      //dd($orders);
      return view ('Admin.forExports.viewCustomerReport')
      ->with('date_from',$in['date_from'])
      ->with('date_to',$in['date_to'])
      ->with('orders',$orders)
      ->with('id',$id)
      ->with('customer',$customer['name']);
    }


}
