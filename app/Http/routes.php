<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the controller to call when that URI is requested.
  |
 */

/* for ajax request */
Route::get('/API/getNotifications', 'NotificationController@getNotifications');
Route::get('/API/getNotificationsCounts', 'NotificationController@getNotificationsCounts');
Route::get('/API/getstats', 'RealtimeController@Statistics');
Route::get('/API/getArticles', 'RealtimeController@getArticles');
Route::get('/API/getQty', 'RealtimeController@getQty');


/*Routes for bringing up the home & general pages*/
Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index');
Route::get('/transactions', 'HomeController@ViewTransactions');
Route::get('/baka', 'HomeController@ViewBaka');
Route::get('/meatshop', 'HomeController@ViewMeatshop');
Route::get('/showStock', 'HomeController@showStock');
Route::get('/preferences', 'HomeController@showPreferences');
Route::get('/change_user', 'HomeController@changeUser');

Route::get('/bigA', 'BigAHomeController@ViewBigA');

/*Routes for OR & DR pointed to ReceiptController*/
Route::get('/or', 'ReceiptController@ViewOR');
Route::post('/or', 'ReceiptController@ViewOR');
Route::post('/dr', 'ReceiptController@ViewDR');
Route::get('/dr', 'ReceiptController@ViewDR');
Route::get('/order_or{id}', 'ReceiptController@ViewOrBreakdown');
Route::get('/dr_order{id}', 'ReceiptController@ViewDRBreakdown');
Route::post('/save_or/{id}', 'ReceiptController@SaveOr');
Route::post('/save_dr/{id}', 'ReceiptController@SaveDr');
Route::post('/postpayment/{id}', 'ReceiptController@PostPayment');
Route::post('/delete_order/{id}', 'ReceiptController@deleteOrder');
Route::post('/edit_order{id}', 'ReceiptController@editOrder');
Route::post('/delete_history/{id}', 'ReceiptController@deleteHistory');
Route::post('/post_vendorPayment{id}','ReceiptController@Post_vendorPayment');

/*Routes for OR & DR pointed to BigAReceiptController*/
Route::get('/BigA_or', 'BigAReceiptController@ViewOR');
Route::get('/BigA_dr', 'BigAReceiptController@ViewDR');
Route::get('/BigA_or_breakdown', 'BigAReceiptController@ViewORBreakdown');
Route::get('/BigA_dr_breakdown', 'BigAReceiptController@ViewDRBreakdown');
Route::get('/BigA_dr_order{id}', 'BigAReceiptController@ViewDRBreakdown');
Route::post('/BigA_save_dr/{id}', 'BigAReceiptController@SaveDr');
Route::post('/BigA_delete_order/{id}', 'BigAReceiptController@deleteOrder');
/*Routes for managing resources pointed to resourceController*/
Route::post('/delete_outsource/{id}', 'ResourceController@DeleteOutsource');
Route::post('/edit_outsource/{id}', 'ResourceController@EditOutsource');
Route::post('/delete_own/{id}', 'ResourceController@DeleteOwn');
Route::post('/edit_own/{id}', 'ResourceController@EditOwn');
Route::post('/delete_tabo/{id}', 'ResourceController@DeleteTabo');
Route::post('/edit_tabo/{id}', 'ResourceController@EditTabo');
Route::post('/save_baka/{id}', 'ResourceController@SaveBaka');
Route::post('/delete_baka/{id}', 'ResourceController@DeleteBaka');
Route::get('/resource_breakdown{id}', 'ResourceController@ResourceBreakdown');
Route::post('/view_tabo_tabo', 'ResourceController@ViewResourcesTabo');
Route::get('/view_tabo_tabo', 'ResourceController@ViewResourcesTabo');
Route::get('/view_tabo_tabo_br{id}', 'ResourceController@ViewTaboBr');
Route::post('/addExpense', 'ResourceController@AddExpense');
Route::post('/addArticles', 'ResourceController@AddArticle');

Route::get('/own', 'ResourceController@ViewFromOwnFarm');
Route::get('/outsource', 'ResourceController@ViewFromOutsource');
Route::post('/addTabo', 'ResourceController@AddTabo');
Route::post('/addVendorOrder', 'ResourceController@AddDeliveryTabo');
Route::get('/own_info{id}', 'ResourceController@CowInfo');
Route::post('/addBaka', 'ResourceController@AddBaka');
Route::post('/addParts{id}', 'ResourceController@AddParts');
Route::post('/addCuts{id}', 'ResourceController@AddCuts');
Route::get('/outsource_reports{id}', 'ResourceController@ViewReports');

//new--Cindy
Route::get('/viewParts{id}', 'ResourceController@ViewParts');
Route::post('/delete_part{id}', 'ResourceController@DeleteParts');
Route::post('/delete_cuts{id}', 'ResourceController@DeleteCuts');
Route::post('/editPart{id}', 'ResourceController@EditPart');
Route::post('/editCut{id}', 'ResourceController@EditCut');
Route::post('/addTaboArticles{id}', 'ResourceController@AddTaboArticles');
Route::get('/viewTaboArticles{id}', 'ResourceController@ViewTaboArticles');
Route::post('/deleteTaboArticle{id}', 'ResourceController@DeleteTaboArticle');
Route::post('/editTaboArticle{id}', 'ResourceController@EditTaboArticle');

/*Routes for Exporting Records*/
//Viewing
Route::post('/genAll', 'ExportController@GenerateAll');
Route::get('/viewCust', 'ExportController@viewCust');
Route::post('/genCust{id}', 'ExportController@GenerateCust');
Route::post('/edit_customer{id}', 'AdminController@EditCustomer');
Route::post('/delete_customer{id}', 'AdminController@DeleteCustomer');

//exporting
Route::post('/exportOR', 'ExportController@ExportOR');
Route::post('/exportDR', 'ExportController@ExportDR');
Route::post('/exportTabo', 'ExportController@ExportTabo');
Route::post('/exportOutsource', 'ExportController@ExportOutsource');
Route::post('/exportOwn', 'ExportController@ExportOwn');
Route::get('/printCowReport{id}', 'ExportController@printCowReport');
Route::get('/printTaboReport{id}', 'ExportController@printTaboReport');
Route::get('/printOutsourceReport{id}', 'ExportController@printOutsourceReport');
Route::post('/exportAll', 'ExportController@ExportAll');
Route::post('/exportCusts{id}', 'ExportController@ExportCust');

/*Routes for importing Records*/
//Viewing
Route::get('/viewImports', 'ImportController@ViewImport');

//importing
Route::post('/importFile', 'ImportController@ImportFile');
Route::post('/exportDR', 'ExportController@ExportDR');
Route::post('/exportTabo', 'ExportController@ExportTabo');
Route::post('/exportOutsource', 'ExportController@ExportOutsource');
Route::post('/exportOwn', 'ExportController@ExportOwn');
Route::get('/printCowReport{id}', 'ExportController@printCowReport');
Route::get('/printTaboReport{id}', 'ExportController@printTaboReport');
Route::get('/printOutsourceReport{id}', 'ExportController@printOutsourceReport');
Route::post('/exportAll', 'ExportController@ExportAll');
Route::post('/exportCusts{id}', 'ExportController@ExportCust');

/* Admin Controller for preferences */
Route::get('/profile', 'AdminController@ShowProfile');
Route::post('/editProfile', 'AdminController@EditProfile');
Route::post('/editProfileAdmin', 'AdminController@EditProfileAdmin');
Route::get('/themes{id}', 'AdminController@ShowThemes');
Route::post('/editThemes', 'AdminController@EditThemes');


//Articles
Route::get('/articles', 'AdminController@ShowArticles');
Route::post('/edit_article{id}', 'AdminController@EditArticles');
Route::post('/delete_article{id}', 'AdminController@DeleteArticles');
Route::post('/add_article', 'AdminController@AddArticles');

//users
Route::get('/users', 'AdminController@ShowUsers');
Route::post('/edit_user{id}', 'AdminController@EditUser');
Route::post('/delete_user{id}', 'AdminController@DeleteUser');
Route::post('/add_user', 'AdminController@AddUser');
Route::post('/reset_user{id}', 'AdminController@ResetUser');


//Admin HERE
Route::get('/Adminprofile', 'AdminController@ShowProfileAdmin');
Route::get('/Adminthemes{id}', 'AdminController@ShowThemesAdmin');
Route::post('/editHeader_{id}', 'AdminController@EditHeader');
Route::post('/sendMail', 'ResetController@sendMail');

//records management -TODO HERE
Route::get('/main', 'AdminController@ShowRecords');
Route::post('/deleteDB', 'AdminController@DeleteRecords');
Route::post('/backup', 'AdminController@BackupRecords');

/* routes for orders */
Route::post('/addOrder', 'OrderController@AddOrder');
Route::post('/addPurchase', 'OrderController@AddPurchase');
Route::post('/add_outsourceOrder', 'OrderController@Add_outsourceOrder');
Route::get('/vendor', 'HomeController@ViewVendor');
Route::get('/vendor_articleOrder{id}', 'HomeController@ViewVendor_articleOrder');


/*notifications */
Route::get('/clearAllNot', 'HomeController@clearAllNoti');
Route::get('/seeAllNoti', 'HomeController@seeAllNoti');
Route::get('/viewlog{id}', 'HomeController@viewLog');
Route::get('/removeLog_{id}', 'HomeController@removeLog');

/*BIG A ROUTES */

/* for ajax request */
Route::get('/API/getMedInfo', 'RealtimeController@getMedicineInfo');
Route::get('/API/getMedInfo1', 'RealtimeController@getMedicineInfo1');
Route::get('/API/getUnitPrice', 'RealtimeController@getUnitPrice');

/*Routes for OR & DR pointed to BigAReceiptController*/
Route::get('/BigA_or', 'BigAReceiptController@ViewOR');
Route::get('/BigA_dr', 'BigAReceiptController@ViewDR');

/*Routes for managing resources pointed to BigAResourceController*/
Route::get('/BigA_Supply', 'BigAResourceController@ViewFromSuppliers');
Route::post('/addSupply', 'BigAResourceController@AddSupply');
Route::post('/save_supply{id}', 'BigAResourceController@editSupply');
Route::get('/BigA_Supply_br{id}', 'BigAResourceController@supplyBreakdown');
Route::get('/BigA_stocks', 'BigAResourceController@ViewDeliveryStocks');
Route::post('/delete_supply/{id}', 'BigAResourceController@DeleteSupply');
Route::get('/BigA_inventory', 'BigAResourceController@ViewInventory');
Route::get('/BigA_inventory_reports', 'BigAResourceController@ViewInventoryReports');

/*routes for orders in Big A */
Route::post('/addOrder', 'BigAResourceController@AddOrder');
//Big A Receipts
Route::post('/post_supplyPayment{id}', 'BigAReceiptController@PostVendorPayment');


Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController',

]);
