<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orderlist extends Model
{

//    public $timestamps = false;
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    public function article_resource()
    {
        return $this->belongsTo('App\Article_Resource','article__resource_id');
    }
    public function article()
    {
        return $this->belongsTo('App\Article');
    }

    public function tabotabo()
    {
        return $this->hasOne('App\TaboTabo');
    }
}
