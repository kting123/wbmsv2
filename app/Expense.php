<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Expense extends Model {

	//
    public $timestamps = false;
    public function tabotabo() {
        return $this->belongsTo('App\TaboTabo');
    }

}
