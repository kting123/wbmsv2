<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicine extends Model{

    public $timestamps = false;

    public function clients(){
        return $this->belongsTo('\App\Clients','client_id');
    }

    public function big_a_orderlist(){
        return $this->hasMany('App\Big_A_Orderlists');
    }

    public function med_resources(){
        return $this->hasMany('App\Med_Resources');
    }

}
