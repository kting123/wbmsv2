<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Med_Resources extends Model{

    public $timestamps = false;

    public function medicine(){
        return $this->belongsTo('\App\Medicine','medicines_id');
    }

    public function bigAoutsource(){
        return $this->belongsTo('\App\BigAoutsource','big_aoutsources_id');
    }


    public function med_supply_order(){
        return $this->belongsTo('\App\Med_Supply_Order');
    }

    public function big_a_orderlists(){
        return $this->hasMany('App\Big_A_Orderlists','med__resource_id');
    }

}
