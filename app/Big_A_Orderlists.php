<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Big_A_Orderlists extends Model{
    public $timestamps = false;
    
    public function medicines(){
        return $this->belongsTo('\App\Medicine','medicines__id');
    }
    
    public function big_a_orders(){
        return $this->belongsTo('\App\Big_A_Orders','big__a__orders_id');
    }
    
    public function med_resources(){
        return $this->belongsTo('\App\Med_Resources','med__resource_id');
    }
    
}