<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment_History extends Model {

	//
    public $timestamps = false;

    public function outsource(){
        return $this->belongsTo('\App\Outsource');
    }

    public function order(){
        return $this->belongsTo('\App\Order');
    }
    public function bigAoutsource(){
        return $this->belongsTo('\App\BigAoutsource','big_aoutsources_id');
    }
}
