<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Nofication extends Model {

	//
	public $timestamps = false;
	protected $table = 'notifications';
	public function user(){
		return $this->belongsTo('App\User');
	}

}
