<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class BigAoutsource extends Model {

	//
    public function clients(){
        return $this->belongsTo('\App\Clients','client_id');
    }

    public function med_resources(){
        return $this->hasMany('\App\Med_Resources','big_aoutsources_id');
    }

    public function payment_history(){
        return $this->hasMany('\App\Payment_History');
    }
}
