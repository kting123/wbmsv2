<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class MainParts extends Model {

	//
    public $timestamps = false;
    public function cow(){
        return $this->belongsTo('\App\Cow');
    }
    public function article(){
        return $this->belongsTo('\App\Article');
    }

}
