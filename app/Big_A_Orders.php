<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App;

use Illuminate\Database\Eloquent\Model;

class Big_A_Orders extends Model{
    public $timestamps = false;
    
    public function customer() {
        return $this->belongsTo('App\Customer');
    }
    public function big_a_orderlists(){
        return $this->hasMany('App\Big_A_Orderlists');
    }
    public function payment_history(){
        return $this->hasMany('App\Payment_History','big_aoutsources_id');
    }
}