$(function(){
  /* ajax request in getting the number of notifications*/
  setInterval(function(){
  $.ajax({
   url: 'API/getNotificationsCounts',
   type: "get",
   success: function(data){
   /* set notifications counter*/
     $('#count').html(data);
     $('#countHome').html(data);
     if(data>0){
  /* set notifications counter to red if more than 1*/
     $('#count').css("color","red");
     }
   }
 });},1000)

/* ajax request in getting the  notifications*/
  setInterval(function(){
  $.ajax({
   url: 'API/getNotifications',
   type: "get",
   success: function(data){
     var i=0;
       $("#alerts").find('li').remove().end().append('<li class="center"><a class="text-center" href="./seeAllNoti"> <strong>View all alerts</strong> <i class="fa fa-angle-right"></i> </a></li>');
     for(notify in data){
         /* get the client time */
         var n = new Date();
         var hour = n.getHours();
         var minutes = n.getMinutes();
         var seconds = n.getSeconds();

          /* convert into timestmp*/
         var timestamp = Math.floor(Number(n.getTime()/1000));

          /* compare current time vs recorded time*/
         var timenow = timestamp - Number(data[i].timestamp);

         /* check if the timenow is 1 day long set to Yesterday*/
         if(timenow>86400 && timenow<172800){
           $('#alerts .center').before("<li><a href='./viewlog"+Number(data[i].id)+"'><div><strong>"+String(data[i].name) +"</strong><span class='pull-right text-muted'><em>Yesterday</em></span></div><div>"+String(data[i].description)+"</div></a></li><li class='divider'></li>");
         }
         /* check if the timenow is within the day*/
         else if(timenow<86400){
              if(hour>Number(data[i].hr)){
                   /* check the difference between time recorded and now*/
                  timelaps = ((hour*60)+minutes)-(Number(data[i].min)+(60*Number(data[i].hr)));
                  if(timelaps<60){
                      if(timelaps<=1){
                        $('#alerts .center').before("<li><a href='./viewlog"+Number(data[i].id)+"'><div><strong>"+String(data[i].name)+"</strong><span class='pull-right text-muted'><em>"+timelaps +" minute ago</em></span></div><div>"+String(data[i].description)+"</div></a></li><li class='divider'></li>");
                      }
                      else{
                        $('#alerts .center').before("<li><a href='./viewlog"+Number(data[i].id)+"'><div><strong>"+String(data[i].name)+"</strong><span class='pull-right text-muted'><em>"+timelaps +" minutes ago</em></span></div><div>"+String(data[i].description)+"</div></a></li><li class='divider'></li>");
                      }
                          }
                  else {
                  var now = Math.floor(timelaps/60);
                          if(now<=1){
                                  $('#alerts .center').before("<li><a href='./viewlog"+Number(data[i].id)+"'><div><strong>"+String(data[i].name)+"</strong><span class='pull-right text-muted'><em>"+now +"  hour ago</em></span></div><div>"+String(data[i].description)+"</div></a></li><li class='divider'></li>");
                                }
                          else {
                                  $('#alerts .center').before("<li><a href='./viewlog"+Number(data[i].id)+"'><div><strong>"+String(data[i].name)+"</strong><span class='pull-right text-muted'><em>"+now + " hours ago</em></span></div><div>"+String(data[i].description)+"</div></a></li><li class='divider'></li>");
                                }
                      }
              }
          else if (hour==Number(data[i].hr)){
                timelaps = minutes-Number(data[i].min);
                $('#alerts .center').before("<li><a href='./viewlog"+Number(data[i].id)+"'><div><strong>"+String(data[i].name)+"</strong><span class='pull-right text-muted'><em>"+timelaps+" minutes ago</em></span></div><div>"+String(data[i].description)+"</div></a></li><li class='divider'></li>");
              }
          else{
                $('#alerts .center').before("<li><a href='./viewlog"+Number(data[i].id)+"'><div><strong>"+String(data[i].name)+"</strong><span class='pull-right text-muted'><em>Yesterday</em></span></div><div>"+String(data[i].description)+"</div></a></li><li class='divider'></li>");
              }
         }

         /* if more than 2 days, set to the date*/
         else if(timenow>172800){
             $('#alerts .center').before("<li><a href='./viewlog"+Number(data[i].id)+"'><div><strong>"+String(data[i].name)+"</strong><span class='pull-right text-muted'><em>"+data[i].date +" </em></span></div><div>"+String(data[i].description)+"</div></a></li><li class='divider'></li>");
         }
         /* increment counter in the next loop*/
         i++;
     }
   }
 });},1000)

 /* for sliding up the alerts*/
 $('.alert').delay(5000).slideUp(300);


/* for navigation back and forward */
  $('#back').click(function(e){
      history.back();
  });
  $('#forward').click(function(e){
      history.forward();
  });
})
