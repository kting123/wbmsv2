<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBigAoutsourcesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('big_aoutsources', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('client_id');
			$table->string('date');
			$table->string('due_date');
			$table->string('status');
			$table->double('total_due');
			$table->double('balance');
			$table->integer('dr');
			$table->integer('or');
			$table->string('bank');
			$table->integer('check');
			$table->string('date_of_check');
			$table->double('amount_paid');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('big_aoutsources');
	}

}
